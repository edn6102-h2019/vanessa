## ACTE V.

### SCÈNE I

\- Orgon, Cléante.

#### CLÉANTE. {#cléante. .cleante}

Où voulez-vous courir ?

#### ORGON. {#orgon. .orgon}

Las ! que sais-je ?

#### CLÉANTE. {#cléante.-1 .cleante}

Il me semble\
Que l\'on doit commencer par consulter ensemble\
Les choses qu\'on peut faire en cet événement.

#### ORGON. {#orgon.-1 .orgon}

Cette cassette-là me trouble entièrement  ;\
Plus que le reste encore elle me désespère.

#### CLÉANTE. {#cléante.-2 .cleante}

Cette cassette est donc un important mystère ?

#### ORGON. {#orgon.-2 .orgon}

C\'est un dépôt qu\'Argas, cet ami que je plains,\
Lui-même, en grand secret, m\'a mis entre les mains :\
Pour cela, dans sa fuite il me voulut élire[^128] ;\
Et ce sont des papiers, à ce qu\'il m\'a pu dire,\
Où sa vie et ses biens se trouvent attachés.

#### CLÉANTE. {#cléante.-3 .cleante}

Pourquoi donc les avoir en d\'autres mains lâchés ?

#### ORGON. {#orgon.-3 .orgon}

Ce fut par un motif de cas de conscience :\
J\'allai droit à mon traître en faire confidence ;\
Et son raisonnement me vint persuader\
De lui donner plutôt la cassette à garder,\
Afin que, pour nier, en cas de quelque enquête,\
J\'eusse d\'un faux-fuyant la faveur toute prête,\
Par où ma conscience eût pleine sûreté\
À faire des serments contre la vérité.

#### CLÉANTE. {#cléante.-4 .cleante}

Vous voilà mal, au moins si j\'en crois l\'apparence ;\
Et la donation, et cette confidence[^129],\
Sont, à vous en parler selon mon sentiment,\
Des démarches par vous faites légèrement.\
On peut vous mener loin avec de pareils gages  ;\
Et cet homme sur vous ayant ces avantages,\
Le pousser est encor grande imprudence à vous,\
Et vous deviez chercher quelque biais plus doux.

#### ORGON. {#orgon.-4 .orgon}

Quoi ? sous un beau semblant[^130] de ferveur si
touchante\
Cacher un coeur si double, une âme si méchante !\
Et moi qui l\'ai reçu gueusant et n\'ayant rien\...\
C\'en est fait, je renonce à tous les gens de bien :\
J\'en aurai désormais une horreur effroyable,\
Et m\'en vais devenir pour eux pire qu\'un diable.

#### CLÉANTE. {#cléante.-5 .cleante}

Hé bien ! ne voilà pas de vos emportements !\
Vous ne gardez en rien les doux tempéraments ;\
Dans la droite raison jamais n\'entre la vôtre,\
Et toujours d\'un excès vous vous jetez dans l\'autre.\
Vous voyez votre erreur, et vous avez connu\
Que par un zèle feint vous étiez prévenu ;\
Mais pour vous corriger, quelle raison demande\
Que vous alliez passer dans une erreur plus grande,\
Et qu\'avecque le coeur d\'un perfide vaurien\
Vous confondiez les coeurs de tous les gens de bien ?\
Quoi ? parce qu\'un fripon vous dupe avec audace\
Sous le pompeux éclat d\'une austère grimace,\
Vous voulez que partout on soit fait comme lui,\
Et qu\'aucun vrai dévot ne se trouve aujourd\'hui ?\
Laissez aux libertins ces sottes conséquences ;\
Démêlez la vertu d\'avec ses apparences,\
Ne hasardez jamais votre estime trop tôt,\
Et soyez pour cela dans le milieu qu\'il faut :\
Gardez-vous, s\'il se peut, d\'honorer l\'imposture,\
Mais au vrai zèle aussi n\'allez pas faire injure ;\
Et s\'il vous faut tomber dans une extrémité,\
Péchez plutôt encor de cet autre côté.

-   [^128]:*Élire* : choisir.
-   [^129]:*Cette confidence* : le fait d\'avoir conservé cette
    cassette qui vous avait été confiée.
-   [^130]:Var. Quoi ? sur un beau semblant... (1682).

## ACTE V.

### SCÈNE II

\- Damis, Orgon, Cléante.

#### DAMIS. {#damis. .damis}

Quoi ? mon père, est-il vrai qu\'un coquin vous menace ?\
Qu\'il n\'est point de bienfait qu\'en son âme il n\'efface,\
Et que son lâche orgueil, trop digne de courroux,\
Se fait de vos bontés des armes contre vous ?

#### ORGON. {#orgon.-5 .orgon}

Oui, mon fils, et j\'en sens des douleurs non pareilles.

#### DAMIS. {#damis.-1 .damis}

Laissez-moi, je lui veux couper les deux oreilles :\
Contre son insolence on ne doit point gauchir[^131] ;\
C\'est à moi, tout d\'un coup, de vous en affranchir,\
Et pour sortir d\'affaire, il faut que je l\'assomme.

#### CLÉANTE. {#cléante.-6 .cleante}

Voilà tout justement parler en vrai jeune homme.\
Modérez, s\'il vous plaît, ces transports éclatants :\
Nous vivons sous un règne et sommes dans un temps\
Où par la violence on fait mal ses affaires.

-   [^131]:*On ne doit point gauchir* : on ne doit pas y aller par
    quatre chemins.

## ACTE V.

### SCÈNE III

\- Madame Pernelle, Mariane, Elmire, Dorine, Damis, Orgon, Cléante.

#### MADAME PERNELLE. {#madame-pernelle. .pernelle}

Qu\'est-ce ? J\'apprends ici de terribles mystères[^132].

#### ORGON. {#orgon.-6 .orgon}

Ce sont des nouveautés dont mes yeux sont témoins,\
Et vous voyez le prix dont sont payés mes soins.\
Je recueille avec zèle un homme en sa misère,\
Je le loge, et le tiens comme mon propre frère ;\
De bienfaits chaque jour il est par moi chargé ;\
Je lui donne ma fille et tout le bien que j\'ai ;\
Et, dans le même temps, le perfide, l\'infâme,\
Tente le noir dessein de suborner ma femme,\
Et non content encor de ces lâches essais,\
Il m\'ose menacer de mes propres bienfaits,\
Et veut, à ma ruine, user des avantages\
Dont le viennent d\'armer mes bontés trop peu sages,\
Me chasser de mes biens, où je l\'ai transféré,\
Et me réduire au point d\'où je l\'ai retiré.

#### DORINE. {#dorine. .dorine}

Le pauvre homme !

#### MADAME PERNELLE. {#madame-pernelle.-1 .pernelle}

Mon fils, je ne puis du tout croire\
Qu\'il ait voulu commettre une action si noire.

#### ORGON. {#orgon.-7 .orgon}

Comment ?

#### MADAME PERNELLE. {#madame-pernelle.-2 .pernelle}

Les gens de bien sont enviés toujours.

#### ORGON. {#orgon.-8 .orgon}

Que voulez-vous donc dire avec votre discours,\
Ma mère ?

#### MADAME PERNELLE. {#madame-pernelle.-3 .pernelle}

Que chez vous on vit d\'étrange sorte,\
Et qu\'on ne sait que trop la haine qu\'on lui porte.

#### ORGON. {#orgon.-9 .orgon}

Qu\'à cette haine à faire avec ce qu\'on vous dit ?

#### MADAME PERNELLE. {#madame-pernelle.-4 .pernelle}

Je vous l\'ai dit cent fois quand vous étiez petit :\
La vertu dans le monde est toujours poursuivie ;\
Les envieux mourront, mais non jamais l\'envie.

#### ORGON. {#orgon.-10 .orgon}

Mais que fait ce discours aux choses d\'aujourd\'hui ?

#### MADAME PERNELLE. {#madame-pernelle.-5 .pernelle}

On vous aura forgé cent sots contes de lui.

#### ORGON. {#orgon.-11 .orgon}

Je vous ai déjà dit que j\'ai vu tout moi-même.

#### MADAME PERNELLE. {#madame-pernelle.-6 .pernelle}

Des esprits médisants la malice est extrême.

#### ORGON. {#orgon.-12 .orgon}

Vous me feriez damner, ma mère. Je vous di\
Que j\'ai vu de mes yeux un crime si hardi.

#### MADAME PERNELLE. {#madame-pernelle.-7 .pernelle}

Les langues ont toujours du venin à répandre,\
Et rien n\'est ici-bas qui s\'en puisse défendre.

#### ORGON. {#orgon.-13 .orgon}

C\'est tenir un propos de sens bien dépourvu.\
Je l\'ai vu, dis-je, vu, de mes propres yeux vu,\
Ce qui s\'appelle vu  : faut-il vous le rebattre\
Aux oreilles cent fois, et crier comme quatre  ?

#### MADAME PERNELLE. {#madame-pernelle.-8 .pernelle}

Mon Dieu, le plus souvent l\'apparence déçoit  :\
Il ne faut pas toujours juger sur ce qu\'on voit.

#### ORGON. {#orgon.-14 .orgon}

J\'enrage.

#### MADAME PERNELLE. {#madame-pernelle.-9 .pernelle}

Aux faux soupçons la nature est sujette,\
Et c\'est souvent à mal que le bien s\'interprète.

#### ORGON. {#orgon.-15 .orgon}

Je dois interpréter à charitable soin\
Le désir d\'embrasser ma femme ?

#### MADAME PERNELLE. {#madame-pernelle.-10 .pernelle}

Il est besoin,\
Pour accuser les gens, d\'avoir de justes causes ;\
Et vous deviez attendre à vous voir sûr des choses.

#### ORGON. {#orgon.-16 .orgon}

Hé, diantre ! le moyen de m\'en assurer mieux ?\
Je devois donc, ma mère, attendre qu\'à mes yeux\
Il eût\... Vous me feriez dire quelque sottise.

#### MADAME PERNELLE. {#madame-pernelle.-11 .pernelle}

Enfin d\'un trop pur zèle on voit son âme éprise ;\
Et je ne puis du tout me mettre dans l\'esprit\
Qu\'il ait voulu tenter les choses que l\'on dit.

#### ORGON. {#orgon.-17 .orgon}

Allez, je ne sais pas, si vous n\'étiez ma mère,\
Ce que je vous dirois, tant je suis en colère.

#### DORINE. {#dorine.-1 .dorine}

Juste retour, Monsieur, des choses d\'ici-bas :\
Vous ne vouliez point croire, et l\'on ne vous croit pas.

#### CLÉANTE. {#cléante.-7 .cleante}

Nous perdons des moments en bagatelles pures,\
Qu\'il faudroit employer à prendre des mesures.\
Aux menaces du fourbe on doit ne dormir point[^133].

#### DAMIS. {#damis.-2 .damis}

Quoi ? son effronterie iroit jusqu\'à ce point ?

#### ELMIRE. {#elmire. .elmire}

Pour moi, je ne crois pas cette instance[^134] possible,\
Et son ingratitude est ici trop visible.

#### CLÉANTE. {#cléante.-8 .cleante}

Ne vous y fiez pas : il aura des ressorts\
Pour donner contre vous raison à ses efforts ;\
Et sur moins que cela, le poids d\'une cabale\
Embarrasse les gens dans un fâcheux dédale.\
Je vous le dis encore : armé de ce qu\'il a,\
Vous ne deviez jamais le pousser jusque là.

#### ORGON. {#orgon.-18 .orgon}

Il est vrai ; mais qu\'y faire ? À l\'orgueil de ce
traître[^135],\
De mes ressentiments je n\'ai pas été maître.

#### CLÉANTE. {#cléante.-9 .cleante}

Je voudrois, de bon coeur, qu\'on pût entre vous deux\
De quelque ombre de paix raccommoder les noeuds.

#### ELMIRE. {#elmire.-1 .elmire}

Si j\'avois su qu\'en main il a de telles armes,\
Je n\'aurois pas donné matière à tant d\'alarmes,\
Et mes\...

#### ORGON. {#orgon.-19 .orgon}

Que veut cet homme ? Allez tôt le savoir.\
Je suis bien en état que l\'on me vienne voir  !

-   [^132]:*Mystères* : secrets, révélations.
-   [^133]:VAR. Aux menaces du fourbe, on ne doit dormir point.
    (1682).
-   [^134]:*Cette instance* : ce procès, cette poursuite.
-   [^135]:*À l\'orgueil de ce traître* : devant l\'orgueil de ce
    traître.

## ACTE V.

### SCÈNE IV

\- Monsieur Loyal, Madame Pernelle, Orgon, Damis, Mariane, Dorine,
Elmire, Cléante.

#### MONSIEUR LOYAL. {#monsieur-loyal. .loyal}

Bonjour, ma chère soeur ; faites, je vous supplie,\
Que je parle à Monsieur.

#### DORINE. {#dorine.-2 .dorine}

Il est en compagnie,\
Et je doute qu\'il puisse à présent voir quelqu\'un.

#### MONSIEUR LOYAL. {#monsieur-loyal.-1 .loyal}

Je ne suis pas pour être en ces lieux importun.\
Mon abord n\'aura rien, je crois, qui lui déplaise ;\
Et je viens pour un fait dont il sera bien aise.

#### DORINE. {#dorine.-3 .dorine}

Votre nom ?

#### MONSIEUR LOYAL. {#monsieur-loyal.-2 .loyal}

Dites-lui seulement que je vien\
De la part de Monsieur Tartuffe, pour son bien.

#### DORINE. {#dorine.-4 .dorine}

C\'est un homme qui vient, avec douce manière,\
De la part de Monsieur Tartuffe, pour affaire\
Dont vous serez, dit-il, bien aise.

#### CLÉANTE. {#cléante.-10 .cleante}

Il vous faut voir\
Ce que c\'est que cet homme, et ce qu\'il peut vouloir.

#### ORGON. {#orgon.-20 .orgon}

Pour nous raccommoder il vient ici peut-être :\
Quels sentiments aurai-je à lui faire paroître ?

#### CLÉANTE. {#cléante.-11 .cleante}

Votre ressentiment ne doit point éclater ;\
Et s\'il parle d\'accord, il le faut écouter.

#### MONSIEUR LOYAL. {#monsieur-loyal.-3 .loyal}

Salut, Monsieur. Le Ciel perde qui vous veut nuire,\
Et vous soit favorable autant que je désire !

#### ORGON. {#orgon.-21 .orgon}

Ce doux début s\'accorde avec mon jugement,\
Et présage déjà quelque accommodement.

#### MONSIEUR LOYAL. {#monsieur-loyal.-4 .loyal}

Toute votre maison m\'a toujours été chère,\
Et j\'étois serviteur de Monsieur votre père.

#### ORGON. {#orgon.-22 .orgon}

Monsieur, j\'ai grande honte et demande pardon\
D\'être sans vous connoître ou savoir votre nom.

#### MONSIEUR LOYAL. {#monsieur-loyal.-5 .loyal}

Je m\'appelle Loyal, natif de Normandie,\
Et suis huissier à verge[^136], en dépit de l\'envie.\
J\'ai depuis quarante ans, grâce au Ciel, le bonheur\
D\'en exercer la charge avec beaucoup d\'honneur ;\
Et je vous viens, Monsieur, avec votre licence,\
Signifier l\'exploit de certaine ordonnance\...

#### ORGON. {#orgon.-23 .orgon}

Quoi ? vous êtes ici\...

#### MONSIEUR LOYAL. {#monsieur-loyal.-6 .loyal}

Monsieur, sans passion :\
Ce n\'est rien seulement qu\'une sommation,\
Un ordre de vuider d\'ici, vous et les vôtres,\
Mettre vos meubles hors, et faire place à d\'autres,\
Sans délai ni remise, ainsi que besoin est\...

#### ORGON. {#orgon.-24 .orgon}

Moi, sortir de céans ?

#### MONSIEUR LOYAL. {#monsieur-loyal.-7 .loyal}

Oui, Monsieur, s\'il vous plaît.\
La maison à présent, comme savez de reste,\
Au bon Monsieur Tartuffe appartient sans conteste.\
De vos biens désormais il est maître et seigneur,\
En vertu d\'un contrat duquel je suis porteur :\
Il est en bonne forme, et l\'on n\'y peut rien dire.

#### DAMIS. {#damis.-3 .damis}

Certes cette impudence est grande, et je l\'admire[^137].

#### MONSIEUR LOYAL. {#monsieur-loyal.-8 .loyal}

Monsieur, je ne dois point avoir affaire à vous ;\
C\'est à Monsieur : il est et raisonnable et doux,\
Et d\'un homme de bien il sait trop bien
l\'office[^138],\
Pour se vouloir du tout opposer à justice.

#### ORGON. {#orgon.-25 .orgon}

Mais\...

#### MONSIEUR LOYAL. {#monsieur-loyal.-9 .loyal}

Oui, Monsieur, je sais que pour un million\
Vous ne voudriez pas faire rébellion,\
Et que vous souffrirez, en honnête personne,\
Que j\'exécute ici les ordres qu\'on me donne.

#### DAMIS. {#damis.-4 .damis}

Vous pourriez bien ici sur votre noir jupon[^139],\
Monsieur l\'huissier à verge, attirer le bâton.

#### MONSIEUR LOYAL. {#monsieur-loyal.-10 .loyal}

Faites que votre fils se taise ou se retire,\
Monsieur. J\'aurois regret d\'être obligé d\'écrire,\
Et de vous voir couché dans mon procès-verbal.

#### DORINE. {#dorine.-5 .dorine}

Ce Monsieur Loyal porte un air bien déloyal !

#### MONSIEUR LOYAL. {#monsieur-loyal.-11 .loyal}

Pour tous les gens de bien j\'ai de grandes tendresses,\
Et ne me suis voulu, Monsieur, charger des pièces\
Que pour vous obliger et vous faire plaisir,\
Que pour ôter par là le moyen d\'en choisir\
Qui, n\'ayant point pour vous le zèle qui me pousse,\
Auroient pu procéder d\'une façon moins douce.

#### ORGON. {#orgon.-26 .orgon}

Et que peut-on de pis que d\'ordonner aux gens\
De sortir de chez eux ?

#### MONSIEUR LOYAL. {#monsieur-loyal.-12 .loyal}

On vous donne du temps,\
Et jusques à demain je ferai surséance\
À l\'exécution, Monsieur, de l\'ordonnance.\
Je viendrai seulement passer ici la nuit,\
Avec dix de mes gens, sans scandale et sans bruit.\
Pour la forme, il faudra, s\'il vous plaît, qu\'on m\'apporte,\
Avant que se coucher, les clefs de votre porte.\
J\'aurai soin de ne pas troubler votre repos,\
Et de ne rien souffrir qui ne soit à propos.\
Mais demain, du matin, il vous faut être habile\
À vuider de céans jusqu\'au moindre ustensile :\
Mes gens vous aideront, et je les ai pris forts,\
Pour vous faire service à tout mettre dehors.\
On n\'en peut pas user mieux que je fais, je pense ;\
Et comme je vous traite avec grande indulgence,\
Je vous conjure aussi, Monsieur, d\'en user bien,\
Et qu\'au dû de ma charge on ne me trouble en rien.

#### ORGON. {#orgon.-27 .orgon}

Du meilleur de mon coeur je donnerois sur l\'heure\
Les cent plus beaux louis de ce qui me demeure,\
Et pouvoir à plaisir sur ce mufle assener\
Le plus grand coup de poing qui se puisse donner[^140].

#### CLÉANTE. {#cléante.-12 .cleante}

Laissez, ne gâtons rien.

#### DAMIS. {#damis.-5 .damis}

À cette audace étrange,\
J\'ai peine à me tenir, et la main me démange[^141].

#### DORINE. {#dorine.-6 .dorine}

Avec un si bon dos, ma foi, Monsieur Loyal,\
Quelques coups de bâton ne vous siéroient pas mal.

#### MONSIEUR LOYAL. {#monsieur-loyal.-13 .loyal}

On pourroit bien punir ces paroles infâmes,\
Mamie, et l\'on décrète aussi contre les femmes.

#### CLÉANTE. {#cléante.-13 .cleante}

Finissons tout cela, Monsieur : c\'en est assez ;\
Donnez tôt ce papier, de grâce, et nous laissez.

#### MONSIEUR LOYAL. {#monsieur-loyal.-14 .loyal}

Jusqu\'au revoir. Le Ciel vous tienne tous en joie !

#### ORGON. {#orgon.-28 .orgon}

Puisse-t-il te confondre, et celui qui t\'envoie !

-   [^136]:La verge servait aux huissiers --- mais M. Loyal est-il
    huissier ou simplement sergent, comme l'indique la liste des
    personnages ? --- à toucher celui auquel il venait signifier un
    exploit.
-   [^137]:*Je l\'admire* : j'en suis stupéfait.
-   [^138]:*L\'office* : le rôle, la conduite.
-   [^139]:*Votre noir jupon* : le jupon est, selon le
    dictionnaire de Furetière (1690), un « grand pourpoint » ou « un
    petit juste-au-corps \[...\] qui ne serre point le corps et qui est
    une espèce de veste propre pour l\'été. »
-   [^140]:Le long passage qui va du v.1773 au v.1800 était sauté
    à la représentation.
-   [^141]:VAR. Cette audace est trop forte,/ J\'ai peine à me
    tenir, il vaut mieux que je sorte (1682).\
    Selon l\'édition de 1734 (t. Ier, p. V et VI), « les comédiens
    avaient fait ce changement parce que souvent ils étaient dans la
    nécessité de faire jouer deux personnages à un même acteur, et
    qu\'en faisant ainsi sortir Damis du théâtre, il pouvait, en
    changeant d\'habits, faire le rôle de l\'Exempt, qui vient avec
    Tartuffe à la fin de l\'acte. »

## ACTE V.

### SCÈNE V

\- Orgon, Cléante, Mariane, Elmire, Madame Pernelle, Dorine, Damis.

#### ORGON. {#orgon.-29 .orgon}

Hé bien, vous le voyez, ma mère, si j\'ai droit[^142],\
Et vous pouvez juger du reste par l\'exploit :\
Ses trahisons enfin vous sont-elles connues ?

#### MADAME PERNELLE. {#madame-pernelle.-12 .pernelle}

Je suis toute ébaubie, et je tombe des nues !

#### DORINE. {#dorine.-7 .dorine}

Vous vous plaignez à tort, à tort vous le blâmez,\
Et ses pieux desseins par là sont confirmés :\
Dans l\'amour du prochain sa vertu se consomme ;\
Il sait que très souvent les biens corrompent l\'homme,\
Et, par charité pure, il veut vous enlever\
Tout ce qui vous peut faire obstacle à vous sauver.

#### ORGON. {#orgon.-30 .orgon}

Taisez-vous, c\'est le mot qu\'il vous faut toujours dire.

#### CLÉANTE. {#cléante.-14 .cleante}

Allons voir quel conseil on doit vous faire élire[^143].

#### ELMIRE. {#elmire.-2 .elmire}

Allez faire éclater l\'audace de l\'ingrat.\
Ce procédé détruit la vertu du contrat ;\
Et sa déloyauté va paroître trop noire,\
Pour souffrir qu\'il en ait le succès qu\'on veut croire.

-   [^142]:*Si j\'ai droit* : si j\'ai sujet de me plaindre.
-   [^143]:*Quel conseil on doit vous faire élire* : quel parti on
    doit vous faire choisir. Les vers 1815 à 1822 étaient sautés à la
    représentation.

## ACTE V.

### SCÈNE VI

\- Valère, Orgon, Cléante, Elmire, Mariane, etc.

#### VALÈRE. {#valère. .valere}

Avec regret, Monsieur, je viens vous affliger ;\
Mais je m\'y vois contraint par le pressant danger.\
Un ami, qui m\'est joint d\'une amitié fort tendre,\
Et qui sait l\'intérêt qu\'en vous j\'ai lieu de prendre,\
A violé pour moi, par un pas délicat,\
Le secret que l\'on doit aux affaires d\'État,\
Et me vient d\'envoyer un avis dont la suite\
Vous réduit[^144] au parti d\'une soudaine fuite.\
Le fourbe qui longtemps a pu vous imposer\
Depuis une heure au Prince a su vous accuser,\
Et remettre en ses mains, dans les traits qu\'il vous jette,\
D\'un criminel d\'État l\'importante cassette,\
Dont, au mépris, dit-il, du devoir d\'un sujet,\
Vous avez conservé le coupable secret.\
J\'ignore le détail du crime qu\'on vous donne ;\
Mais un ordre est donné contre votre personne ;\
Et lui-même est chargé, pour mieux l\'exécuter,\
D\'accompagner celui qui vous doit arrêter.

#### CLÉANTE. {#cléante.-15 .cleante}

Voilà ses droits armés[^145] ; et c\'est par où le
traître\
De vos biens qu\'il prétend cherche à se rendre maître.

#### ORGON. {#orgon.-31 .orgon}

L\'homme est, je vous l\'avoue, un méchant animal !

#### VALÈRE. {#valère.-1 .valere}

Le moindre amusement[^146] vous peut être fatal.\
J\'ai, pour vous emmener, mon carrosse à la porte,\
Avec mille louis qu\'ici je vous apporte.\
Ne perdons point de temps : le trait est foudroyant,\
Et ce sont de ces coups que l\'on pare en fuyant.\
À vous mettre en lieu sûr je m\'offre pour conduite,\
Et veux accompagner jusqu\'au bout votre fuite.

#### ORGON. {#orgon.-32 .orgon}

Las ! que ne dois-je point à vos soins obligeants !\
Pour vous en rendre grâce il faut un autre temps ;\
Et je demande au Ciel de m\'être assez propice,\
Pour reconnoître un jour ce généreux service.\
Adieu : prenez le soin, vous autres\...

#### CLÉANTE. {#cléante.-16 .cleante}

Allez tôt :\
Nous songerons, mon frère, à faire ce qu\'il faut.

-   [^144]:*Dont la suite vous réduit* : dont la conséquence vous
    contraint...
-   [^145]:*Voilà ses droits armés* : le voilà maintenant muni
    d\'armes qui lui permettront d\'abuser de ses droits.
-   [^146]:*Amusement* : délai, atermoiement.

## ACTE V.

### SCÈNE VII (DERNIÈRE)

\- L\'Exempt, Tartuffe, Valère, Orgon, Elmire, Mariane, etc.

#### TARTUFFE. {#tartuffe. .tartuffe}

Tout beau, Monsieur, tout beau, ne courez point si vite :\
Vous n\'irez pas fort loin pour trouver votre gîte,\
Et de la part du Prince on vous fait prisonnier.

#### ORGON. {#orgon.-33 .orgon}

Traître, tu me gardois ce trait pour le dernier ;\
C\'est le coup, scélérat, par où tu m\'expédies,\
Et voilà couronner toutes tes perfidies.

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Vos injures n\'ont rien à me pouvoir aigrir,\
Et je suis pour le Ciel appris à tout souffrir.

#### CLÉANTE. {#cléante.-17 .cleante}

La modération est grande, je l\'avoue.

#### DAMIS. {#damis.-6 .damis}

Comme du Ciel l\'infâme impudemment se joue !

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Tous vos emportements ne sauroient m\'émouvoir,\
Et je ne songe à rien qu\'à faire mon devoir.

#### MARIANE. {#mariane. .mariane}

Vous avez de ceci grande gloire à prétendre,\
Et cet emploi pour vous est fort honnête à prendre.

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

Un emploi ne sauroit être que glorieux,\
Quand il part du pouvoir qui m\'envoie en ces lieux.

#### ORGON. {#orgon.-34 .orgon}

Mais t\'es-tu souvenu que ma main charitable,\
Ingrat, t\'a retiré d\'un état misérable ?

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

Oui, je sais quels secours j\'en ai pu recevoir ;\
Mais l\'intérêt du Prince est mon premier devoir ;\
De ce devoir sacré la juste violence\
Étouffe dans mon coeur toute reconnoissance,\
Et je sacrifierois à de si puissants noeuds\
Ami, femme, parents, et moi-même avec eux.

#### ELMIRE. {#elmire.-3 .elmire}

L\'imposteur !

#### DORINE. {#dorine.-8 .dorine}

Comme il sait, de traîtresse manière,\
Se faire un beau manteau de tout ce qu\'on révère !

#### CLÉANTE. {#cléante.-18 .cleante}

Mais s\'il est si parfait que vous le déclarez,\
Ce zèle qui vous pousse et dont vous vous parez,\
D\'où vient que pour paroître il s\'avise d\'attendre\
Qu\'à poursuivre sa femme il ait su vous surprendre,\
Et que vous ne songez à l\'aller dénoncer\
Que lorsque son honneur l\'oblige à vous chasser ?\
Je ne vous parle point, pour devoir en distraire,\
Du don de tout son bien[^147] qu\'il venoit de vous
faire ;\
Mais le voulant traiter en coupable aujourd\'hui,\
Pourquoi consentiez-vous à rien prendre de lui ?

#### TARTUFFE, *à l\'Exempt.* {#tartuffe-à-lexempt. .tartuffe}

Délivrez-moi, Monsieur, de la criaillerie,\
Et daignez accomplir votre ordre, je vous prie.

#### L\'EXEMPT. {#lexempt. .exempt}

Oui, c\'est trop demeurer, sans doute[^148], à
l\'accomplir :\
Votre bouche à propos m\'invite à le remplir ;\
Et pour l\'exécuter, suivez-moi tout à l\'heure[^149]\
Dans la prison qu\'on doit vous donner pour demeure.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Qui ? moi, Monsieur ?

#### L\'EXEMPT. {#lexempt.-1 .exempt}

Oui, vous.

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

Pourquoi donc la prison  ?

#### L\'EXEMPT. {#lexempt.-2 .exempt}

Ce n\'est pas vous à qui j\'en veux rendre raison.\
Remettez-vous, Monsieur, d\'une alarme si chaude[^150].\
Nous vivons sous un prince ennemi de la fraude,\
Un prince dont les yeux se font jour dans les coeurs,\
Et que ne peut tromper tout l\'art des imposteurs.\
D\'un fin discernement sa grande âme pourvue\
Sur les choses toujours jette une droite vue ;\
Chez elle jamais rien ne surprend trop d\'accès,\
Et sa ferme raison ne tombe en nul excès.\
Il donne aux gens de bien une gloire immortelle ;\
Mais sans aveuglement il fait briller ce zèle,\
Et l\'amour pour les vrais[^151] ne ferme point son
coeur\
À tout ce que les faux doivent donner d\'horreur.\
Celui-ci n\'étoit pas pour le pouvoir surprendre,\
Et de pièges plus fins on le voit se défendre.\
D\'abord il a percé, par ses vives clartés,\
Des replis de son coeur toutes les lâchetés.\
Venant vous accuser, il s\'est trahi lui-même,\
Et par un juste trait de l\'équité suprême[^152],\
S\'est découvert au Prince un fourbe renommé,\
Dont sous un autre nom il étoit informé ;\
Et c\'est un long détail d\'actions toutes noires\
Dont on pourroit former des volumes d\'histoires.\
Ce monarque, en un mot, a vers vous détesté\
Sa lâche ingratitude et sa déloyauté[^153] ;\
À ses autres horreurs il a joint cette suite[^154],\
Et ne m\'a jusqu\'ici soumis à sa conduite\
Que pour voir l\'impudence aller jusques au bout,\
Et vous faire par lui faire raison du tout[^155].\
Oui, de tous vos papiers, dont il se dit le maître,\
Il veut qu\'entre vos mains je dépouille le traître.\
D\'un souverain pouvoir, il brise les liens\
Du contrat qui lui fait un don de tous vos biens,\
Et vous pardonne enfin cette offense secrète\
Où vous a d\'un ami fait tomber la retraite  ;\
Et c\'est le prix qu\'il donne au zèle qu\'autrefois\
On vous vit témoigner en appuyant ses droits[^156],\
Pour montrer que son coeur sait, quand moins on y pense,\
D\'une bonne action verser la récompense,\
Que jamais le mérite avec lui ne perd rien,\
Et que mieux que du mal il se souvient du bien.

#### DORINE. {#dorine.-9 .dorine}

Que le Ciel soit loué !

#### MADAME PERNELLE. {#madame-pernelle.-13 .pernelle}

Maintenant je respire.

#### ELMIRE. {#elmire.-4 .elmire}

Favorable succès !

#### MARIANE. {#mariane.-1 .mariane}

Qui l\'auroit osé dire ?

#### ORGON, *à Tartuffe.* {#orgon-à-tartuffe. .orgon}

Hé bien ! te voilà, traître\...

#### CLÉANTE. {#cléante.-19 .cleante}

Ah ! mon frère, arrêtez,\
Et ne descendez point à des indignités ;\
À son mauvais destin laissez un misérable,\
Et ne vous joignez point au remords qui l\'accable :\
Souhaitez bien plutôt que son coeur en ce jour\
Au sein de la vertu fasse un heureux retour,\
Qu\'il corrige sa vie en détestant son vice\
Et puisse du grand Prince adoucir la justice,\
Tandis qu\'à sa bonté vous irez à genoux\
Rendre ce que demande un traitement si doux.

#### ORGON. {#orgon.-35 .orgon}

Oui, c\'est bien dit : allons à ses pieds avec joie\
Nous louer des bontés que son coeur nous déploie.\
Puis, acquittés un peu de ce premier devoir,\
Aux justes soins d\'un autre il nous faudra pourvoir,\
Et par un doux hymen couronner en Valère\
La flamme d\'un amant généreux et sincère.

-   [^147]:*Je ne vous parle point...* : je ne vous parle point du
    don de tout son bien..., ce qui aurait dû vous détourner de cette
    dénonciation.
-   [^148]:*Sans doute* : sans aucun doute, assurément.
-   [^149]:*Tout à l\'heure* : immédiatement.
-   [^150]:Bien entendu, L\'Exempt s\'adresse à Orgon à partir du
    vers 1905.
-   [^151]:*Pour les vrais* : pour les véritables gens de bien.
-   [^152]:*Et par un juste trait de l\'équité suprême* : par un
    effet de la justice divine.
-   [^153]:*A vers vous détesté sa lâche ingratitude et sa
    déloyauté* : a eu en horreur l\'ingratitude et la déloyauté qu\'il a
    manifestées envers vous.
-   [^154]:*Cette suite* : cette dernière horreur.
-   [^155]:Les vers 1909-1916, 1919-1926, et 1929-1932 étaient
    sautés à la représentation.
-   [^156]:On se souvient qu\'Orgon, durant la Fronde, a soutenu,
    contre le Parlement et les Princes, les droits de la Couronne.
