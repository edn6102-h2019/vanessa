## ACTEURS

#### Mme PERNELLE, mère d\'Orgon. {#mme-pernelle-mère-dorgon. .pernelle}

#### ORGON, mari d\'Elmire. {#orgon-mari-delmire. .orgon}

#### ELMIRE, femme d\'Orgon. {#elmire-femme-dorgon. .elmire}

#### DAMIS, fils d\'Orgon. {#damis-fils-dorgon. .damis}

#### MARIANE, fille d\'Orgon et amante de Valère. {#mariane-fille-dorgon-et-amante-de-valère. .mariane}

#### VALÈRE, amant de Mariane. {#valère-amant-de-mariane. .valere}

#### CLÉANTE, beau-frère d\'Orgon. {#cléante-beau-frère-dorgon. .cleante}

#### TARTUFFE, faux dévot. {#tartuffe-faux-dévot. .tartuffe}

#### DORINE, suivante de Mariane. {#dorine-suivante-de-mariane. .dorine}

#### M. LOYAL, sergent. {#m.-loyal-sergent. .loyal}

#### UN EXEMPT. {#un-exempt. .exempt}

#### FLIPOTE, servante de Mme Pernelle. {#flipote-servante-de-mme-pernelle. .flipote}

La scène est à Paris.
