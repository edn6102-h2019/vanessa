## ACTE III.

### SCÈNE I

\- Damis, Dorine.

#### DAMIS. {#damis. .damis}

Que la foudre sur l\'heure achève mes destins,\
Qu\'on me traite partout du plus grand des faquins,\
S\'il est aucun respect ni pouvoir qui m\'arrête,\
Et si je ne fais pas quelque coup de ma tête !

#### DORINE. {#dorine. .dorine}

De grâce, modérez un tel emportement ;\
Votre père n\'a fait qu\'en parler simplement.\
On n\'exécute pas tout ce qui se propose,\
Et le chemin est long du projet à la chose.

#### DAMIS. {#damis.-1 .damis}

Il faut que de ce fat j\'arrête les complots,\
Et qu\'à l\'oreille un peu je lui dise deux mots.

#### DORINE. {#dorine.-1 .dorine}

Ha ! tout doux ! Envers lui, comme envers votre père,\
Laissez agit les soins de votre belle-mère.\
Sur l\'esprit de Tartuffe elle a quelque crédit ;\
Il se rend complaisant à tout ce qu\'elle dit,\
Et pourroit bien avoir douceur de coeur pour elle.\
Plût à Dieu qu\'il fût vrai ! la chose seroit belle.\
Enfin votre intérêt l\'oblige à le mander :\
Sur l\'hymen qui vous touche elle veut le sonder,\
Savoir ses sentiments, et lui faire connaître\
Quels fâcheux démêlés il pourra faire naître,\
S\'il faut qu\'à ce dessein il prête quelque espoir[^80].\
Son valet dit qu\'il prie, et je n\'ai pu le voir ;\
Mais ce valet m\'a dit qu\'il s\'en alloit descendre.\
Sortez donc, je vous prie, et me laissez l\'attendre.

#### DAMIS. {#damis.-2 .dorine}

Je puis être présent à tout cet entretien.

#### DORINE. {#dorine.-2 .dorine}

Point. Il faut qu\'ils soient seuls.

#### DAMIS. {#damis.-3 .damis}

Je ne lui dirai rien.

#### DORINE. {#dorine.-3 .dorine}

Vous vous moquez : on sait vos transports ordinaires,\
Et c\'est le vrai moyen de gâter les affaires.\
Sortez.

#### DAMIS. {#damis.-4 .damis}

Non : je veux voir sans me mettre en courroux.

#### DORINE. {#dorine.-4 .dorine}

Que vous êtes fâcheux ! Il vient. Retirez-vous.

-   [^80]:*S\'il faut qu\'à ce dessein il prête quelque espoir* :
    si par malheur il pousse à la réalisation de ce projet d\'Orgon.

## ACTE III.

### SCÈNE II

\- Tartuffe, Laurent, Dorine.

#### TARTUFFE, *apercevant Dorine.* {#tartuffe-apercevant-dorine. .tartuffe}

Laurent, serrez ma haire avec ma discipline,\
Et priez que toujours le Ciel vous illumine.\
Si l\'on vient pour me voir, je vais aux prisonniers\
Des aumônes que j\'ai partager les deniers.

#### DORINE. {#dorine.-5 .dorine}

Que d\'affectation et de forfanterie !

#### TARTUFFE. {#tartuffe. .tartuffe}

Que voulez-vous ?

#### DORINE. {#dorine.-6 .dorine}

Vous dire\...

#### TARTUFFE. *Il tire un mouchoir de sa poche.* {#tartuffe.-il-tire-un-mouchoir-de-sa-poche. .tartuffe}

Ah ! mon Dieu, je vous prie,\
Avant que de parler prenez-moi ce mouchoir.

#### DORINE. {#dorine.-7 .dorine}

Comment ?

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Couvrez ce sein que je ne saurois voir :\
Par de pareils objets les âmes sont blessées,\
Et cela fait venir de coupables pensées.

#### DORINE. {#dorine.-8 .dorine}

Vous êtes donc bien tendre à la tentation,\
Et la chair sur vos sens fait grande impression ?\
Certes je ne sais pas quelle chaleur vous monte :\
Mais à convoiter, moi, je ne suis point si prompte[^81],\
Et je vous verrois nu du haut jusques en bas,\
Que toute votre peau ne me tenteroit pas.

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Mettez dans vos discours un peu de modestie,\
Ou je vais sur-le-champ vous quitter la partie.

#### DORINE. {#dorine.-9 .dorine}

Non, non, c\'est moi qui vais vous laisser en repos,\
Et je n\'ai seulement qu\'à vous dire deux mots.\
Madame va venir dans cette salle basse,\
Et d\'un mot d\'entretien vous demande la grâce.

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

Hélas[^82] ! très volontiers.

#### DORINE, *en soi-même.* {#dorine-en-soi-même. .dorine}

Comme il se radoucit !\
Ma foi, je suis toujours pour ce que j\'en ai dit.

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

Viendra-t-elle bientôt ?

#### DORINE. {#dorine.-10 .dorine}

Je l\'entends, ce me semble.\
Oui, c\'est elle en personne, et je vous laisse ensemble.

-   [^81]:VAR. Mais à convoiter, moi, je ne suis pas si prompte.
    (1682).
-   [^82]:*Hélas* : cette interjection ne marque pas ici le regret
    ou la douleur, mais l\'attendrissement. (Cf. Les Femmes savantes,
    IV, 5, v. 1447: « Hélas ! dans cette humeur conservez-le
    toujours ! »).

## ACTE III.

### SCÈNE III

\- Elmire, Tartuffe.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Que le Ciel à jamais par sa toute bonté\
Et de l\'âme et du corps vous donne la santé,\
Et bénisse vos jours autant que le désire\
Le plus humble de ceux que son amour inspire.

#### ELMIRE. {#elmire. .elmire}

Je suis fort obligée à ce souhait pieux.\
Mais prenons une chaise, afin d\'être un peu mieux.

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

Comment de votre mal vous sentez-vous remise ?

#### ELMIRE. {#elmire.-1 .elmire}

Fort bien ; et cette fièvre a bientôt quitté prise.

#### TARTUFFE. {#tartuffe.-7 .tartuffe}

Mes prières n\'ont pas le mérite qu\'il faut\
Pour avoir attiré cette grâce d\'en haut ;\
Mais je n\'ai fait au Ciel nulle dévote instance\
Qui n\'ait eu pour objet votre convalescence.

#### ELMIRE. {#elmire.-2 .elmire}

Votre zèle pour moi s\'est trop inquiété.

#### TARTUFFE. {#tartuffe.-8 .tartuffe}

On ne peut trop chérir votre chère santé,\
Et pour la rétablir j\'aurois donné la mienne.

#### ELMIRE. {#elmire.-3 .elmire}

C\'est pousser bien avant la charité chrétienne,\
Et je vous dois beaucoup pour toutes ces bontés.

#### TARTUFFE. {#tartuffe.-9 .tartuffe}

Je fais bien moins pour vous que vous ne méritez.

#### ELMIRE. {#elmire.-4 .elmire}

J\'ai voulu vous parler en secret d\'une affaire,\
Et suis bien aise ici qu\'aucun ne nous éclaire[^83].

#### TARTUFFE. {#tartuffe.-10 .tartuffe}

J\'en suis ravi de même, et sans doute[^84] il m\'est
doux,\
Madame, de me voir seul à seul avec vous :\
C\'est une occasion qu\'au Ciel j\'ai demandée,\
Sans que jusqu\'à cette heure il me l\'ait accordée.

#### ELMIRE. {#elmire.-5 .elmire}

Pour moi, ce que je veux, c\'est un mot d\'entretien,\
Où tout votre coeur s\'ouvre, et ne me cache rien.

#### TARTUFFE. {#tartuffe.-11 .tartuffe}

Et je ne veux aussi pour grâce singulière\
Que montrer à vos yeux mon âme toute entière,\
Et vous faire serment que les bruits que j\'ai faits[^85]\
Des visites qu\'ici reçoivent vos attraits\
Ne sont pas envers vous l\'effet d\'aucune haine,\
Mais plutôt d\'un transport de zèle qui m\'entraîne,\
Et d\'un pur mouvement\...

#### ELMIRE. {#elmire.-6 .elmire}

Je le prends bien aussi,\
Et crois que mon salut vous donne ce souci.

#### TARTUFFE. *Il lui serre le bout des doigts.* {#tartuffe.-il-lui-serre-le-bout-des-doigts. .tartuffe}

Oui, Madame, sans doute, et ma ferveur est telle\...

#### ELMIRE. {#elmire.-7 .elmire}

Ouf ! vous me serrez trop.

#### TARTUFFE. {#tartuffe.-12 .tartuffe}

C\'est par excès de zèle.\
De vous faire autre mal je n\'eus jamais dessein[^86],\
Et j\'aurois bien plutôt\...\
*(Il lui met la main sur le genou.)*

#### ELMIRE. {#elmire.-8 .elmire}

Que fait là votre main ?

#### TARTUFFE. {#tartuffe.-13 .tartuffe}

Je tâte votre habit : l\'étoffe en est moelleuse.

#### ELMIRE. {#elmire.-9 .elmire}

Ah ! de grâce, laissez, je suis fort chatouilleuse.\
*(Elle recule sa chaise, et Tartuffe rapproche la sienne.)*

#### TARTUFFE. {#tartuffe.-14 .tartuffe}

Mon Dieu ! que de ce point l\'ouvrage est merveilleux !\
On travaille aujourd\'hui d\'un air miraculeux ;\
Jamais, en toute chose, on n\'a vu si bien faire.

#### ELMIRE. {#elmire.-10 .elmire}

Il est vrai. Mais parlons un peu de notre affaire.\
On tient que mon mari veut dégager sa foi,\
Et vous donner sa fille. Est-il vrai, dites-moi ?

#### TARTUFFE. {#tartuffe.-15 .tartuffe}

Il m\'en a dit deux mots ; mais, Madame, à vrai dire,\
Ce n\'est pas le bonheur après quoi je soupire ;\
Et je vois autre part les merveilleux attraits\
De la félicité qui fait tous mes souhaits.

#### ELMIRE. {#elmire.-11 .elmire}

C\'est que vous n\'aimez rien des choses de la terre.

#### TARTUFFE. {#tartuffe.-16 .tartuffe}

Mon sein n\'enferme pas un coeur qui soit de pierre.

#### ELMIRE. {#elmire.-12 .elmire}

Pour moi, je crois qu\'au Ciel tendent tous vos soupirs,\
Et que rien ici-bas n\'arrête vos désirs.

#### TARTUFFE. {#tartuffe.-17 .tartuffe}

L\'amour qui nous attache aux beautés éternelles\
N\'étouffe pas en nous l\'amour des temporelles ;\
Nos sens facilement peuvent être charmés\
Des ouvrages parfaits que le Ciel a formés.\
Ses attraits réfléchis[^87] brillent dans vos pareilles ;\
Mais il étale en vous ses plus rares merveilles ;\
Il a sur votre face épanché des beautés\
Dont les yeux sont surpris, et les coeurs transportés,\
Et je n\'ai pu vous voir, parfaite créature,\
Sans admirer en vous l\'auteur de la nature,\
Et d\'une ardente amour[^88] sentir mon coeur atteint,\
Au plus beau des portraits[^89] où lui-même il s\'est
peint.\
D\'abord j\'appréhendai que cette ardeur secrète\
Ne fût du noir esprit[^90] une surprise
adroite[^91] ;\
Et même à fuir vos yeux mon coeur se résolut,\
Vous croyant un obstacle à faire mon salut.\
Mais enfin je connus, ô beauté toute aimable,\
Que cette passion peut n\'être point coupable,\
Que je puis l\'ajuster avecque la pudeur,\
Et c\'est ce qui m\'y fait abandonner mon coeur.\
Ce m\'est, je le confesse, une audace bien grande\
Que d\'oser de ce coeur vous adresser l\'offrande ;\
Mais j\'attends en mes voeux tout de votre bonté,\
Et rien des vains efforts de mon infirmité ;\
En vous est mon espoir, mon bien, ma quiétude,\
De vous dépend ma peine ou ma béatitude,\
Et je vais être enfin, par votre seul arrêt,\
Heureux si vous voulez, malheureux s\'il vous plaît.

#### ELMIRE. {#elmire.-13 .elmire}

La déclaration est tout à fait galante,\
Mais elle est, à vrai dire, un peu bien surprenante.\
Vous deviez[^92], ce me semble, armer mieux votre sein,\
Et raisonner un peu sur un pareil dessein.\
Un dévot comme vous, et que partout on nomme\...

#### TARTUFFE. {#tartuffe.-18 .tartuffe}

Ah ! pour être dévot, je n\'en suis pas moins
homme[^93] ;\
Et lorsqu\'on vient à voir vos célestes appas,\
Un coeur se laisse prendre, et ne raisonne pas.\
Je sais qu\'un tel discours de moi paroît étrange ;\
Mais, Madame, après tout, je ne suis pas un ange ;\
Et si vous condamnez l\'aveu que je vous fais,\
Vous devez vous en prendre à vos charmants attraits.\
Dès qu j\'en vis briller la splendeur plus qu\'humaine,\
De mon intérieur[^94] vous fûtes souveraine ;\
De vos regards divins l\'ineffable douceur\
Força la résistance où s\'obstinoit mon coeur  ;\
Elle surmonta tout, jeûnes, prières, larmes,\
Et tourna tous mes voeux du côté de vos charmes.\
Mes yeux et mes soupirs vous l\'ont dit mille fois,\
Et pour mieux m\'expliquer j\'emploie ici la voix.\
Que si vous contemplez d\'une âme un peu bénigne\
Les tribulations de votre esclave indigne,\
S\'il faut que vos bontés veuillent me consoler\
Et jusqu\'à mon néant daignent se ravaler,\
J\'aurai toujours pour vous, ô suave merveille,\
Une dévotion à nulle autre pareille.\
Votre honneur avec moi ne court point de hasard,\
Et n\'a nulle disgrâce à craindre de ma part.\
Tous ces galants de cour, dont les femmes sont folles,\
Sont bruyants dans leurs faits et vains dans leurs paroles,\
De leurs progrès sans cesse on les voit se targuer ;\
Ils n\'ont point de faveurs qu\'ils n\'aillent divulguer,\
Et leur langue indiscrète, en qui l\'on se confie,\
Déshonore l\'autel où leur coeur sacrifie.\
Mais les gens comme nous brûlent d\'un feu discret,\
Avec qui pour toujours on est sûr du secret :\
Le soin que nous prenons de notre renommée\
Répond de toute chose à la personne aimée,\
Et c\'est en nous qu\'on trouve, acceptant notre coeur,\
De l\'amour sans scandale et du plaisir sans peur[^95].

#### ELMIRE. {#elmire.-14 .elmire}

Je vous écoute dire, et votre rhétorique\
En termes assez forts à mon âme s\'explique.\
N\'appréhendez-vous point que je ne sois d\'humeur\
À dire à mon mari cette galante ardeur,\
Et que le prompt avis d\'un amour de la sorte\
Ne pût bien altérer l\'amitié qu\'il vous porte  ?

#### TARTUFFE. {#tartuffe.-19 .tartuffe}

Je sais que vous avez trop de bénignité,\
Et que vous ferez grâce à ma témérité\
Que vous m\'excuserez sur l\'humaine foiblesse\
Des violents transports d\'un amour qui vous blesse,\
Et considérerez, en regardant votre air,\
Que l\'on est pas aveugle, et qu\'un homme est de chair.

#### ELMIRE. {#elmire.-15 .elmire}

D\'autres prendroient cela d\'autre façon peut-être ;\
Mais ma discrétion se veut faire paroître.\
Je ne redirai point l\'affaire à mon époux  ;\
Mais je veux en revanche une chose de vous :\
C\'est de presser tout franc et sans nulle chicane\
L\'union de Valère avecque Mariane,\
De renoncer vous-même à l\'injuste pouvoir\
Qui veut du bien d\'un autre enrichir votre espoir,\
Et\...

-   [^83]:*Ne nous éclaire* : ne nous épie, ne nous observe.
-   [^84]:*Sans doute* : sans aucun doute, assurément.
-   [^85]:VAR. Et vous faire serment, que les bruits que je fais.
    (1682).
-   [^86]:VAR. De vous faire aucun mal je n\'eus jamais dessein.
    (1682).
-   [^87]:*Ses attraits réfléchis* : un reflet de ses attraits, de
    ses splendeurs.
-   [^88]:*Amour* est souvent encore féminin au XVIIe siècle.
-   [^89]:*Au plus beau des portraits* : devant le plus beau des
    portraits.
-   [^90]:*Le noir esprit* : le diable.
-   [^91]:*Adroite* se prononçait *adrète* (Vaugelas nous indique,
    dans ses *Remarques*, que droit se prononçait dret).
-   [^92]:*Vous deviez* : vous auriez dû.
-   [^93]:Parodie du fameux vers de Corneille : « Ah ! pour être
    Romain, je n\'en suis pas moins homme ! » (Sertorius, IV, 1, v.
    1194).
-   [^94]:*De mon intérieur* : l\'intérieur est, dans la langue de
    la spiritualité, « la partie intime de l\'âme » (Littré).
-   [^95]:Cf. Mathurin Régnier, *Satire XIII*, v. 121-124, où il
    est dit des moines : « Outre le saint voeu qui sert de couverture,/
    Ils sont trop obligés au secret de nature/ Et savent, plus discrets,
    apporter en aimant/ Avecque moins d\'éclat, plus de contentement. »

## ACTE III.

### SCÈNE IV

\- Damis, Elmire, Tartuffe.

#### DAMIS, *sortant du petit cabinet où il s\'étoit retiré.* {#damis-sortant-du-petit-cabinet-où-il-sétoit-retiré. .damis}

Non, Madame, non : ceci doit se répandre.\
J\'étois en cet endroit, d\'où j\'ai pu tout entendre ;\
Et la bonté du Ciel m\'y semble avoir conduit\
Pour confondre l\'orgueil d\'un traître qui me nuit,\
Pour m\'ouvrir une voie à prendre la vengeance\
De son hypocrisie et de son insolence,\
À détromper mon père, et lui mettre en plein jour\
L\'âme d\'un scélérat qui vous parle d\'amour.

#### ELMIRE. {#elmire.-16 .elmire}

Non, Damis : il suffit qu\'il se rende plus sage,\
Et tâche à mériter la grâce où je m\'engage.\
Puisque je l\'ai promis, ne m\'en dédites pas.\
Ce n\'est point mon humeur de faire des éclats :\
Une femme se rit de sottises pareilles,\
Et jamais d\'un mari n\'en trouble les oreilles.

#### DAMIS. {#damis.-5 .damis}

Vous avez vos raisons pour en user ainsi,\
Et pour faire autrement j\'ai les miennes aussi.\
Le vouloir épargner est une raillerie[^96] ;\
Et l\'insolent orgueil de sa cagoterie\
N\'a triomphé que trop de mon juste courroux,\
Et que trop excité de désordre chez nous.\
Le fourbe trop longtemps a gouverné mon père,\
Et desservi mes feux avec ceux de Valère.\
Il faut que du perfide il soit désabusé,\
Et le Ciel pour cela m\'offre un moyen aisé.\
De cette occasion je lui suis redevable,\
Et pour la négliger, elle est trop favorable :\
Ce seroit mériter qu\'il me la vînt ravir\
Que de l\'avoir en main et ne m\'en pas servir.

#### ELMIRE. {#elmire.-17 .elmire}

Damis\...

#### DAMIS. {#damis.-6 .damis}

Non, s\'il vous plaît, il faut que je me croie[^97].\
Mon âme est maintenant au comble de sa joie ;\
Et vos discours en vain prétendent m\'obliger\
À quitter le plaisir de me pouvoir venger.\
Sans aller plus avant, je vais vuider d\'affaire[^98] ;\
Et voici justement de quoi me satisfaire.

-   [^96]:*Raillerie* : chose déraisonnable.
-   [^97]:*Il faut que je me croie* : il faut que je suive mon
    sentiment, que je fasse ce que j\'ai envie de faire (Cf. Le Dépit
    amoureux, v. 927).
-   [^98]:*Vider d\'affaire* (ou d\'affaires) : « On dit vider
    d\'affaires pour dire travailler à en sortir promptement, à les
    terminer » (Dictionnaire de l\'Académie, 1694).

## ACTE III.

### SCÈNE V

\- Orgon, Damis, Tartuffe, Elmire.

#### DAMIS. {#damis.-7 .damis}

Nous allons régaler, mon père, votre abord\
D\'un incident tout frais qui vous surprendra fort.\
Vous êtes bien payé de toutes vos caresses,\
Et Monsieur d\'un beau prix reconnoît vos tendresses.\
Son grand zèle pour vous vient de se déclarer :\
Il ne va pas à moins qu\'à vous déshonorer ;\
Et je l\'ai surpris là qui faisoit à Madame\
L\'injurieux aveu d\'une coupable flamme.\
Elle est d\'une humeur douce, et son coeur trop discret\
Vouloit à toute force en garder le secret ;\
Mais je ne puis flatter une telle impudence,\
Et crois que vous la taire est vous faire une offense.

#### ELMIRE. {#elmire.-18 .elmire}

Oui, je tiens que jamais de tous ces vains propos\
On ne doit d\'un mari traverser le repos,\
Que ce n\'est point de là que l\'honneur peut dépendre,\
Et qu\'il suffit pour nous de savoir nous défendre :\
Ce sont mes sentiments ; et vous n\'auriez rien dit,\
Damis, si j\'avois eu sur vous quelque crédit.

## ACTE III.

### SCÈNE VI

\- Orgon, Damis, Tartuffe.

#### ORGON. {#orgon. .orgon}

Ce que je viens d\'entendre, ô Ciel ! est-il croyable ?

#### TARTUFFE. {#tartuffe.-20 .tartuffe}

Oui, mon frère, je suis un méchant, un coupable,\
Un malheureux pécheur, tout plein d\'iniquité,\
Le plus grand scélérat qui jamais ait été ;\
Chaque instant de ma vie est chargé de souillures ;\
Elle n\'est qu\'un amas de crimes et d\'ordures ;\
Et je vois que le Ciel, pour ma punition,\
Me veut mortifier en cette occasion.\
De quelque grand forfait qu\'on me puisse reprendre,\
Je n\'ai garde d\'avoir l\'orgueil de m\'en défendre.\
Croyez ce qu\'on vous dit, armez votre courroux,\
Et comme un criminel chassez-moi de chez vous :\
Je ne saurois avoir tant de honte en partage,\
Que je n\'en aie encor mérité davantage.

#### ORGON, *à son fils.* {#orgon-à-son-fils. .orgon}

Ah ! traître, oses-tu bien par cette fausseté\
Vouloir de sa vertu ternir la pureté ?

#### DAMIS. {#damis.-8 .damis}

Quoi ? la feinte douceur de cette âme hypocrite\
Vous fera démentir\... ?

#### ORGON. {#orgon.-1 .orgon}

Tais-toi, peste maudite.

#### TARTUFFE. {#tartuffe.-21 .tartuffe}

Ah ! laissez-le parler : vous l\'accusez à tort,\
Et vous feriez bien mieux de croire à son rapport.\
Pourquoi sur un tel fait m\'être si favorable ?\
Savez-vous, après tout, de quoi je suis capable ?\
Vous fiez-vous, mon frère, à mon extérieur ?\
Et, pour tout ce qu\'on voit, me croyez-vous meilleur ?\
Non, non : vous vous laissez tromper à l\'apparence,\
Et je ne suis rien moins, hélas ! que ce qu\'on pense ;\
Tout le monde me prend pour un homme de bien ;\
Mais la vérité pure est que je ne vaux rien.\
*(S\'adressant à Damis.)*\
Oui, mon cher fils, parlez : traitez-moi de perfide,\
D\'infâme, de perdu, de voleur, d\'homicide ;\
Accablez-moi de noms encor plus détestés :\
Je n\'y contredis point, je les ai mérités ;\
Et j\'en veux à genoux souffrir l\'ignominie,\
Comme une honte due aux crimes de ma vie.

#### ORGON. {#orgon.-2 .orgon}

*(À Tartuffe.)*\
Mon frère, c\'en est trop.\
*(À son fils.)*\
Ton coeur ne se rend point,\
Traître ?

#### DAMIS. {#damis.-9 .damis}

Quoi ! ses discours vous séduiront au point\...

#### ORGON. {#orgon.-3 .orgon}

Tais-toi, pendard.\
*(À Tartuffe.)*\
Mon frère, eh ! levez-vous, de grâce !\
*(À son fils.)*\
Infâme !

#### DAMIS. {#damis.-10 .damis}

Il peut\...

#### ORGON. {#orgon.-4 .orgon}

Tais-toi.

#### DAMIS. {#damis.-11 .damis}

J\'enrage ! Quoi ? je passe\...

#### ORGON. {#orgon.-5 .orgon}

Si tu dis un seul mot, je te romprai les bras.

#### TARTUFFE. {#tartuffe.-22 .tartuffe}

Mon frère, au nom de Dieu, ne vous emportez pas.\
J\'aimerois mieux souffrir la peine la plus dure,\
Qu\'il eût reçu pour moi la moindre égratignure.

#### ORGON. {#orgon.-6 .orgon}

*(À son fils.)*\
Ingrat !

#### TARTUFFE. {#tartuffe.-23 .tartuffe}

Laissez-le en paix[^99]. S\'il faut, à deux genoux,\
Vous demander sa grâce\...

#### ORGON, *à Tartuffe.* {#orgon-à-tartuffe. .orgon}

Hélas ! vous moquez-vous ?\
*(À son fils.)*\
Coquin ! vois sa bonté.

#### DAMIS. {#damis.-12 .damis}

Donc\...

#### ORGON. {#orgon.-7 .orgon}

Paix.

#### DAMIS. {#damis.-13 .damis}

Quoi ? je\...

#### ORGON. {#orgon.-8 .orgon}

Paix, dis-je\
Je sais bien quel motif à l\'attaquer t\'oblige :\
Vous le haïssez tous ; et je vois aujourd\'hui\
Femme, enfants et valets déchaînés contre lui ;\
On met impudemment toute chose en usage,\
Pour ôter de chez moi ce dévot personnage.\
Mais plus on fait d\'effort afin de le bannir,\
Plus j\'en veux employer à l\'y mieux retenir ;\
Et je vais me hâter de lui donner ma fille,\
Pour confondre l\'orgueil de toute ma famille.

#### DAMIS. {#damis.-14 .damis}

A recevoir sa main on pense l\'obliger ?

#### ORGON. {#orgon.-9 .orgon}

Oui, traître, et dès ce soir, pour vous faire enrager.\
Ah ! je vous brave tous, et vous ferai connaître\
Qu\'il faut qu\'on m\'obéisse et que je suis le maître.\
Allons, qu\'on se rétracte, et qu\'à l\'instant, fripon,\
On se jette à ses pieds pour demander pardon.

#### DAMIS. {#damis.-15 .damis}

Qui, moi ? de ce coquin, qui, par ses impostures\...

#### ORGON. {#orgon.-10 .orgon}

Ah ! tu résistes, gueux, et lui dis des injures ?\
*(À Tartuffe.)*\
Un bâton ! un bâton ! Ne me retenez pas.\
*(À son fils.)*\
Sus, que de ma maison on sorte de ce pas,\
Et que d\'y revenir on n\'ait jamais l\'audace.

#### DAMIS. {#damis.-16 .damis}

Oui, je sortirai ; mais\...

#### ORGON. {#orgon.-11 .orgon}

Vite quittons la place.\
Je te prive, pendard, de ma succession,\
Et te donne de plus ma malédiction.

-   [^99]:*Laissez-le en paix* : le *e* muet du pronom *le*
    s\'élide devant la voyelle du mot suivant.

## ACTE III.

### SCÈNE VII

\- Orgon, Tartuffe.

#### ORGON. {#orgon.-12 .orgon}

Offenser de la sorte une sainte personne !

#### TARTUFFE. {#tartuffe.-24 .tartuffe}

Ô Ciel, pardonne-lui la douleur qu\'il me donne[^100] !\
*(À Orgon.)*\
Si vous pouviez savoir avec quel déplaisir\
Je vois qu\'envers mon frère on tâche à me noircir\...

#### ORGON. {#orgon.-13 .orgon}

Hélas !

#### TARTUFFE. {#tartuffe.-25 .tartuffe}

Le seul penser de cette ingratitude\
Fait souffrir à mon âme un supplice si rude\...\
L\'horreur que j\'en conçois\... J\'ai le coeur si serré,\
Que je ne puis parler, et crois que j\'en mourrai.

#### ORGON. {#orgon.-14 .orgon}

*(Il court tout en larmes à la porte par où il a chassé son fils.)*\
Coquin ! je me repens que ma main t\'ai fait grâce,\
Et ne t\'ait pas d\'abord assommé sur la place.\
Remettez-vous, mon frère, et ne vous fâchez pas.

#### TARTUFFE. {#tartuffe.-26 .tartuffe}

Rompons, rompons le cours de ces fâcheux débats.\
Je regarde céans quels grands troubles j\'apporte,\
Et crois qu\'il est besoin, mon frère, que j\'en sorte.

#### ORGON. {#orgon.-15 .orgon}

Comment ? vous moquez-vous ?

#### TARTUFFE. {#tartuffe.-27 .tartuffe}

On m\'y hait, et je voi\
Qu\'on cherche à vous donner des soupçons de ma
foi[^101].

#### ORGON. {#orgon.-16 .orgon}

Qu\'importe ? Voyez-vous que mon coeur les écoute ?

#### TARTUFFE. {#tartuffe.-28 .tartuffe}

On ne manquera pas de poursuivre, sans doute[^102] ;\
Et ces mêmes rapports qu\'ici vous rejetez\
Peut-être une autre fois seront-ils écoutés.

#### ORGON. {#orgon.-17 .orgon}

Non, mon frère, jamais.

#### TARTUFFE. {#tartuffe.-29 .tartuffe}

Ah ! mon frère, une femme\
Aisément d\'un mari peut bien surprendre l\'âme.

#### ORGON. {#orgon.-18 .orgon}

Non, non.

#### TARTUFFE. {#tartuffe.-30 .tartuffe}

Laissez-moi vite, en m\'éloignant d\'ici,\
Leur ôter tout sujet de m\'attaquer ainsi.

#### ORGON. {#orgon.-19 .orgon}

Non, vous demeurerez : il y va de ma vie.

#### TARTUFFE. {#tartuffe.-31 .tartuffe}

Hé bien ! il faudra donc que je me mortifie.\
Pourtant, si vous vouliez\...

#### ORGON. {#orgon.-20 .orgon}

Ah !

#### TARTUFFE. {#tartuffe.-32 .tartuffe}

Soit : n\'en parlons plus.\
Mais je sais comme il faut en user là-dessus.\
L\'honneur est délicat, et l\'amitié m\'engage\
À prévenir les bruits et les sujets d\'ombrage.\
Je fuirai votre épouse, et vous ne me verrez\...

#### ORGON. {#orgon.-21 .orgon}

Non, en dépit de tous vous la fréquenterez.\
Faire enrager le monde est ma plus grande joie,\
Et je veux qu\'avec elle à toute heure on vous voie.\
Ce n\'est pas tout encor  : pour les mieux braver tous,\
Je ne veux point avoir d\'autre héritier que vous,\
Et je vais de ce pas, en fort bonne manière,\
Vous faire de mon bien donation entière.\
Un bon et franc ami, que pour gendre je prends,\
M\'est bien plus cher que fils, que femme, et que parents.\
N\'accepterez-vous pas ce que je vous propose ?

#### TARTUFFE. {#tartuffe.-33 .tartuffe}

La volonté du Ciel soit faite en toute chose.

#### ORGON. {#orgon.-22 .orgon}

Le pauvre homme ! Allons vite en dresser un écrit,\
Et que puisse l\'envie en crever de dépit  !

-   [^100]:D\'après un petit livre publié en 1730 (*Lettre à
    Mylord \*\*\* sur Baron* et la *Demoiselle Le Couvreur...* par
    George Wink) et les éditeurs de 1734, Tartuffe disait
    primitivement : « Ô Ciel, pardonne-lui comme je lui pardonne ! »,
    ou, comme l\'indique Voltaire dans son Sommaire de Tartuffe : « Ô
    Ciel, pardonne-moi comme je lui pardonne ! »
-   [^101]:*De ma foi* : de ma fidélité à votre égard.
-   [^102]:*Sans doute* : sans aucun doute, assurément.
