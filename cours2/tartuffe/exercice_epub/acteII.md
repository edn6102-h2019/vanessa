## ACTE II.

### SCÈNE I

\- Orgon, Mariane.

#### ORGON. {#orgon. .orgon}

Mariane.

#### MARIANE. {#mariane. .mariane}

Mon père.

#### ORGON. {#orgon.-1 .orgon}

Approchez, j\'ai de quoi\
Vous parler en secret.

#### MARIANE. {#mariane.-1 .mariane}

Que cherchez-vous ?

#### ORGON. {#orgon.-2 .orgon}

*(Il regarde dans un petit cabinet.)*\
Je vois\
Si quelqu\'un n\'est point là qui pourroit nous entendre ;\
Car ce petit endroit est propre pour surprendre.\
Or sus, nous voilà bien. J\'ai, Mariane, en vous\
Reconnu de tout temps un esprit assez doux,\
Et de tout temps aussi vous m\'avez été chère.

#### MARIANE. {#mariane.-2 .mariane}

Je suis fort redevable à cet amour de père.

#### ORGON. {#orgon.-3 .orgon}

C\'est fort bien dit, ma fille ; et pour le mériter\
Vous devez n\'avoir soin que de me contenter.

#### MARIANE. {#mariane.-3 .mariane}

C\'est où je mets aussi ma gloire la plus haute.

#### ORGON. {#orgon.-4 .orgon}

Fort bien. Que dites-vous de Tartuffe notre hôte ?

#### MARIANE. {#mariane.-4 .mariane}

Qui, moi ?

#### ORGON. {#orgon.-5 .orgon}

Vous. Voyez bien comme vous répondrez.

#### MARIANE. {#mariane.-5 .mariane}

Hélas[^42] ! j\'en dirai, moi, tout ce que vous voudrez.

#### ORGON. {#orgon.-6 .orgon}

C\'est parler sagement. Dites-moi donc, ma fille,\
Qu\'en toute sa personne un haut mérite brille,\
Qu\'il touche votre coeur, et qu\'il vous seroit doux\
De le voir par mon choix devenir votre époux.\
Eh ?\
*(Mariane se recule avec surprise.)*

#### MARIANE. {#mariane.-6 .mariane}

Eh ?

#### ORGON. {#orgon.-7 .orgon}

Qu\'est-ce ?

#### MARIANE. {#mariane.-7 .mariane}

Plaît-il ?

#### ORGON. {#orgon.-8 .orgon}

Quoi ?

#### MARIANE. {#mariane.-8 .mariane}

Me suis-je méprise ?

#### ORGON. {#orgon.-9 .orgon}

Comment ?

#### MARIANE. {#mariane.-9 .mariane}

Qui voulez-vous, mon père, que je dise\
Qui me touche le coeur, et qu\'il me seroit doux\
De voir par votre choix devenir mon époux ?

#### ORGON. {#orgon.-10 .orgon}

Tartuffe.

#### MARIANE. {#mariane.-10 .mariane}

Il n\'en est rien, mon père, je vous jure.\
Pourquoi me faire dire une telle imposture ?

#### ORGON. {#orgon.-11 .orgon}

Mais je veux que cela soit une vérité,\
Et c\'est assez pour vous que je l\'aie arrêté.

#### MARIANE. {#mariane.-11 .mariane}

Quoi ? vous voulez, mon père\... ?

#### ORGON. {#orgon.-12 .orgon}

Oui, je prétends, ma fille,\
Unir par votre hymen[^43] Tartuffe à ma famille.\
Il sera votre époux, j\'ai résolu cela ;\
Et comme sur vos voeux je\...

-   [^42]:*Hélas* : il arrive que cette interjection ne marque pas
    le regret ni la douleur. (Cf. Les Femmes savantes, IV, 5, v. 1447 :
    « Hélas ! dans cette humeur conservez-le toujours ! »).
-   [^43]:*Votre\'hymen* : votre mariage.

## ACTE II.

### SCÈNE II

\- Dorine, Orgon, Mariane.

#### ORGON. {#orgon.-13 .orgon}

Que faites-vous là ?\
La curiosité qui vous presse est bien forte,\
Mamie, à nous venir écouter de la sorte[^44].

#### DORINE. {#dorine. .dorine}

Vraiment, je ne sais pas si c\'est un bruit qui part\
De quelque conjecture, ou d\'un coup de hasard ;\
Mais de ce mariage on m\'a dit la nouvelle,\
Et j\'ai traité cela de pure bagatelle.

#### ORGON. {#orgon.-14 .orgon}

Quoi donc ? la chose est-elle incroyable ?

#### DORINE. {#dorine.-1 .dorine}

À tel point,\
Que vous-même, Monsieur, je ne vous en crois point.

#### ORGON. {#orgon.-15 .orgon}

Je sais bien le moyen de vous le faire croire.

#### DORINE. {#dorine.-2 .dorine}

Oui, oui, vous nous contez une plaisante histoire.

#### ORGON. {#orgon.-16 .orgon}

Je conte justement ce qu\'on verra dans peu.

#### DORINE. {#dorine.-3 .dorine}

Chansons !

#### ORGON. {#orgon.-17 .orgon}

Ce que je dis, ma fille, n\'est point jeu.

#### DORINE. {#dorine.-4 .dorine}

Allez, ne croyez point à Monsieur votre père :\
Il raille.

#### ORGON. {#orgon.-18 .orgon}

Je vous dis\...

#### DORINE. {#dorine.-5 .dorine}

Non, vous avez beau faire,\
On ne vous croira point.

#### ORGON. {#orgon.-19 .orgon}

À la fin mon courroux\...

#### DORINE. {#dorine.-6 .dorine}

Hé bien ! on vous croit donc, et c\'est tant pis pour vous.\
Quoi ? se peut-il, Monsieur, qu\'avec l\'air d\'homme sage\
Et cette large barbe au milieu du visage,\
Vous soyez assez fou pour vouloir\... ?

#### ORGON. {#orgon.-20 .orgon}

Écoutez :\
Vous avez pris céans certaines privautés\
Qui ne me plaisent point  ; je vous le dis, mamie.

#### DORINE. {#dorine.-7 .dorine}

Parlons sans nous fâcher, Monsieur, je vous supplie.\
Vous moquez-vous des gens d\'avoir fait ce complot ?\
Votre fille n\'est point l\'affaire d\'un bigot :\
Il a d\'autres emplois auxquels il faut qu\'il pense.\
Et puis, que vous apporte une telle alliance ?\
À quel sujet aller, avec tout votre bien,\
Choisir une gendre gueux  ?\...

#### ORGON. {#orgon.-21 .orgon}

Taisez-vous. S\'il n\'a rien,\
Sachez que c\'est par là qu\'il faut qu\'on le révère.\
Sa misère est sans doute[^45] une honnête misère ;\
Au-dessus des grandeurs elle doit l\'élever,\
Puisqu\'enfin de son bien il s\'est laissé priver\
Par son trop peu de soin des choses temporelles,\
Et sa puissante attache aux choses éternelles.\
Mais mon secours pourra lui donner les moyens\
De sortir d\'embarras et rentrer dans ses biens :\
Ce sont fiefs qu\'à bon titre au pays on renomme  ;\
Et tel que l\'on le voit, il est bien gentilhomme[^46].

#### DORINE. {#dorine.-8 .dorine}

Oui, c\'est lui qui le dit ; et cette vanité,\
Monsieur, ne sied pas bien avec la piété.\
Qui d\'une sainte vie embrasse l\'innocence\
Ne doit point tant prôner son nom et sa naissance,\
Et l\'humble procédé de la dévotion\
Souffre mal les éclats de cette ambition.\
A quoi bon cet orgueil ?\... Mais ce discours vous blesse :\
Parlons de sa personne, et laissons sa noblesse.\
Ferez-vous possesseur, sans quelque peu d\'ennui,\
D\'une fille comme elle un homme comme lui ?\
Et ne devez-vous pas songer aux bienséances,\
Et de cette union prévoir les conséquences ?\
Sachez que d\'une fille on risque la vertu,\
Lorsque dans son hymen son goût est combattu,\
Que le dessein d\'y vivre en honnête personne\
Dépend des qualités du mari qu\'on lui donne,\
Et que ceux dont partout on montre au doigt le front\
Font leurs femmes souvent ce qu\'on voit qu\'elles sont.\
Il est bien difficile enfin d\'être fidèle\
A de certains maris faits d\'un certain modèle ;\
Et qui donne à sa fille un homme qu\'elle hait\
Est responsable au Ciel des fautes qu\'elle fait.\
Songez à quels périls votre dessein vous livre.

#### ORGON. {#orgon.-22 .orgon}

Je vous dis qu\'il me faut apprendre d\'elle à vivre.

#### DORINE. {#dorine.-9 .dorine}

Vous n\'en feriez que mieux de suivre mes leçons.

#### ORGON. {#orgon.-23 .orgon}

Ne nous amusons point, ma fille, à ces chansons :\
Je sais ce qu\'il vous faut, et je suis votre père.\
J\'avois donné pour vous ma parole à Valère ;\
Mais outre qu\'à jouer on dit qu\'il est enclin,\
Je le soupçonne encor d\'être un peu libertin[^47] :\
Je ne remarque point qu\'il hante les églises.

#### DORINE. {#dorine.-10 .dorine}

Voulez-vous qu\'il y coure à vos heures précises,\
Comme ceux qui n\'y vont que pour être aperçus ?

#### ORGON. {#orgon.-24 .orgon}

Je ne demande pas votre avis là-dessus.\
Enfin avec le Ciel l\'autre est le mieux du monde,\
Et c\'est une richesse à nulle autre seconde.\
Cet hymen de tous biens comblera vos désirs,\
Il sera tout confit en douceurs et plaisirs[^48].\
Ensemble vous vivrez, dans vos ardeurs fidèles,\
Comme deux vrais enfants, comme deux tourterelles ;\
A nul fâcheux débat jamais vous n\'en viendrez,\
Et vous ferez de lui tout ce que vous voudrez.

#### DORINE. {#dorine.-11 .dorine}

Elle ? elle n\'en fera qu\'un sot[^49], je vous assure.

#### ORGON. {#orgon.-25 .orgon}

Ouais ! quels discours !

#### DORINE. {#dorine.-12 .dorine}

Je dis qu\'il en a l\'encolure,\
Et que son ascendant[^50], Monsieur, l\'emportera\
Sur toute la vertu que votre fille aura.

#### ORGON. {#orgon.-26 .orgon}

Cessez de m\'interrompre, et songez à vous taire,\
Sans mettre votre nez où vous n\'avez que faire.

#### DORINE. {#dorine.-13 .dorine}

Je n\'en parle, Monsieur, que pour votre intérêt.\
*(Elle l\'interrompt toujours au moment qu\'il se retourne\
pour parler à sa fille.)*

#### ORGON. {#orgon.-27 .orgon}

C\'est prendre trop de soin : taisez-vous, s\'il vous plaît.

#### DORINE. {#dorine.-14 .dorine}

Si l\'on ne vous aimoit\...

#### ORGON. {#orgon.-28 .orgon}

Je ne veux pas qu\'on m\'aime.

#### DORINE. {#dorine.-15 .dorine}

Et je veux vous aimer, Monsieur, malgré vous-même.

#### ORGON. {#orgon.-29 .orgon}

Ah !

#### DORINE. {#dorine.-16 .dorine}

Votre honneur m\'est cher, et je ne puis souffrir\
Qu\'aux brocards d\'un chacun vous alliez vous offrir.

#### ORGON. {#orgon.-30 .orgon}

Vous ne vous tairez point ?

#### DORINE. {#dorine.-17 .dorine}

C\'est une conscience[^51]\
Que de vous laisser faire une telle alliance.

#### ORGON. {#orgon.-31 .orgon}

Te tairas-tu, serpent, dont les traits effrontés\... ?

#### DORINE. {#dorine.-18 .dorine}

Ah ! vous êtes dévot, et vous vous emportez ?

#### ORGON. {#orgon.-32 .orgon}

Oui, ma bile s\'échauffe à toutes ces fadaises,\
Et tout résolument je veux que tu te taises.

#### DORINE. {#dorine.-19 .dorine}

Soit. Mais, ne disant mot, je n\'en pense pas moins.

#### ORGON. {#orgon.-33 .orgon}

Pense, si tu le veux, mais applique tes soins\
A ne m\'en point parler, ou\... : suffit.\
*(Se retournant vers sa fille.)*\
Comme sage,\
J\'ai pesé mûrement toutes choses.

#### DORINE. {#dorine.-20 .dorine}

J\'enrage\
De ne pouvoir parler.\
*(Elle se tait lorsqu\'il tourne la tête.)*

#### ORGON. {#orgon.-34 .orgon}

Sans être damoiseau,\
Tartuffe est fait de sorte\...

#### DORINE. {#dorine.-21 .dorine}

Oui, c\'est un beau museau.

#### ORGON. {#orgon.-35 .orgon}

Que quand tu n\'aurois même aucune sympathie\
Pour tous les autres dons\...\
*(Il se tourne devant elle, et la regarde les bras croisés.)*

#### DORINE. {#dorine.-22 .dorine}

La voilà bien lotie !\
Si j\'étois en sa place, un homme assurément\
Ne m\'épouseroit pas de force impunément ;\
Et je lui ferois voir bientôt après la fête\
Qu\'une femme a toujours une vengeance prête.

#### ORGON. {#orgon.-36 .orgon}

Donc de ce que je dis on ne fera nul cas ?

#### DORINE. {#dorine.-23 .dorine}

De quoi vous plaignez-vous ? Je ne vous parle pas.

#### ORGON. {#orgon.-37 .orgon}

Qu\'est-ce que tu fais donc ?

#### DORINE. {#dorine.-24 .dorine}

Je me parle à moi-même.

#### ORGON. {#orgon.-38 .orgon}

Fort bien. Pour châtier son insolence extrême,\
Il faut que je lui donne un revers de ma main.\
*(Il se met en posture de lui donner un soufflet ;\
et Dorine, à chaque coup d\'oeil qu\'il jette,\
se tient droite sans parler.)*\
Ma fille, vous devez approuver mon dessein\...\
Croire que le mari\...que j\'ai su vous élire[^52]\...\
Que ne te parles-tu ?

#### DORINE. {#dorine.-25 .dorine}

Je n\'ai rien à me dire.

#### ORGON. {#orgon.-39 .orgon}

Encore un petit mot.

#### DORINE. {#dorine.-26 .dorine}

Il ne me plaît pas, moi.

#### ORGON. {#orgon.-40 .orgon}

Certes, je t\'y guettois.

#### DORINE. {#dorine.-27 .dorine}

Quelque sotte, ma foi[^53] !

#### ORGON. {#orgon.-41 .orgon}

Enfin, ma fille, il faut payer d\'obéissance[^54],\
Et montrer pour mon choix entière déférence.

#### DORINE, *en s\'enfuyant*. {#dorine-en-senfuyant. .dorine}

Je me moquerois fort[^55] de prendre un tel époux.\
*(Il lui veut donner un soufflet et la manque.)*

#### ORGON. {#orgon.-42 .orgon}

Vous avez là, ma fille, une peste avec vous,\
Avec qui sans péché je ne saurois plus vivre.\
Je me sens hors d\'état maintenant de poursuivre :\
Ses discours insolents m\'ont mis l\'esprit en feu,\
Et je vais prendre l\'air pour me rasseoir[^56] un peu.

-   [^44]:Selon l\'édition de 1734, c\'est juste après le vers 440
    prononcé par Mariane, que Dorine est entrée doucement et s\'est mise
    derrière Orgon sans être vue de lui.
-   [^45]:*Sans doute* : sans aucun doute.
-   [^46]:*Il est bien gentilhomme* : être gentilhomme, c\'est
    jouir de cette condition par la naissance, et non pas par
    l\'exercice d\'une charge ou par la grâce du roi.
-   [^47]:*Un peu libertin* : il ne s\'agit pas ici de libre-pensée
    caractérisée, mais de manque de respect pour tout ce qui touche à la
    religion.
-   [^48]:VAR. Et sera tout confit en douceurs, et plaisirs.
    (1682).
-   [^49]:*Un sot* : un mari trompé.
-   [^50]:*Son ascendant* : l\'influence que les astres exercent
    sur lui (cf. L\'Ecole des maris, v. 1099).
-   [^51]:*C\'est une conscience* : c\'est une affaire de
    conscience. C\'est-à-dire : « C\'est un devoir de ne pas vous
    laisser conclure une telle alliance. »
-   [^52]:*Élire* : choisir.
-   [^53]:*Quelque sotte* : seule une sotte ferait cela.
-   [^54]:*Payer d\'obéissance* : faire preuve d\'obéissance.
-   [^55]:*Je me moquerais fort* : je me garderais bien, comme
    d\'une chose ridicule, de prendre un tel époux.
-   [^56]:*Me rasseoir* : me remettre, me calmer.

## ACTE II.

### SCÈNE III

\- Dorine, Mariane.

#### DORINE. {#dorine.-28 .dorine}

Avez-vous donc perdu, dites-moi, la parole,\
Et faut-il qu\'en ceci je fasse votre rôle ?\
Souffrir qu\'on vous propose un projet insensé,\
Sans que du moindre mot vous l\'ayez repoussé !

#### MARIANE. {#mariane.-12 .mariane}

Contre un père absolu que veux-tu que je fasse ?

#### DORINE. {#dorine.-29 .dorine}

Ce qu\'il faut pour parer une telle menace.

#### MARIANE. {#mariane.-13 .mariane}

Quoi ?

#### DORINE. {#dorine.-30 .dorine}

Lui dire qu\'un coeur n\'aime point par autrui,\
Que vous vous mariez pour vous, non pas pour lui,\
Qu\'étant celle pour qui se fait toute l\'affaire,\
C\'est à vous, non à lui, que le mari doit plaire,\
Et que si son Tartuffe est pour lui si charmant,\
Il le peut épouser sans nul empêchement.

#### MARIANE. {#mariane.-14 .mariane}

Un père, je l\'avoue, a sur nous tant d\'empire,\
Que je n\'ai jamais eu la force de rien dire.

#### DORINE. {#dorine.-31 .dorine}

Mais raisonnons. Valère a fait pour vous des pas[^57] :\
L\'aimez-vous, je vous prie, ou ne l\'aimez-vous pas ?

#### MARIANE. {#mariane.-15 .mariane}

Ah ! qu\'envers mon amour ton injustice est grande,\
Dorine ! me dois-tu faire cette demande ?\
T\'ai-je pas là-dessus ouvert cent fois mon coeur,\
Et sais-tu pas pour lui jusqu\'où va mon ardeur ?

#### DORINE. {#dorine.-32 .dorine}

Que sais-je si le coeur a parlé par la bouche,\
Et si c\'est tout de bon que cet amant vous touche ?

#### MARIANE. {#mariane.-16 .mariane}

Tu me fais un grand tort, Dorine, d\'en douter,\
Et mes vrais sentiments ont su trop éclater.

#### DORINE. {#dorine.-33 .dorine}

Enfin, vous l\'aimez donc ?

#### MARIANE. {#mariane.-17 .mariane}

Oui, d\'une ardeur extrême.

#### DORINE. {#dorine.-34 .dorine}

Et selon l\'apparence il vous aime de même ?

#### MARIANE. {#mariane.-18 .mariane}

Je le crois.

#### DORINE. {#dorine.-35 .dorine}

Et tous deux brûlez également\
De vous voir mariés ensemble ?

#### MARIANE. {#mariane.-19 .mariane}

Assurément.

#### DORINE. {#dorine.-36 .dorine}

Sur cette autre union quelle est donc votre attente  ?

#### MARIANE. {#mariane.-20 .mariane}

De me donner la mort si l\'on me violente.

#### DORINE. {#dorine.-37 .dorine}

Fort bien : c\'est un recours où je ne songeois pas ;\
Vous n\'avez qu\'à mourir pour sortir d\'embarras ;\
Le remède sans doute est merveilleux. J\'enrage\
Lorsque j\'entends tenir ces sortes de langage.

#### MARIANE. {#mariane.-21 .mariane}

Mon Dieu ! de quelle humeur, Dorine, tu te rends  !\
Tu ne compatis point aux déplaisirs des gens.

#### DORINE. {#dorine.-38 .dorine}

Je ne compatis point à qui dit des sornettes\
Et dans l\'occasion[^58] mollit comme vous faites.

#### MARIANE. {#mariane.-22 .mariane}

Mais que veux-tu ? si j\'ai de la timidité.

#### DORINE. {#dorine.-39 .dorine}

Mais l\'amour dans un coeur veut de la fermeté.

#### MARIANE. {#mariane.-23 .mariane}

Mais n\'en gardé-je pas pour les feux de Valère ?\
Et n\'est-ce pas à lui de m\'obtenir d\'un père ?

#### DORINE. {#dorine.-40 .dorine}

Mais quoi ? si votre père est un bourru[^59] fieffé,\
Qui s\'est de son Tartuffe entièrement coiffé[^60],\
Et manque à l\'union qu\'il avoit arrêtée,\
La faute à votre amant doit-elle être imputée ?

#### MARIANE. {#mariane.-24 .mariane}

Mais par un haut refus et d\'éclatants mépris\
Ferai-je dans mon choix voir un coeur trop épris  ?\
Sortirai-je pour lui, quelque éclat dont il brille,\
De la pudeur du sexe et du devoir de fille  ?\
Et veux-tu que mes feux par le monde étalés\... ?

#### DORINE. {#dorine.-41 .dorine}

Non, non, je ne veux rien. Je vois que vous voulez\
Être à Monsieur Tartuffe, et j\'aurois, quand j\'y pense,\
Tort de vous détourner d\'une telle alliance.\
Quelle raison aurois-je à combattre vos voeux ?\
Le parti de soi-même est fort avantageux.\
Monsieur Tartuffe ! oh ! oh ! n\'est-ce rien qu\'on propose ?\
Certes Monsieur Tartuffe, à bien prendre la chose,\
N\'est pas un homme, non, qui se mouche du pié[^61],\
Et ce n\'est pas peu d\'heur[^62] que d\'être sa moitié.\
Tout le monde déjà de gloire le couronne ;\
Il est noble chez lui[^63], bien fait de sa personne ;\
Il a l\'oreille rouge et le teint bien fleuri  :\
Vous vivrez trop contente avec un tel mari[^64].

#### MARIANE. {#mariane.-25 .mariane}

Mon Dieu !\...

#### DORINE. {#dorine.-42 .dorine}

Quelle allégresse aurez-vous dans votre âme,\
Quand d\'un époux si beau vous vous verrez la femme !

#### MARIANE. {#mariane.-26 .mariane}

Ha ! cesse, je te prie, un semblable discours,\
Et contre cet hymen ouvre-moi du secours.\
C\'en est fait, je me rends, et suis prête à tout faire.

#### DORINE. {#dorine.-43 .dorine}

Non, il faut qu\'une fille obéisse à son père,\
Voulût-il lui donner un singe pour époux.\
Votre sort est fort beau : de quoi vous plaignez-vous ?\
Vous irez par le coche en sa petite ville,\
Qu\'en oncles et cousins vous trouverez fertile,\
Et vous vous plairez fort à les entretenir.\
D\'abord chez le beau monde on vous fera venir  ;\
Vous irez visiter, pour votre bienvenue,\
Madame la baillive, et Madame l\'élue[^65],\
Qui d\'un siège pliant[^66] vous feront honorer.\
Là, dans le carnaval, vous pourrez espérer\
Le bal et la grand\'bande[^67], à savoir, deux musettes,\
Et parfois, Fagotin[^68], et les marionnettes,\
Si pourtant votre époux\...

#### MARIANE. {#mariane.-27 .mariane}

Ah ! tu me fais mourir. De tes conseils plutôt songe à me secourir.

#### DORINE. {#dorine.-44 .dorine}

Je suis votre servante.

#### MARIANE. {#mariane.-28 .mariane}

Eh ! Dorine, de grâce\...

#### DORINE. {#dorine.-45 .dorine}

Il faut, pour vous punir, que cette affaire passe.

#### MARIANE. {#mariane.-29 .mariane}

Ma pauvre fille !

#### DORINE. {#dorine.-46 .dorine}

Non.

#### MARIANE. {#mariane.-30 .mariane}

Si mes voeux déclarés[^69]\...

#### DORINE. {#dorine.-47 .dorine}

Point : Tartuffe est votre homme, et vous en tâterez.

#### MARIANE. {#mariane.-31 .mariane}

Tu sais qu\'à toi toujours je me suis confiée : Fais-moi\...

#### DORINE. {#dorine.-48 .dorine}

Non, vous serez, ma foi ! tartuffiée.

#### MARIANE. {#mariane.-32 .mariane}

Hé bien ! puisque mon sort ne sauroit t\'émouvoir,\
Laisse-moi désormais toute à mon désespoir :\
C\'est de lui que mon coeur empruntera de l\'aide,\
Et je sais de mes maux l\'infaillible remède.\
*(Elle veut s\'en aller.)*

#### DORINE. {#dorine.-49 .dorine}

Hé ! là, là, revenez. Je quitte mon courroux.\
Il faut, nonobstant tout, avoir pitié de vous.

#### MARIANE. {#mariane.-33 .mariane}

Vois-tu, si l\'on m\'expose à ce cruel martyre,\
Je te le dis, Dorine, il faudra que j\'expire.

#### DORINE. {#dorine.-50 .dorine}

Ne vous tourmentez point. On peut adroitement\
Empêcher\... Mais voici Valère, votre amant.

-   [^57]:*A fait pour vous des pas* : vous a demandé en mariage.
-   [^58]:*Dans l\'occasion* : au combat, dans la bataille. Le sens
    militaire de l\'expression est ici confirmé par l\'emploi de mollir
    (manquer de courage, faillir, fléchir).
-   [^59]:*Bourru* : « fantasque, bizarre, extragant »
    (Dictionnaire de l\'Académie, 1694).
-   [^60]:*Être coiffé* de quelqu\'un, c\'est être entiché de
    quelqu\'un, ne jurer que par lui.
-   [^61]:« On dit d\'un homme habile et difficile à surprendre
    qu\'il ne se mouche pas du pied » (Dictionnaire de Furetière, 1690).
-   [^62]:*Heur*, pour bonheur, commence à être un archaïsme
    après 1660.
-   [^63]:*Chez lui* : c\'est là une restriction significative (cf.
    ce que dit Orgon aux vers 493-494).
-   [^64]:*Avec un tel mari* : Tartuffe est un sanguin, selon la
    théorie des humeurs\*, et les sanguins passaient pour
    particulièrement doués pour l\'amour.
-   [^65]:Le *bailli* a des attributions judiciaires dans une
    petite ville ; quant à l\'élu, il est chargé de trancher en première
    instance les contestations relatives à la répartition de certains
    impots.
-   [^66]:*D\'un siège pliant* : et non d\'une chaise ou d\'un
    fauteuil ; Mariane sera a peine reçue dans cette société
    provinciale.
-   [^67]:*La grand\'bande* : on appelait ainsi les vingt-quatre
    violons de la chambre du Roi.
-   [^68]:*Fagotin* : c\'était le nom du singe de Brioché, célèbre
    montreur de marionnettes au milieu du XVIIe siècle.
-   [^69]:*Si mes voeux déclarés...* : latinisme qui signifie: si
    le fait de déclarer l\'amour que je porte à Valère pouvait conjurer
    le sort...

## ACTE II.

### SCÈNE IV

\- Valère, Mariane, Dorine.

#### VALÈRE. {#valère. .valere}

On vient de débiter, Madame, une nouvelle\
Que je ne savois pas, et qui sans doute[^70] est belle.

#### MARIANE. {#mariane.-34 .mariane}

Quoi ?

#### VALÈRE. {#valère.-1 .valere}

Que vous épousez Tartuffe.

#### MARIANE. {#mariane.-35 .mariane}

Il est certain\
Que mon père s\'est mis en tête ce dessein.

#### VALÈRE. {#valère.-2 .valere}

Votre père, Madame\...

#### MARIANE. {#mariane.-36 .mariane}

A changé de visée :\
La chose vient par lui de m\'être proposée.

#### VALÈRE. {#valère.-3 .valere}

Quoi ? sérieusement ?

#### MARIANE. {#mariane.-37 .mariane}

Oui, sérieusement.\
Il s\'est pour cet hymen déclaré hautement.

#### VALÈRE. {#valère.-4 .valere}

Et quel est le dessein où votre âme s\'arrête,\
Madame ?

#### MARIANE. {#mariane.-38 .mariane}

Je ne sais.

#### VALÈRE. {#valère.-5 .valere}

La réponse est honnête. Vous ne savez ?

#### MARIANE. {#mariane.-39 .mariane}

Non.

#### VALÈRE. {#valère.-6 .valere}

Non ?

#### MARIANE. {#mariane.-40 .mariane}

Que me conseillez-vous ?

#### VALÈRE. {#valère.-7 .valere}

Je vous conseille, moi, de prendre cet époux.

#### MARIANE. {#mariane.-41 .mariane}

Vous me le conseillez ?

#### VALÈRE. {#valère.-8 .valere}

Oui.

#### MARIANE. {#mariane.-42 .mariane}

Tout de bon ?

#### VALÈRE. {#valère.-9 .valere}

Sans doute.\
  Le choix est glorieux, et vaut bien qu\'on l\'écoute.

#### MARIANE. {#mariane.-43 .mariane}

Hé bien ! c\'est un conseil, Monsieur, que je reçois.

#### VALÈRE. {#valère.-10 .valere}

Vous n\'aurez pas grand\'peine à le suivre, je crois.

#### MARIANE. {#mariane.-44 .mariane}

Pas plus qu\'à le donner en a souffert votre âme.

#### VALÈRE. {#valère.-11 .valere}

Moi, je vous l\'ai donné pour vous plaire, Madame.

#### MARIANE. {#mariane.-45 .mariane}

Et moi, je le suivrai pour vous faire plaisir.

#### DORINE. {#dorine.-51 .dorine}

Voyons ce qui pourra de ceci réussir[^71].

#### VALÈRE. {#valère.-12 .valere}

C\'est donc ainsi qu\'on aime ? Et c\'étoit tromperie\
Quand vous\...

#### MARIANE. {#mariane.-46 .mariane}

Ne parlons point de cela, je vous prie.\
Vous m\'avez dit tout franc que je dois accepter\
Celui que pour époux on me veut présenter :\
Et je déclare, moi, que je prétends le faire,\
Puisque vous m\'en donnez le conseil salutaire.

#### VALÈRE. {#valère.-13 .valere}

Ne vous excusez point sur mes intentions.\
Vous aviez pris déjà vos résolutions ;\
Et vous vous saisissez d\'un prétexte frivole\
Pour vous autoriser à manquer de parole.

#### MARIANE. {#mariane.-47 .mariane}

Il est vrai, c\'est bien dit.

#### VALÈRE. {#valère.-14 .valere}

Sans doute, et votre coeur\
N\'a jamais eu pour moi de véritable ardeur.

#### MARIANE. {#mariane.-48 .mariane}

Hélas ! permis à vous d\'avoir cette pensée.

#### VALÈRE. {#valère.-15 .valere}

Oui, oui, permis à moi ; mais mon âme offensée\
Vous préviendra peut-être en un pareil dessein ;\
Et je sais où porter et mes voeux et ma main.

#### MARIANE. {#mariane.-49 .mariane}

Ah ! je n\'en doute point ; et les ardeurs qu\'excite\
Le mérite\...

#### VALÈRE. {#valère.-16 .valere}

Mon Dieu, laissons là le mérite :\
J\'en ai fort peu, sans doute[^72], et vous en faites
foi.\
Mais j\'espère aux bontés qu\'une autre aura pour moi,\
Et j\'en sais de qui l\'âme, à ma retraite ouverte,\
Consentira sans honte à réparer ma perte.

#### MARIANE. {#mariane.-50 .mariane}

La perte n\'est pas grande  ; et de ce changement\
Vous vous consolerez assez facilement.

#### VALÈRE. {#valère.-17 .valere}

J\'y ferai mon possible, et vous le pouvez croire.\
Un coeur qui nous oublie engage notre gloire[^73] ;\
Il faut à l\'oublier mettre aussi tous nos soins :\
Si l\'on n\'en vient à bout, on le doit feindre au moins  ;\
Et cette lâcheté jamais ne se pardonne,\
De montrer de l\'amour pour qui nous abandonne.

#### MARIANE. {#mariane.-51 .mariane}

Ce sentiment, sans doute[^74], est noble et relevé.

#### VALÈRE. {#valère.-18 .valere}

Fort bien ; et d\'un chacun il doit être approuvé.\
Hé quoi ? vous voudriez qu\'à jamais dans mon âme\
Je gardasse pour vous les ardeurs de ma flamme,\
Et vous visse, à mes yeux, passer en d\'autres bras,\
Sans mettre ailleurs un coeur dont vous ne voulez pas ?

#### MARIANE. {#mariane.-52 .mariane}

Au contraire : pour moi, c\'est ce que je souhaite ;\
Et je voudrois déjà que la chose fût faite.

#### VALÈRE. {#valère.-19 .valere}

Vous le voudriez ?

#### MARIANE. {#mariane.-53 .mariane}

Oui.

#### VALÈRE. {#valère.-20 .valere}

C\'est assez m\'insulter,\
Madame, et de ce pas je vais vous contenter.\
*(Il fait un pas pour s\'en aller et revient toujours.)*

#### MARIANE. {#mariane.-54 .mariane}

Fort bien.

#### VALÈRE. {#valère.-21 .valere}

Souvenez-vous au moins que c\'est vous-même\
Qui contraignez mon coeur à cet effort extrême.

#### MARIANE. {#mariane.-55 .mariane}

Oui.

#### VALÈRE. {#valère.-22 .valere}

Et que le dessein que mon âme conçoit\
N\'est rien qu\'à votre exemple.

#### MARIANE. {#mariane.-56 .mariane}

À mon exemple, soit.

#### VALÈRE. {#valère.-23 .valere}

Suffit : vous allez être à point nommé servie.

#### MARIANE. {#mariane.-57 .mariane}

Tant mieux.

#### VALÈRE. {#valère.-24 .valere}

Vous me voyez, c\'est pour toute ma vie.

#### MARIANE. {#mariane.-58 .mariane}

À la bonne heure.

#### VALÈRE. {#valère.-25 .valere}

Euh ?\
*(Il s\'en va ; et lorsqu\'il est vers la porte, il se\
retourne.)*

#### MARIANE. {#mariane.-59 .mariane}

Quoi ?

#### VALÈRE. {#valère.-26 .valere}

Ne m\'appelez-vous pas ?

#### MARIANE. {#mariane.-60 .mariane}

Moi ? Vous rêvez.

#### VALÈRE. {#valère.-27 .valere}

Hé bien ! je poursuis donc mes pas.\
Adieu, Madame.

#### MARIANE. {#mariane.-61 .mariane}

Adieu, Monsieur.

#### DORINE. {#dorine.-52 .dorine}

Pour moi, je pense\
Que vous perdez l\'esprit par cette extravagance ;\
Et je vous ai laissé tout du long quereller,\
Pour voir où tout cela pourroit enfin aller.\
Holà ! seigneur Valère.\
*(Elle va l\'arrêter par le bras, et lui, fait mine\
de grande résistance.)*

#### VALÈRE. {#valère.-28 .valere}

Hé ! que veux-tu, Dorine ?

#### DORINE. {#dorine.-53 .dorine}

Venez ici.

#### VALÈRE. {#valère.-29 .valere}

Non, non, le dépit me domine.\
Ne me détourne point de ce qu\'elle a voulu.

#### DORINE. {#dorine.-54 .dorine}

Arrêtez.

#### VALÈRE. {#valère.-30 .valere}

Non, vois-tu ? c\'est un point résolu.

#### DORINE. {#dorine.-55 .dorine}

Ah !

#### MARIANE. {#mariane.-62 .mariane}

Il souffre à me voir, ma présence le chasse,\
Et je ferai bien mieux de lui quitter la place.

#### DORINE. *Elle quitte Valère et court à Mariane.* {#dorine.-elle-quitte-valère-et-court-à-mariane. .dorine}

À l\'autre. Où courez-vous ?

#### MARIANE. {#mariane.-63 .mariane}

Laisse.

#### DORINE. {#dorine.-56 .dorine}

Il faut revenir.

#### MARIANE. {#mariane.-64 .mariane}

Non, non, Dorine ; en vain tu veux me retenir.

#### VALÈRE. {#valère.-31 .valere}

Je vois bien que ma vue est pour elle un supplice,\
Et sans doute il vaut mieux que je l\'en affranchisse.

#### DORINE. *Elle quitte Mariane et court à Valère.* {#dorine.-elle-quitte-mariane-et-court-à-valère. .dorine}

Encor ! Diantre soit fait de vous si je le veux[^75] !\
Cessez ce badinage, et venez çà tous deux.\
*(Elle les tire l\'un et l\'autre.)*

#### VALÈRE. {#valère.-32 .valere}

Mais quel est ton dessein ?

#### MARIANE. {#mariane.-65 .mariane}

Qu\'est-ce que tu veux faire ?

#### DORINE. {#dorine.-57 .dorine}

Vous bien remettre ensemble, et vous tirer d\'affaire.\
Êtes-vous fou d\'avoir un pareil démêlé ?

#### VALÈRE. {#valère.-33 .valere}

N\'as-tu pas entendu comme elle m\'a parlé ?

#### DORINE. {#dorine.-58 .dorine}

Êtes-vous folle, vous, de vous être emportée ?

#### MARIANE. {#mariane.-66 .mariane}

N\'as-tu pas vu la chose, et comme il m\'a traitée ?

#### DORINE. {#dorine.-59 .dorine}

Sottise des deux parts. Elle n\'a d\'autre soin\
Que de se conserver à vous, j\'en suis témoin.\
Il n\'aime que vous seule, et n\'a point d\'autre envie\
Que d\'être votre époux ; j\'en réponds sur ma vie.

#### MARIANE. {#mariane.-67 .mariane}

Pourquoi donc me donner un semblable conseil ?

#### VALÈRE. {#valère.-34 .valere}

Pourquoi m\'en demander sur un sujet pareil ?

#### DORINE. {#dorine.-60 .dorine}

Vous êtes fous tous deux. Çà, la main l\'un et l\'autre.\
Allons, vous.

#### VALÈRE, *en donnant sa main à Dorine.* {#valère-en-donnant-sa-main-à-dorine. .valere}

À quoi bon ma main ?

#### DORINE. {#dorine.-61 .dorine}

Ah ! Çà la vôtre.

#### MARIANE, *en donnant aussi sa main.* {#mariane-en-donnant-aussi-sa-main. .mariane}

De quoi sert tout cela ?

#### DORINE. {#dorine.-62 .dorine}

Mon Dieu ! vite, avancez.\
Vous vous aimez tous deux plus que vous ne pensez.

#### VALÈRE. {#valère.-35 .valere}

Mais ne faites donc point les choses avec gêne,\
Et regardez un peu les gens sans nulle haine.\
*(Mariane tourne l\'oeil vers Valère et fait un petit sourire.)*

#### DORINE. {#dorine.-63 .dorine}

À vous dire le vrai, les amants sont bien fous !

#### VALÈRE. {#valère.-36 .valere}

Ho çà n\'ai-je pas lieu de me plaindre de vous ?\
Et pour n\'en point mentir, n\'êtes-vous pas méchante\
De vous plaire à me dire une chose affligeante ?

#### MARIANE. {#mariane.-68 .mariane}

Mais vous, n\'êtes-vous pas l\'homme le plus ingrat\... ?

#### DORINE. {#dorine.-64 .dorine}

Pour une autre saison laissons tout ce débat,\
Et songeons à parer ce fâcheux mariage.

#### MARIANE. {#mariane.-69 .mariane}

Dis-nous donc quels ressorts il faut mettre en usage.

#### DORINE. {#dorine.-65 .dorine}

Nous en ferons agir de toutes les façons.\
Votre père se moque, et ce sont des chansons ;\
Mais pour vous, il vaut mieux qu\'à son extravagance\
D\'un doux consentement vous prêtiez l\'apparence,\
Afin qu\'en cas d\'alarme il vous soit plus aisé\
De tirer en longueur cet hymen proposé.\
En attrapant du temps, à tout on remédie.\
Tantôt vous payerez de quelque maladie[^76],\
Qui viendra tout à coup et voudra des délais ;\
Tantôt vous payerez de présages mauvais :\
Vous aurez fait d\'un mort la rencontre fâcheuse,\
Cassé quelque miroir, ou songé d\'eau bourbeuse.\
Enfin le bon de tout, c\'est qu\'à d\'autres qu\'à lui\
On ne vous peut lier, que vous ne disiez « oui »[^77].\
Mais pour mieux réussir, il est bon, ce me semble,\
Qu\'on ne vous trouve point tous deux parlant ensemble.\
*(À Valère.)*\
Sortez, et sans tarder employez vos amis,\
Pour vous faire tenir ce qu\'on vous a promis.\
Nous allons réveiller les efforts de son frère,\
Et dans votre parti jeter la belle-mère[^78].\
Adieu.

#### VALÈRE, *à Mariane.* {#valère-à-mariane. .valere}

Quelques efforts que nous préparions tous,\
Ma plus grande espérance, à vrai dire, est en vous.

#### MARIANE, *à Valère.* {#mariane-à-valère. .mariane}

Je ne vous répons pas des volontés d\'un père ;\
Mais je ne serai point à d\'autre qu\'à Valère.

#### VALÈRE. {#valère.-37 .valere}

Que vous me comblez d\'aise ! Et quoi que puisse oser\...

#### DORINE. {#dorine.-66 .dorine}

Ah ! jamais les amants ne sont las de jaser.\
Sortez, vous dis-je.

#### VALÈRE. *Il fait un pas et revient.* {#valère.-il-fait-un-pas-et-revient. .valere}

Enfin\...

#### DORINE. {#dorine.-67 .dorine}

Quel caquet est le vôtre !\
Tirez[^79] de cette part ; et vous, tirez de l\'autre.\
*(Les poussant chacun par l\'épaule.)*

-   [^70]:*Sans doute* : sans aucun doute.
-   [^71]:*Réussir* : arriver.
-   [^72]:*Sans doute* : sans aucun doute.
-   [^73]:*Engage notre gloire* : met en cause notre fierté.
-   [^74]:*Sans doute* : sans aucun doute. Assurément.
-   [^75]:*Diantre soit fait de vous si je le veux !* : le diable
    vous emporte si je consens à vous laisser partir.
-   [^76]:*Vous payerez de quelque maladie* : vous prétexterez
    quelque maladie.
-   [^77]:*Que vous disiez « oui »* : si vous ne dites pas « oui ».
-   [^78]:V. 813-814 : Nous allons réveiller les efforts de son
    frère (nous dirions aujourd\'hui *beau-frère*) Cléante, et jeter
    Elmire, la belle-mère de Mariane, dans notre parti. Ces deux vers
    s\'adressent à Mariane, comme le souligne l\'édition de 1734.
-   [^79]:*Tirez* : allez, partez.
