ACTE I.
-------

### SC&Egrave;NE PREMI&Egrave;RE

\- Madame Pernelle et Flipote *sa servante*, Elmire, Mariane, Dorine,
Damis, Cl&eacute;ante.

#### MADAME PERNELLE. {#madame-pernelle. .pernelle}

Allons, Flipote, allons, que d\'eux je me d&eacute;livre.

#### ELMIRE. {#elmire. .elmire}

Vous marchez d\'un tel pas qu\'on a peine &agrave; vous suivre.

#### MADAME PERNELLE. {#madame-pernelle.-1 .pernelle}

Laissez, ma bru, laissez, ne venez pas plus loin&nbsp;:\
Ce sont toutes fa&ccedil;ons dont je n\'ai pas besoin.

#### ELMIRE. {#elmire.-1 .elmire}

De ce que l\'on vous doit envers vous on s\'acquitte,\
Mais ma m&egrave;re, d\'o&ugrave; vient que vous sortez si vite&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-2 .pernelle}

C\'est que je ne puis voir tout ce m&eacute;nage-ci,\
Et que de me complaire on ne prend nul souci.\
Oui, je sors de chez vous fort mal &eacute;difi&eacute;e&nbsp;:\
Dans toutes mes le&ccedil;ons j\'y suis contrari&eacute;e,\
On n\'y respecte rien, chacun y parle haut,\
Et c\'est tout justement la cour du roi P&eacute;taut.[^1]

#### DORINE. {#dorine. .dorine}

Si\...

#### MADAME PERNELLE. {#madame-pernelle.-3 .pernelle}

Vous &ecirc;tes, mamie, une fille suivante\
Un peu trop forte en gueule, et fort impertinente&nbsp;:\
Vous vous m&ecirc;lez sur tout de dire votre avis.

#### DAMIS. {#damis. .damis}

Mais\...

#### MADAME PERNELLE. {#madame-pernelle.-4 .pernelle}

Vous &ecirc;tes un sot en trois lettres, mon fils.\
C\'est moi qui vous le dis, qui suis votre grand\'m&egrave;re&nbsp;;\
Et j\'ai pr&eacute;dit cent fois &agrave; mon fils, votre p&egrave;re,\
Que vous preniez tout l\'air d\'un m&eacute;chant garnement,\
Et ne lui donneriez jamais que du tourment.

#### MARIANE. {#mariane. .mariane}

Je crois\...

#### MADAME PERNELLE. {#madame-pernelle.-5 .pernelle}

Mon Dieu, sa soeur, vous faites la discrette,\
Et vous n\'y touchez pas, tant vous semblez doucette&nbsp;;\
Mais il n\'est, comme on dit, pire eau que l\'eau qui dort,\
Et vous menez sous chape un train que je hais fort.

#### ELMIRE. {#elmire.-2 .elmire}

Mais, ma m&egrave;re,\...

#### MADAME PERNELLE. {#madame-pernelle.-6 .pernelle}

Ma bru, qu\'il ne vous en d&eacute;plaise,\
Votre conduite en tout est tout &agrave; fait mauvaise&nbsp;;\
Vous devriez leur mettre un bon exemple aux yeux,\
Et leur d&eacute;funte m&egrave;re en usoit beaucoup mieux.\
Vous &ecirc;tes d&eacute;pensi&egrave;re&nbsp;; et cet &eacute;tat me
blesse,\
Que vous alliez v&ecirc;tue ainsi qu\'une princesse.\
Quiconque &agrave; son mari veut plaire seulement,\
Ma bru, n\'a pas besoin de tant d\'ajustement.

#### CL&Eacute;ANTE. {#cléante. .cleante}

Mais, Madame, apr&egrave;s tout\...

#### MADAME PERNELLE. {#madame-pernelle.-7 .pernelle}

Pour vous, Monsieur son fr&egrave;re,\
Je vous estime fort, vous aime, et vous r&eacute;v&egrave;re&nbsp;;\
Mais enfin, si j\'&eacute;tois de mon fils, son &eacute;poux,\
Je vous prierois bien fort de n\'entrer point chez nous.\
Sans cesse vous pr&ecirc;chez des maximes de vivre\
Qui par d\'honn&ecirc;tes gens ne se doivent point suivre.\
Je vous parle un peu franc ; mais c\'est l&agrave; mon humeur,\
Et je ne m&acirc;che point ce que j\'ai sur le coeur.

#### DAMIS. {#damis.-1 .damis}

Votre Monsieur Tartuffe est bien heureux sans doute\...

#### MADAME PERNELLE. {#madame-pernelle.-8 .pernelle}

C\'est un homme de bien, qu\'il faut que l\'on &eacute;coute&nbsp;;\
Et je ne puis souffrir sans me mettre en courroux\
De le voir querell&eacute; par un fou comme vous.

#### DAMIS. {#damis.-2 .damis}

Quoi&nbsp;? je souffrirai, moi, qu\'un cagot[^2] de critique\
Vienne usurper c&eacute;ans un pouvoir tyrannique,\
Et que nous ne puissions &agrave; rien nous divertir,\
Si ce beau Monsieur-l&agrave; n\'y daigne consentir&nbsp;?

#### DORINE. {#dorine.-1 .dorine}

S\'il le faut &eacute;couter et croire &agrave; ses maximes,\
On ne peut faire rien qu\'on ne fasse des crimes&nbsp;;\
Car il contr&ocirc;le tout, ce critique z&eacute;l&eacute;.

#### MADAME PERNELLE. {#madame-pernelle.-9 .pernelle}

Et tout ce qu\'il contr&ocirc;le est fort bien contr&ocirc;l&eacute;.\
C\'est au chemin du Ciel qu\'il pr&eacute;tend vous conduire,\
Et mon fils &agrave; l\'aimer vous devroit tous induire.

#### DAMIS. {#damis.-3 .damis}

Non, voyez-vous, ma m&egrave;re, il n\'est p&egrave;re ni rien\
Qui me puisse obliger &agrave; lui vouloir du bien&nbsp;:\
Je trahirois mon coeur de parler d\'autre sorte&nbsp;;\
Sur ses fa&ccedil;ons de faire &agrave; tous coups je m\'emporte&nbsp;;\
J\'en pr&eacute;vois une suite, et qu\'avec ce pied plat\
Il faudra que j\'en vienne &agrave; quelque grand &eacute;clat.

#### DORINE. {#dorine.-2 .dorine}

Certes c\'est une chose aussi qui scandalise,\
De voir qu\'un inconnu c&eacute;ans s\'impatronise,\
Qu\'un gueux qui, quand il vint, n\'avoit pas de souliers\
Et dont l\'habit entier valoit bien six&nbsp;deniers,\
En vienne jusque-l&agrave; que de se m&eacute;conna&icirc;tre,\
De contrarier tout, et de faire le ma&icirc;tre.

#### MADAME PERNELLE. {#madame-pernelle.-10 .pernelle}

H&eacute;&nbsp;! merci de ma vie[^3]&nbsp;! il en iroit bien mieux,\
Si tout se gouvernoit par ses ordres pieux.

#### DORINE. {#dorine.-3 .dorine}

Il passe pour un saint dans votre fantaisie&nbsp;:\
Tout son fait, croyez-moi, n\'est rien qu\'hypocrisie.

#### MADAME PERNELLE. {#madame-pernelle.-11 .pernelle}

Voyez la langue&nbsp;!

#### DORINE. {#dorine.-4 .dorine}

&Agrave; lui, non plus qu\'&agrave; son Laurent,\
Je ne me fierois, moi, que sur un bon garant.

#### MADAME PERNELLE. {#madame-pernelle.-12 .pernelle}

J\'ignore ce qu\'au fond le serviteur peut &ecirc;tre&nbsp;;\
Mais pour homme de bien, je garantis le ma&icirc;tre.\
Vous ne lui voulez mal et ne le rebutez[^4]\
Qu\'&agrave; cause qu\'il vous dit &agrave; tous vos
v&eacute;rit&eacute;s.\
C\'est contre le p&eacute;ch&eacute; que son coeur se courrouce,\
Et l\'int&eacute;r&ecirc;t du Ciel est tout ce qui le pousse.

#### DORINE. {#dorine.-5 .dorine}

Oui&nbsp;; mais pourquoi, surtout depuis un certain temps,\
Ne sauroit-il souffrir qu\'aucun hante c&eacute;ans&nbsp;?\
En quoi blesse le Ciel une visite honn&ecirc;te,\
Pour en faire un vacarme &agrave; nous rompre la t&ecirc;te&nbsp;?\
Veut-on que l&agrave;-dessus je m\'explique entre nous&nbsp;?\
Je crois que de Madame il est, ma foi, jaloux.

#### MADAME PERNELLE. {#madame-pernelle.-13 .pernelle}

Taisez-vous, et songez aux choses que vous dites.\
Ce n\'est pas lui tout seul qui bl&acirc;me ces visites.\
Tout ce tracas qui suit les gens que vous hantez,\
Ces carrosses sans cesse &agrave; la porte plant&eacute;s,\
Et de tant de laquais le bruyant assemblage\
Font un &eacute;clat f&acirc;cheux dans tout le voisinage.\
Je veux croire qu\'au fond il ne se passe rien&nbsp;;\
Mais enfin on en parle, et cela n\'est pas bien.

#### CL&Eacute;ANTE. {#cléante.-1 .cleante}

H&eacute;&nbsp;! voulez-vous, Madame, emp&ecirc;cher qu\'on ne
cause&nbsp;?\
Ce seroit dans la vie une f&acirc;cheuse chose,\
Si pour les sots discours o&ugrave; l\'on peut &ecirc;tre mis,\
Il falloit renoncer &agrave; ses meilleurs amis.\
Et quand m&ecirc;me on pourroit se r&eacute;soudre &agrave; le faire,\
Croiriez-vous obliger tout le monde &agrave; se taire&nbsp;?\
Contre la m&eacute;disance il n\'est point de rempart.\
&Agrave; tous les sots caquets n\'ayons donc nul &eacute;gard&nbsp;;\
Effor&ccedil;ons-nous de vivre avec toute innocence,\
Et laissons aux causeurs une pleine licence.

#### DORINE. {#dorine.-6 .dorine}

Daphn&eacute;, notre voisine, et son petit &eacute;poux\
Ne seroient-ils point ceux qui parlent mal de nous&nbsp;?\
Ceux de qui la conduite offre le plus &agrave; rire\
Sont toujours sur autrui les premiers &agrave; m&eacute;dire&nbsp;;\
Ils ne manquent jamais de saisir promptement\
L\'apparente lueur du moindre attachement,\
D\'en semer la nouvelle avec beaucoup de joie,\
Et d\'y donner le tour qu\'ils veulent qu\'on y croie&nbsp;:\
Des actions d\'autrui, teintes de leurs couleurs,\
Ils pensent dans le monde autoriser les leurs,\
Et sous le faux espoir de quelque ressemblance,\
Aux intrigues qu\'ils ont donner de l\'innocence,\
Ou faire ailleurs tomber quelques traits partag&eacute;s\
De ce bl&acirc;me public dont ils sont trop charg&eacute;s.

#### MADAME PERNELLE. {#madame-pernelle.-14 .pernelle}

Tous ces raisonnements ne font rien &agrave; l\'affaire.\
On sait qu\'Orante m&egrave;ne une vie exemplaire&nbsp;:\
Tout ses soins vont au Ciel&nbsp;; et j\'ai su par des gens\
Qu\'elle condamne fort le train qui vient c&eacute;ans.

#### DORINE. {#dorine.-7 .dorine}

L\'exemple est admirable, et cette dame est bonne&nbsp;!\
Il est vrai qu\'elle vit en aust&egrave;re personne&nbsp;;\
Mais l\'&acirc;ge dans son &acirc;me a mis ce z&egrave;le ardent,\
Et l\'on sait qu\'elle est prude &agrave; son corps d&eacute;fendant.\
Tant qu\'elle a pu des coeurs attirer les hommages,\
Elle a fort bien joui de tous ses avantages&nbsp;;\
Mais, voyant de ses yeux tous les brillants[^5] baisser,\
Au monde, qui la quitte, elle veut renoncer,\
Et du voile pompeux d\'une haute sagesse\
De ses attraits us&eacute;s d&eacute;guise la foiblesse.\
Ce sont l&agrave; les retours des coquettes du temps.\
Il leur est dur de voir d&eacute;serter les galants.\
Dans un tel abandon, leur sombre inqui&eacute;tude\
Ne voit d\'autre recours que le m&eacute;tier de prude&nbsp;;\
Et la s&eacute;v&eacute;rit&eacute; de ces femmes de bien\
Censure toute chose, et ne pardonne &agrave; rien&nbsp;;\
Hautement d\'un chacun elles bl&acirc;ment la vie,\
Non point par charit&eacute;, mais par un trait d\'envie,\
Qui ne sauroit souffrir qu\'une autre ait les plaisirs[^6]\
Dont le penchant de l\'&acirc;ge a sevr&eacute; leurs d&eacute;sirs.

#### MADAME PERNELLE. {#madame-pernelle.-15 .pernelle}

Voil&agrave; les contes bleus[^7] qu\'il vous faut pour vous plaire.\
Ma bru, l\'on est chez vous contrainte de se taire,\
Car Madame[^8] &agrave; jaser tient le d&eacute; tout le jour.\
Mais enfin je pr&eacute;tends discourir &agrave; mon tour&nbsp;:\
Je vous dis que mon fils n\'a rien fait de plus sage\
Qu\'en recueillant chez soi ce d&eacute;vot personnage&nbsp;;\
Que le Ciel au besoin[^9] l\'a c&eacute;ans envoy&eacute;\
Pour redresser &agrave; tous votre esprit fourvoy&eacute;&nbsp;;\
Que pour votre salut vous le devez entendre,\
Et qu\'il ne reprend rien qui ne soit &agrave; reprendre.\
Ces visites, ces bals, ces conversations\
Sont du malin esprit toutes inventions.\
L&agrave; jamais on n\'entend de pieuses paroles&nbsp;:\
Ce sont propos oisifs, chansons et fariboles&nbsp;;\
Bien souvent le prochain en a sa bonne part,\
Et l\'on y sait m&eacute;dire et du tiers et du quart.\
Enfin les gens sens&eacute;s ont leurs t&ecirc;tes troubl&eacute;es\
De la confusion de telles assembl&eacute;es&nbsp;:\
Mille caquets divers s\'y font en moins de rien&nbsp;;\
Et comme l\'autre jour un docteur dit fort bien,\
C\'est v&eacute;ritablement la tour de Babylone,\
Car chacun y babille, et tout du long de l\'aune[^10]&nbsp;;\
Et pour conter l\'histoire o&ugrave; ce point l\'engagea\...\
Voil&agrave;-t-il pas Monsieur qui ricane d&eacute;j&agrave;&nbsp;!\
Allez chercher vos fous qui vous donnent &agrave; rire,\
Et sans\... Adieu, ma bru&nbsp;: je ne veux plus rien dire.\
Sachez que pour c&eacute;ans j\'en rabats de moiti&eacute;[^11],\
Et qu\'il fera beau temps quand j\'y mettrai le pied.\
*(Donnant un soufflet &agrave; Flipote.)*\
Allons, vous, vous r&ecirc;vez, et bayez aux corneilles.\
Jour de Dieu[^12]&nbsp;! je saurai vous frotter les oreilles.\
Marchons, gaupe[^13], marchons.

-   -   -   -   -   -   -   -   -   -   -   -   -   

ACTE I.
-------

### SC&Egrave;NE II

\- Cl&eacute;ante, Dorine.

#### CL&Eacute;ANTE. {#cléante.-2 .cleante}

Je n\'y veux point aller,\
De peur qu\'elle ne v&icirc;nt encor me quereller,\
Que cette bonne femme\...[^14]

#### DORINE. {#dorine.-8 .dorine}

Ah&nbsp;! certes, c\'est dommage\
Qu\'elle ne vous ou&icirc;t tenir un tel langage&nbsp;:\
Elle vous diroit bien qu\'elle vous trouve bon,\
Et qu\'elle n\'est point d\'&acirc;ge &agrave; lui donner ce nom.\

#### CL&Eacute;ANTE. {#cléante.-3 .cleante}

Comme elle s\'est pour rien contre nous &eacute;chauff&eacute;e&nbsp;!
Et que de son Tartuffe elle paro&icirc;t coiff&eacute;e[^15]&nbsp;!

#### DORINE. {#dorine.-9 .dorine}

Oh&nbsp;! vraiment tout cela n\'est rien au prix du fils,\
Et si vous l\'aviez vu, vous diriez&nbsp;: &laquo;&nbsp;C\'est bien
pis&nbsp;!&nbsp;&raquo;\
Nos troubles l\'avoient mis sur le pied d\'homme sage[^16],\
Et pour servir son prince il montra du courage&nbsp;;\
Mais il est devenu comme un homme h&eacute;b&eacute;t&eacute;,\
Depuis que de Tartuffe on le voit ent&ecirc;t&eacute;&nbsp;;\
Il l\'appelle son fr&egrave;re, et l\'aime dans son &acirc;me\
Cent&nbsp;fois plus qu\'il ne fait m&egrave;re, fils, fille et femme.\
C\'est de tous ses secrets l\'unique confident,\
Et de ses actions le directeur[^17] prudent&nbsp;;\
Il le choie, il l\'embrasse, et pour une ma&icirc;tresse\
On ne sauroit, je pense, avoir plus de tendresse&nbsp;;\
&Agrave; table, au plus haut bout[^18] il veut qu\'il soit assis&nbsp;;\
Avec joie il l\'y voit manger autant que six&nbsp;;\
Les bons morceaux de tout, il faut qu\'on les lui
c&egrave;de[^19]&nbsp;;\
Et s\'il vient &agrave; roter, il lui dit&nbsp;: &laquo;&nbsp;Dieu vous
aide[^20]&nbsp;!&nbsp;&raquo;\
*(C\'est une servante qui parle.)*\
Enfin il en est fou&nbsp;; c\'est son tout, son h&eacute;ros&nbsp;;\
Il l\'admire &agrave; tous coups, le cite &agrave; tout propos&nbsp;;\
Ses moindres actions lui semblent des miracles,\
Et tous les mots qu\'il dit sont pour lui des oracles.\
Lui, qui conno&icirc;t sa dupe et qui veut en jouir,\
Par cent dehors fard&eacute;s a l\'art de l\'&eacute;blouir[^21]&nbsp;;\
Son cagotisme[^22] en tire &agrave; toute heure des sommes,\
Et prend droit de gloser[^23] sur tous tant que nous sommes.\
Il n\'est pas jusqu\'au fat qui lui sert de gar&ccedil;on[^24]\
Qui ne se m&ecirc;le aussi de nous faire le&ccedil;on&nbsp;;\
Il vient nous sermonner avec des yeux farouches,\
Et jeter nos rubans, notre rouge et nos mouches.\
Le tra&icirc;tre, l\'autre jour, nous rompit de ses mains\
Un mouchoir qu\'il trouva dans une *Fleur des Saints*[^25],\
Disant que nous m&ecirc;lions, par un crime effroyable,\
Avec la saintet&eacute; les parures du diable.

-   -   -   -   -   -   -   -   -   -   -   -   

ACTE I.
-------

### SC&Egrave;NE III

\- Elmire, Mariane, Damis, Cl&eacute;ante, Dorine.

#### ELMIRE. {#elmire.-3 .elmire}

Vous &ecirc;tes bien heureux de n\'&ecirc;tre point venu\
Au discours qu\'&agrave; la porte elle nous a tenu.\
Mais j\'ai vu mon mari&nbsp;: comme il ne m\'a point vue,\
Je veux aller l&agrave;-haut attendre sa venue.

#### CL&Eacute;ANTE. {#cléante.-4 .cleante}

Moi, je l\'attends ici pour moins d\'amusement[^26],\
Et je vais lui donner le bonjour seulement.

#### DAMIS. {#damis.-4 .damis}

De l\'hymen[^27] de ma soeur touchez-lui quelque chose.\
J\'ai soup&ccedil;on que Tartuffe &agrave; son effet[^28] s\'oppose,\
Qu\'il oblige mon p&egrave;re &agrave; des d&eacute;tours si
grands&nbsp;;\
Et vous n\'ignorez pas quel int&eacute;r&ecirc;t j\'y prends.\
Si m&ecirc;me ardeur enflamme et ma soeur et Val&egrave;re,\
La soeur de cet ami, vous le savez, m\'est ch&egrave;re&nbsp;;\
Et s\'il falloit\...

#### DORINE. {#dorine.-10 .dorine}

Il entre.

-   -   -   

ACTE I.
-------

### SC&Egrave;NE IV

\- Orgon, Cl&eacute;ante, Dorine.

#### ORGON. {#orgon. .orgon}

Ah&nbsp;! mon fr&egrave;re, bonjour.

#### CL&Eacute;ANTE. {#cléante.-5 .cleante}

Je sortois, et j\'ai joie &agrave; vous voir de retour.\
La campagne &agrave; pr&eacute;sent n\'est pas beaucoup fleurie.

#### ORGON. {#orgon.-1 .orgon}

Dorine\...Mon beau-fr&egrave;re, attendez, je vous prie&nbsp;:\
Vous voulez bien souffrir, pour m\'&ocirc;ter de souci,\
Que je m\'informe un peu des nouvelles d\'ici.\
Tout s\'est-il, ces deux&nbsp;jours, pass&eacute; de bonne sorte&nbsp;?\
Qu\'est-ce qu\'on fait c&eacute;ans&nbsp;? comme est-ce qu\'on s\'y
porte&nbsp;?

#### DORINE. {#dorine.-11 .dorine}

Madame eut avant-hier la fi&egrave;vre jusqu\'au soir,\
Avec un mal de t&ecirc;te &eacute;trange &agrave; concevoir.

#### ORGON. {#orgon.-2 .orgon}

Et Tartuffe&nbsp;?

#### DORINE. {#dorine.-12 .dorine}

Tartuffe&nbsp;? Il se porte &agrave; merveille,\
Gros et gras, le teint frais, et la bouche vermeille.

#### ORGON. {#orgon.-3 .orgon}

Le pauvre homme&nbsp;!

#### DORINE. {#dorine.-13 .dorine}

Le soir, elle eut un grand d&eacute;go&ucirc;t,\
Et ne put au souper toucher &agrave; rien du tout,\
Tant sa douleur de t&ecirc;te &eacute;toit encore cruelle&nbsp;!

#### ORGON. {#orgon.-4 .orgon}

Et Tartuffe&nbsp;?

#### DORINE. {#dorine.-14 .dorine}

Il soupa, lui tout seul, devant elle,\
Et fort d&eacute;votement il mangea deux&nbsp;perdrix,\
Avec une moiti&eacute; de gigot en hachis.

#### ORGON. {#orgon.-5 .orgon}

Le pauvre homme&nbsp;!

#### DORINE. {#dorine.-15 .dorine}

La nuit se passa toute enti&egrave;re\
Sans qu\'elle p&ucirc;t fermer un moment la paupi&egrave;re&nbsp;;\
Des chaleurs l\'emp&ecirc;choient de pouvoir sommeiller,\
Et jusqu\'au jour pr&egrave;s d\'elle il nous fallut veiller.

#### ORGON. {#orgon.-6 .orgon}

Et Tartuffe&nbsp;?

#### DORINE. {#dorine.-16 .dorine}

Press&eacute; d\'un sommeil agr&eacute;able,\
Il passa dans sa chambre au sortir de la table,\
Et dans son lit bien chaud il se mit tout soudain,\
O&ugrave; sans trouble il dormit jusques au lendemain.

#### ORGON. {#orgon.-7 .orgon}

Le pauvre homme&nbsp;!

#### DORINE. {#dorine.-17 .dorine}

&Agrave; la fin, par nos raisons gagn&eacute;e,\
Elle se r&eacute;solut &agrave; souffrir la saign&eacute;e,\
Et le soulagement suivit tout aussit&ocirc;t.

#### ORGON. {#orgon.-8 .orgon}

Et Tartuffe&nbsp;?

#### DORINE. {#dorine.-18 .dorine}

Il reprit courage comme il faut,\
Et contre tous les maux fortifiant son &acirc;me,\
Pour r&eacute;parer le sang qu\'avoit perdu Madame,\
But &agrave; son d&eacute;jeuner quatre grands coups de vin.

#### ORGON. {#orgon.-9 .orgon}

Le pauvre homme[^29]&nbsp;!

#### DORINE. {#dorine.-19 .dorine}

Tous deux se portent bien enfin&nbsp;;\
Et je vais &agrave; Madame annoncer par avance\
La part que vous prenez &agrave; sa convalescence.

-   

ACTE I.
-------

### SC&Egrave;NE V

\- Orgon, Cl&eacute;ante.

#### CL&Eacute;ANTE. {#cléante.-6 .cleante}

&Agrave; votre nez, mon fr&egrave;re, elle se rit de vous&nbsp;;\
Et sans avoir dessein de vous mettre en courroux,\
Je vous dirai tout franc que c\'est avec justice.\
A-t-on jamais parl&eacute; d\'un semblable caprice&nbsp;?\
Et se peut-il qu\'un homme ait un charme aujourd\'hui\
&Agrave; vous faire oublier toutes choses pour lui,\
Qu\'apr&egrave;s avoir chez vous r&eacute;par&eacute; sa mis&egrave;re,\
Vous en veniez au point\...&nbsp;?

#### ORGON. {#orgon.-10 .orgon}

Alte-l&agrave;, mon beau-fr&egrave;re&nbsp;: Vous ne connoissez pas
celui dont vous parlez.

#### CL&Eacute;ANTE. {#cléante.-7 .cleante}

Je ne le connois pas, puisque vous le voulez&nbsp;;\
Mais enfin, pour savoir quel homme ce peut &ecirc;tre\...

#### ORGON. {#orgon.-11 .orgon}

Mon fr&egrave;re, vous seriez charm&eacute; de le conno&icirc;tre,\
Et vos ravissements[^30] ne prendroient point de fin.\
C\'est un homme\... qui\... ha&nbsp;!\... un homme\... un homme enfin.\
Qui suit bien ses le&ccedil;ons go&ucirc;te une paix profonde,\
Et comme du fumier regarde tout le monde[^31].\
Oui, je deviens tout autre avec son entretien&nbsp;;\
Il m\'enseigne &agrave; n\'avoir affection pour rien,\
De toutes amiti&eacute;s il d&eacute;tache mon &acirc;me&nbsp;;\
Et je verrois mourir fr&egrave;re, enfants, m&egrave;re et femme,\
Que je m\'en soucierois autant que de cela.

#### CL&Eacute;ANTE. {#cléante.-8 .cleante}

Les sentiments humains, mon fr&egrave;re, que voil&agrave;&nbsp;!

#### ORGON. {#orgon.-12 .orgon}

Ha&nbsp;! si vous aviez vu comme j\'en fis rencontre,\
Vous auriez pris pour lui l\'amiti&eacute; que je montre.\
Chaque jour &agrave; l\'&eacute;glise il venoit, d\'un air doux,\
Tout vis-&agrave;-vis de moi se mettre &agrave; deux genoux.\
Il attiroit les yeux de l\'assembl&eacute;e enti&egrave;re\
Par l\'ardeur dont au Ciel il poussoit sa pri&egrave;re&nbsp;;\
Il faisoit des soupirs, de grands &eacute;lancements,\
Et baisoit humblement la terre &agrave; tous moments&nbsp;;\
Et lorsque je sortois, il me devan&ccedil;oit vite,\
Pour m\'aller &agrave; la porte offrir de l\'eau b&eacute;nite.\
Instruit par son gar&ccedil;on, qui dans tout l\'imitoit,\
Et de son indigence, et de ce qu\'il &eacute;toit,\
Je lui faisois des dons&nbsp;; mais avec modestie\
Il me vouloit toujours en rendre une partie.\
&laquo;&nbsp;C\'est trop, me disoit-il, c\'est trop de la
moiti&eacute;&nbsp;;\
Je ne m&eacute;rite pas de vous faire piti&eacute;&nbsp;;&nbsp;&raquo;\
Et quand je refusois de le vouloir reprendre,\
Aux pauvres, &agrave; mes yeux, il alloit le r&eacute;pandre.\
Enfin le Ciel chez moi me le fit retirer,\
Et depuis ce temps-l&agrave; tout semble y prosp&eacute;rer.\
Je vois qu\'il reprend tout, et qu\'&agrave; ma femme m&ecirc;me\
Il prend, pour mon honneur, un int&eacute;r&ecirc;t
extr&ecirc;me&nbsp;;\
Il m\'avertit des gens qui lui font les yeux doux,\
Et plus que moi six fois il s\'en montre jaloux.\
Mais vous ne croiriez point jusqu\'o&ugrave; monte son
z&egrave;le&nbsp;:\
Il s\'impute &agrave; p&eacute;ch&eacute; la moindre bagatelle&nbsp;;\
Un rien presque suffit pour le scandaliser&nbsp;;\
Jusque-l&agrave; qu\'il se vint l\'autre jour accuser\
D\'avoir pris une puce en faisant sa pri&egrave;re,\
Et de l\'avoir tu&eacute;e avec trop de col&egrave;re.

#### CL&Eacute;ANTE. {#cléante.-9 .cleante}

Parbleu&nbsp;! vous &ecirc;tes fou, mon fr&egrave;re, que je croi.\
Avec de tels discours vous moquez-vous de moi&nbsp;?\
Et que pr&eacute;tendez-vous que tout ce badinage[^32]\...&nbsp;?

#### ORGON. {#orgon.-13 .orgon}

Mon fr&egrave;re, ce discours sent le libertinage[^33]&nbsp;:\
Vous en &ecirc;tes un peu dans votre &acirc;me
entich&eacute;[^34]&nbsp;;\
Et comme je vous l\'ai plus de dix&nbsp;fois pr&ecirc;ch&eacute;,\
Vous vous attirerez quelque m&eacute;chante affaire.

#### CL&Eacute;ANTE. {#cléante.-10 .cleante}

Voil&agrave; de vos pareils le discours ordinaire&nbsp;:\
Ils veulent que chacun soit aveugle comme eux.\
C\'est &ecirc;tre libertin que d\'avoir de bons yeux,\
Et qui n\'adore pas de vaines simagr&eacute;es,\
N\'a ni respect ni foi pour les choses sacr&eacute;es.\
Allez, tous vos discours ne me font point de peur&nbsp;:\
Je sais comme je parle, et le Ciel voit mon coeur.\
De tous vos fa&ccedil;onniers on n\'est point les esclaves.\
Il est de faux d&eacute;vots ainsi que de faux braves&nbsp;;\
Et comme on ne voit pas qu\'o&ugrave; l\'honneur les conduit\
Les vrais braves soient ceux qui font beaucoup de bruit,\
Les bons et vrais d&eacute;vots, qu\'on doit suivre &agrave; la trace,\
Ne sont pas ceux aussi qui font tant de grimace.\
H&eacute; quoi&nbsp;? vous ne ferez nulle distinction\
Entre l\'hypocrisie et la d&eacute;votion&nbsp;?\
Vous les voulez traiter d\'un semblable langage,\
Et rendre m&ecirc;me honneur au masque qu\'au visage,\
&Eacute;galer l\'artifice &agrave; la sinc&eacute;rit&eacute;,\
Confondre l\'apparence avec la v&eacute;rit&eacute;,\
Estimer le fant&ocirc;me autant que la personne,\
Et la fausse monnaie &agrave; l\'&eacute;gal de la bonne&nbsp;?\
Les hommes la plupart sont &eacute;trangement faits&nbsp;!\
Dans la juste nature on ne les voit jamais&nbsp;;\
La raison a pour eux des bornes trop petites&nbsp;;\
En chaque caract&egrave;re ils passent ses limites&nbsp;;\
Et la plus noble chose, ils la g&acirc;tent souvent\
Pour la vouloir outrer et pousser trop avant.\
Que cela vous soit dit en passant, mon beau-fr&egrave;re.

#### ORGON. {#orgon.-14 .orgon}

Oui, vous &ecirc;tes, sans doute[^35] un docteur qu\'on
r&eacute;v&egrave;re&nbsp;;\
Tout le savoir du monde est chez vous retir&eacute;&nbsp;;\
Vous &ecirc;tes le seul sage et le seul &eacute;clair&eacute;,\
Un oracle, un Caton[^36] dans le si&egrave;cle o&ugrave; nous
sommes&nbsp;;\
Et pr&egrave;s de vous ce sont des sots que tous les hommes.

#### CL&Eacute;ANTE. {#cléante.-11 .cleante}

Je ne suis point, mon fr&egrave;re, un docteur
r&eacute;v&eacute;r&eacute;,\
Et le savoir chez moi n\'est pas tout retir&eacute;.\
Mais, en un mot, je sais, pour toute ma science,\
Du faux avec le vrai faire la diff&eacute;rence.\
Et comme je ne vois nul genre de h&eacute;ros\
Qui soient plus &agrave; priser que les parfaits d&eacute;vots,\
Aucune chose au monde et plus noble et plus belle\
Que la sainte ferveur d\'un v&eacute;ritable z&egrave;le,\
Aussi ne vois-je rien qui soit plus odieux\
Que le dehors pl&acirc;tr&eacute; d\'un z&egrave;le sp&eacute;cieux,\
Que ces francs charlatans, que ces d&eacute;vots de place[^37],\
De qui la sacril&egrave;ge et trompeuse grimace\
Abuse impun&eacute;ment et se joue &agrave; leur gr&eacute;\
De ce qu\'ont les mortels de plus saint et sacr&eacute;,\
Ces gens qui, par une &acirc;me &agrave; l\'int&eacute;r&ecirc;t
soumise,\
Font de d&eacute;votion m&eacute;tier et marchandise,\
Et veulent acheter cr&eacute;dit et dignit&eacute;s\
&Agrave; prix de faux clins d\'yeux et d\'&eacute;lans affect&eacute;s,\
Ces gens, dis-je, qu\'on voit d\'une ardeur non commune\
Par le chemin du Ciel courir &agrave; leur fortune,\
Qui, br&ucirc;lants et priants, demandent chaque jour[^38],\
Et pr&ecirc;chent la retraite au milieu de la cour,\
Qui savent ajuster leur z&egrave;le avec leurs vices,\
Sont prompts, vindicatifs, sans foi, pleins d\'artifices,\
Et pour perdre quelqu\'un couvrent insolemment\
De l\'int&eacute;r&ecirc;t du Ciel, leur fier[^39] ressentiment,\
D\'autant plus dangereux dans leur &acirc;pre col&egrave;re,\
Qu\'ils prennent contre nous des armes qu\'on r&eacute;v&egrave;re,\
Et que leur passion, dont on leur sait bon gr&eacute;,\
Veut nous assassiner avec un fer sacr&eacute;.\
De ce faux caract&egrave;re on en voit trop paro&icirc;tre&nbsp;;\
Mais les d&eacute;vots de coeur sont ais&eacute;s &agrave;
conno&icirc;tre.\
Notre si&egrave;cle, mon fr&egrave;re, en expose &agrave; nos yeux\
Qui peuvent nous servir d\'exemples glorieux :\
Regardez Ariston, regardez P&eacute;riandre,\
Oronte, Alcidamas, Polydore, Clitandre&nbsp;;\
Ce titre par aucun ne leur est d&eacute;battu&nbsp;;\
Ce ne sont point du tout fanfarons de vertu&nbsp;;\
On ne voit point en eux ce faste insupportable,\
Et leur d&eacute;votion est humaine, est traitable&nbsp;;\
Ils ne censurent point toutes nos actions&nbsp;:\
Ils trouvent trop d\'orgueil dans ces corrections&nbsp;;\
Et laissant la fiert&eacute; des paroles aux autres,\
C\'est par leurs actions qu\'ils reprennent les n&ocirc;tres.\
L\'apparence du mal a chez eux peu d\'appui[^40],\
Et leur &acirc;me est port&eacute;e &agrave; juger bien d\'autrui.\
Point de cabale en eux, point d\'intrigues &agrave; suivre&nbsp;;\
On les voit, pour tous soins, se m&ecirc;ler de bien vivre&nbsp;;\
Jamais contre un p&eacute;cheur ils n\'ont d\'acharnement&nbsp;;\
Ils attachent leur haine au p&eacute;ch&eacute; seulement,\
Et ne veulent point prendre, avec un z&egrave;le extr&ecirc;me,\
Les int&eacute;r&ecirc;ts du Ciel plus qu\'il ne veut lui-m&ecirc;me.\
Voil&agrave; mes gens, voil&agrave; comme il en faut user,\
Voil&agrave; l\'exemple enfin qu\'il se faut proposer.\
Votre homme, &agrave; dire vrai, n\'est pas de ce mod&egrave;le&nbsp;:\
C\'est de fort bonne foi que vous vantez son z&egrave;le&nbsp;;\
Mais par un faux &eacute;clat je vous crois &eacute;bloui[^41].

#### ORGON. {#orgon.-15 .orgon}

Monsieur mon cher beau-fr&egrave;re, avez-vous tout dit&nbsp;?

#### CL&Eacute;ANTE. {#cléante.-12 .cleante}

Oui.

#### ORGON. {#orgon.-16 .orgon}

Je suis votre valet. *(Il veut s\'en aller.)*

#### CL&Eacute;ANTE. {#cléante.-13 .cleante}

De gr&acirc;ce, un mot, mon fr&egrave;re.\
Laissons l&agrave; ce discours. Vous savez que Val&egrave;re\
Pour &ecirc;tre votre gendre a parole de vous&nbsp;?

#### ORGON. {#orgon.-17 .orgon}

Oui.

#### CL&Eacute;ANTE. {#cléante.-14 .cleante}

Vous aviez pris jour pour un lien si doux.

#### ORGON. {#orgon.-18 .orgon}

Il est vrai.

#### CL&Eacute;ANTE. {#cléante.-15 .cleante}

Pourquoi donc en diff&eacute;rer la f&ecirc;te&nbsp;?

#### ORGON. {#orgon.-19 .orgon}

Je ne sais.

#### CL&Eacute;ANTE. {#cléante.-16 .cleante}

Auriez-vous autre pens&eacute;e en t&ecirc;te&nbsp;?

#### ORGON. {#orgon.-20 .orgon}

Peut-&ecirc;tre.

#### CL&Eacute;ANTE. {#cléante.-17 .cleante}

Vous voulez manquer &agrave; votre foi&nbsp;?

#### ORGON. {#orgon.-21 .orgon}

Je ne dis pas cela.

#### CL&Eacute;ANTE. {#cléante.-18 .cleante}

Nul obstacle, je croi,\
Ne peut vous emp&ecirc;cher d\'accomplir vos promesses.

#### ORGON. {#orgon.-22 .orgon}

Selon.

#### CL&Eacute;ANTE. {#cléante.-19 .cleante}

Pour dire un mot faut-il tant de finesses&nbsp;?\
Val&egrave;re sur ce point me fait vous visiter.

#### ORGON. {#orgon.-23 .orgon}

Le Ciel en soit lou&eacute;&nbsp;!

#### CL&Eacute;ANTE. {#cléante.-20 .cleante}

Mais que lui reporter&nbsp;?

#### ORGON. {#orgon.-24 .orgon}

Tout ce qu\'il vous plaira.

#### CL&Eacute;ANTE. {#cléante.-21 .cleante}

Mais il est n&eacute;cessaire\
De savoir vos desseins. Quels sont-ils donc&nbsp;?

#### ORGON. {#orgon.-25 .orgon}

De faire\
Ce que le Ciel voudra.

#### CL&Eacute;ANTE. {#cléante.-22 .cleante}

Mais parlons tout de bon.\
Val&egrave;re a votre foi&nbsp;: la tiendrez-vous, ou non&nbsp;?

#### ORGON. {#orgon.-26 .orgon}

Adieu.

#### CL&Eacute;ANTE. {#cléante.-23 .cleante}

Pour son amour je crains une disgr&acirc;ce,\
Et je dois l\'avertir de tout ce qui se passe.

-   -   -   -   -   -   -   -   -   -   -   -   

ACTE II.
--------

### SC&Egrave;NE I

\- Orgon, Mariane.

#### ORGON. {#orgon. .orgon}

Mariane.

#### MARIANE. {#mariane. .mariane}

Mon p&egrave;re.

#### ORGON. {#orgon.-1 .orgon}

Approchez, j\'ai de quoi\
Vous parler en secret.

#### MARIANE. {#mariane.-1 .mariane}

Que cherchez-vous&nbsp;?

#### ORGON. {#orgon.-2 .orgon}

*(Il regarde dans un petit cabinet.)*\
Je vois\
Si quelqu\'un n\'est point l&agrave; qui pourroit nous entendre&nbsp;;\
Car ce petit endroit est propre pour surprendre.\
Or sus, nous voil&agrave; bien. J\'ai, Mariane, en vous\
Reconnu de tout temps un esprit assez doux,\
Et de tout temps aussi vous m\'avez &eacute;t&eacute; ch&egrave;re.

#### MARIANE. {#mariane.-2 .mariane}

Je suis fort redevable &agrave; cet amour de p&egrave;re.

#### ORGON. {#orgon.-3 .orgon}

C\'est fort bien dit, ma fille&nbsp;; et pour le m&eacute;riter\
Vous devez n\'avoir soin que de me contenter.

#### MARIANE. {#mariane.-3 .mariane}

C\'est o&ugrave; je mets aussi ma gloire la plus haute.

#### ORGON. {#orgon.-4 .orgon}

Fort bien. Que dites-vous de Tartuffe notre h&ocirc;te&nbsp;?

#### MARIANE. {#mariane.-4 .mariane}

Qui, moi&nbsp;?

#### ORGON. {#orgon.-5 .orgon}

Vous. Voyez bien comme vous r&eacute;pondrez.

#### MARIANE. {#mariane.-5 .mariane}

H&eacute;las[^42]&nbsp;! j\'en dirai, moi, tout ce que vous voudrez.

#### ORGON. {#orgon.-6 .orgon}

C\'est parler sagement. Dites-moi donc, ma fille,\
Qu\'en toute sa personne un haut m&eacute;rite brille,\
Qu\'il touche votre coeur, et qu\'il vous seroit doux\
De le voir par mon choix devenir votre &eacute;poux.\
Eh&nbsp;?\
*(Mariane se recule avec surprise.)*

#### MARIANE. {#mariane.-6 .mariane}

Eh&nbsp;?

#### ORGON. {#orgon.-7 .orgon}

Qu\'est-ce&nbsp;?

#### MARIANE. {#mariane.-7 .mariane}

Pla&icirc;t-il&nbsp;?

#### ORGON. {#orgon.-8 .orgon}

Quoi&nbsp;?

#### MARIANE. {#mariane.-8 .mariane}

Me suis-je m&eacute;prise&nbsp;?

#### ORGON. {#orgon.-9 .orgon}

Comment&nbsp;?

#### MARIANE. {#mariane.-9 .mariane}

Qui voulez-vous, mon p&egrave;re, que je dise\
Qui me touche le coeur, et qu\'il me seroit doux\
De voir par votre choix devenir mon &eacute;poux&nbsp;?

#### ORGON. {#orgon.-10 .orgon}

Tartuffe.

#### MARIANE. {#mariane.-10 .mariane}

Il n\'en est rien, mon p&egrave;re, je vous jure.\
Pourquoi me faire dire une telle imposture&nbsp;?

#### ORGON. {#orgon.-11 .orgon}

Mais je veux que cela soit une v&eacute;rit&eacute;,\
Et c\'est assez pour vous que je l\'aie arr&ecirc;t&eacute;.

#### MARIANE. {#mariane.-11 .mariane}

Quoi&nbsp;? vous voulez, mon p&egrave;re\...&nbsp;?

#### ORGON. {#orgon.-12 .orgon}

Oui, je pr&eacute;tends, ma fille,\
Unir par votre hymen[^43] Tartuffe &agrave; ma famille.\
Il sera votre &eacute;poux, j\'ai r&eacute;solu cela&nbsp;;\
Et comme sur vos voeux je\...

-   -   

ACTE II.
--------

### SC&Egrave;NE II

\- Dorine, Orgon, Mariane.

#### ORGON. {#orgon.-13 .orgon}

Que faites-vous l&agrave;&nbsp;?\
La curiosit&eacute; qui vous presse est bien forte,\
Mamie, &agrave; nous venir &eacute;couter de la sorte[^44].

#### DORINE. {#dorine. .dorine}

Vraiment, je ne sais pas si c\'est un bruit qui part\
De quelque conjecture, ou d\'un coup de hasard&nbsp;;\
Mais de ce mariage on m\'a dit la nouvelle,\
Et j\'ai trait&eacute; cela de pure bagatelle.

#### ORGON. {#orgon.-14 .orgon}

Quoi donc&nbsp;? la chose est-elle incroyable&nbsp;?

#### DORINE. {#dorine.-1 .dorine}

&Agrave; tel point,\
Que vous-m&ecirc;me, Monsieur, je ne vous en crois point.

#### ORGON. {#orgon.-15 .orgon}

Je sais bien le moyen de vous le faire croire.

#### DORINE. {#dorine.-2 .dorine}

Oui, oui, vous nous contez une plaisante histoire.

#### ORGON. {#orgon.-16 .orgon}

Je conte justement ce qu\'on verra dans peu.

#### DORINE. {#dorine.-3 .dorine}

Chansons&nbsp;!

#### ORGON. {#orgon.-17 .orgon}

Ce que je dis, ma fille, n\'est point jeu.

#### DORINE. {#dorine.-4 .dorine}

Allez, ne croyez point &agrave; Monsieur votre p&egrave;re&nbsp;:\
Il raille.

#### ORGON. {#orgon.-18 .orgon}

Je vous dis\...

#### DORINE. {#dorine.-5 .dorine}

Non, vous avez beau faire,\
On ne vous croira point.

#### ORGON. {#orgon.-19 .orgon}

&Agrave; la fin mon courroux\...

#### DORINE. {#dorine.-6 .dorine}

H&eacute; bien&nbsp;! on vous croit donc, et c\'est tant pis pour vous.\
Quoi&nbsp;? se peut-il, Monsieur, qu\'avec l\'air d\'homme sage\
Et cette large barbe au milieu du visage,\
Vous soyez assez fou pour vouloir\...&nbsp;?

#### ORGON. {#orgon.-20 .orgon}

&Eacute;coutez&nbsp;:\
Vous avez pris c&eacute;ans certaines privaut&eacute;s\
Qui ne me plaisent point &nbsp;; je vous le dis, mamie.

#### DORINE. {#dorine.-7 .dorine}

Parlons sans nous f&acirc;cher, Monsieur, je vous supplie.\
Vous moquez-vous des gens d\'avoir fait ce complot ?\
Votre fille n\'est point l\'affaire d\'un bigot&nbsp;:\
Il a d\'autres emplois auxquels il faut qu\'il pense.\
Et puis, que vous apporte une telle alliance ?\
&Agrave; quel sujet aller, avec tout votre bien,\
Choisir une gendre gueux &nbsp;?\...

#### ORGON. {#orgon.-21 .orgon}

Taisez-vous. S\'il n\'a rien,\
Sachez que c\'est par l&agrave; qu\'il faut qu\'on le
r&eacute;v&egrave;re.\
Sa mis&egrave;re est sans doute[^45] une honn&ecirc;te
mis&egrave;re&nbsp;;\
Au-dessus des grandeurs elle doit l\'&eacute;lever,\
Puisqu\'enfin de son bien il s\'est laiss&eacute; priver\
Par son trop peu de soin des choses temporelles,\
Et sa puissante attache aux choses &eacute;ternelles.\
Mais mon secours pourra lui donner les moyens\
De sortir d\'embarras et rentrer dans ses biens&nbsp;:\
Ce sont fiefs qu\'&agrave; bon titre au pays on renomme&nbsp;&nbsp;;\
Et tel que l\'on le voit, il est bien gentilhomme[^46].

#### DORINE. {#dorine.-8 .dorine}

Oui, c\'est lui qui le dit&nbsp;; et cette vanit&eacute;,\
Monsieur, ne sied pas bien avec la pi&eacute;t&eacute;.\
Qui d\'une sainte vie embrasse l\'innocence\
Ne doit point tant pr&ocirc;ner son nom et sa naissance,\
Et l\'humble proc&eacute;d&eacute; de la d&eacute;votion\
Souffre mal les &eacute;clats de cette ambition.\
A quoi bon cet orgueil&nbsp;?\... Mais ce discours vous blesse&nbsp;:\
Parlons de sa personne, et laissons sa noblesse.\
Ferez-vous possesseur, sans quelque peu d\'ennui,\
D\'une fille comme elle un homme comme lui&nbsp;?\
Et ne devez-vous pas songer aux biens&eacute;ances,\
Et de cette union pr&eacute;voir les cons&eacute;quences&nbsp;?\
Sachez que d\'une fille on risque la vertu,\
Lorsque dans son hymen son go&ucirc;t est combattu,\
Que le dessein d\'y vivre en honn&ecirc;te personne\
D&eacute;pend des qualit&eacute;s du mari qu\'on lui donne,\
Et que ceux dont partout on montre au doigt le front\
Font leurs femmes souvent ce qu\'on voit qu\'elles sont.\
Il est bien difficile enfin d\'&ecirc;tre fid&egrave;le\
A de certains maris faits d\'un certain mod&egrave;le&nbsp;;\
Et qui donne &agrave; sa fille un homme qu\'elle hait\
Est responsable au Ciel des fautes qu\'elle fait.\
Songez &agrave; quels p&eacute;rils votre dessein vous livre.

#### ORGON. {#orgon.-22 .orgon}

Je vous dis qu\'il me faut apprendre d\'elle &agrave; vivre.

#### DORINE. {#dorine.-9 .dorine}

Vous n\'en feriez que mieux de suivre mes le&ccedil;ons.

#### ORGON. {#orgon.-23 .orgon}

Ne nous amusons point, ma fille, &agrave; ces chansons :\
Je sais ce qu\'il vous faut, et je suis votre p&egrave;re.\
J\'avois donn&eacute; pour vous ma parole &agrave; Val&egrave;re&nbsp;;\
Mais outre qu\'&agrave; jouer on dit qu\'il est enclin,\
Je le soup&ccedil;onne encor d\'&ecirc;tre un peu libertin[^47]&nbsp;:\
Je ne remarque point qu\'il hante les &eacute;glises.

#### DORINE. {#dorine.-10 .dorine}

Voulez-vous qu\'il y coure &agrave; vos heures pr&eacute;cises,\
Comme ceux qui n\'y vont que pour &ecirc;tre aper&ccedil;us&nbsp;?

#### ORGON. {#orgon.-24 .orgon}

Je ne demande pas votre avis l&agrave;-dessus.\
Enfin avec le Ciel l\'autre est le mieux du monde,\
Et c\'est une richesse &agrave; nulle autre seconde.\
Cet hymen de tous biens comblera vos d&eacute;sirs,\
Il sera tout confit en douceurs et plaisirs[^48].\
Ensemble vous vivrez, dans vos ardeurs fid&egrave;les,\
Comme deux vrais enfants, comme deux tourterelles&nbsp;;\
A nul f&acirc;cheux d&eacute;bat jamais vous n\'en viendrez,\
Et vous ferez de lui tout ce que vous voudrez.

#### DORINE. {#dorine.-11 .dorine}

Elle&nbsp;? elle n\'en fera qu\'un sot[^49], je vous assure.

#### ORGON. {#orgon.-25 .orgon}

Ouais&nbsp;! quels discours !

#### DORINE. {#dorine.-12 .dorine}

Je dis qu\'il en a l\'encolure,\
Et que son ascendant[^50], Monsieur, l\'emportera\
Sur toute la vertu que votre fille aura.

#### ORGON. {#orgon.-26 .orgon}

Cessez de m\'interrompre, et songez &agrave; vous taire,\
Sans mettre votre nez o&ugrave; vous n\'avez que faire.

#### DORINE. {#dorine.-13 .dorine}

Je n\'en parle, Monsieur, que pour votre int&eacute;r&ecirc;t.\
*(Elle l\'interrompt toujours au moment qu\'il se retourne\
pour parler &agrave; sa fille.)*

#### ORGON. {#orgon.-27 .orgon}

C\'est prendre trop de soin&nbsp;: taisez-vous, s\'il vous pla&icirc;t.

#### DORINE. {#dorine.-14 .dorine}

Si l\'on ne vous aimoit\...

#### ORGON. {#orgon.-28 .orgon}

Je ne veux pas qu\'on m\'aime.

#### DORINE. {#dorine.-15 .dorine}

Et je veux vous aimer, Monsieur, malgr&eacute; vous-m&ecirc;me.

#### ORGON. {#orgon.-29 .orgon}

Ah&nbsp;!

#### DORINE. {#dorine.-16 .dorine}

Votre honneur m\'est cher, et je ne puis souffrir\
Qu\'aux brocards d\'un chacun vous alliez vous offrir.

#### ORGON. {#orgon.-30 .orgon}

Vous ne vous tairez point&nbsp;?

#### DORINE. {#dorine.-17 .dorine}

C\'est une conscience[^51]\
Que de vous laisser faire une telle alliance.

#### ORGON. {#orgon.-31 .orgon}

Te tairas-tu, serpent, dont les traits effront&eacute;s\...&nbsp;?

#### DORINE. {#dorine.-18 .dorine}

Ah&nbsp;! vous &ecirc;tes d&eacute;vot, et vous vous emportez&nbsp;?

#### ORGON. {#orgon.-32 .orgon}

Oui, ma bile s\'&eacute;chauffe &agrave; toutes ces fadaises,\
Et tout r&eacute;solument je veux que tu te taises.

#### DORINE. {#dorine.-19 .dorine}

Soit. Mais, ne disant mot, je n\'en pense pas moins.

#### ORGON. {#orgon.-33 .orgon}

Pense, si tu le veux, mais applique tes soins\
A ne m\'en point parler, ou\...&nbsp;: suffit.\
*(Se retournant vers sa fille.)*\
Comme sage,\
J\'ai pes&eacute; m&ucirc;rement toutes choses.

#### DORINE. {#dorine.-20 .dorine}

J\'enrage\
De ne pouvoir parler.\
*(Elle se tait lorsqu\'il tourne la t&ecirc;te.)*

#### ORGON. {#orgon.-34 .orgon}

Sans &ecirc;tre damoiseau,\
Tartuffe est fait de sorte\...

#### DORINE. {#dorine.-21 .dorine}

Oui, c\'est un beau museau.

#### ORGON. {#orgon.-35 .orgon}

Que quand tu n\'aurois m&ecirc;me aucune sympathie\
Pour tous les autres dons\...\
*(Il se tourne devant elle, et la regarde les bras crois&eacute;s.)*

#### DORINE. {#dorine.-22 .dorine}

La voil&agrave; bien lotie&nbsp;!\
Si j\'&eacute;tois en sa place, un homme assur&eacute;ment\
Ne m\'&eacute;pouseroit pas de force impun&eacute;ment&nbsp;;\
Et je lui ferois voir bient&ocirc;t apr&egrave;s la f&ecirc;te\
Qu\'une femme a toujours une vengeance pr&ecirc;te.

#### ORGON. {#orgon.-36 .orgon}

Donc de ce que je dis on ne fera nul cas ?

#### DORINE. {#dorine.-23 .dorine}

De quoi vous plaignez-vous ? Je ne vous parle pas.

#### ORGON. {#orgon.-37 .orgon}

Qu\'est-ce que tu fais donc ?

#### DORINE. {#dorine.-24 .dorine}

Je me parle &agrave; moi-m&ecirc;me.

#### ORGON. {#orgon.-38 .orgon}

Fort bien. Pour ch&acirc;tier son insolence extr&ecirc;me,\
Il faut que je lui donne un revers de ma main.\
*(Il se met en posture de lui donner un soufflet ;\
et Dorine, &agrave; chaque coup d\'oeil qu\'il jette,\
se tient droite sans parler.)*\
Ma fille, vous devez approuver mon dessein\...\
Croire que le mari\...que j\'ai su vous &eacute;lire[^52]\...\
Que ne te parles-tu&nbsp;?

#### DORINE. {#dorine.-25 .dorine}

Je n\'ai rien &agrave; me dire.

#### ORGON. {#orgon.-39 .orgon}

Encore un petit mot.

#### DORINE. {#dorine.-26 .dorine}

Il ne me pla&icirc;t pas, moi.

#### ORGON. {#orgon.-40 .orgon}

Certes, je t\'y guettois.

#### DORINE. {#dorine.-27 .dorine}

Quelque sotte, ma foi[^53]&nbsp;!

#### ORGON. {#orgon.-41 .orgon}

Enfin, ma fille, il faut payer d\'ob&eacute;issance[^54],\
Et montrer pour mon choix enti&egrave;re d&eacute;f&eacute;rence.

#### DORINE, *en s\'enfuyant*. {#dorine-en-senfuyant. .dorine}

Je me moquerois fort[^55] de prendre un tel &eacute;poux.\
*(Il lui veut donner un soufflet et la manque.)*

#### ORGON. {#orgon.-42 .orgon}

Vous avez l&agrave;, ma fille, une peste avec vous,\
Avec qui sans p&eacute;ch&eacute; je ne saurois plus vivre.\
Je me sens hors d\'&eacute;tat maintenant de poursuivre&nbsp;:\
Ses discours insolents m\'ont mis l\'esprit en feu,\
Et je vais prendre l\'air pour me rasseoir[^56] un peu.

-   -   -   -   -   -   -   -   -   -   -   -   -   

ACTE II.
--------

### SC&Egrave;NE III

\- Dorine, Mariane.

#### DORINE. {#dorine.-28 .dorine}

Avez-vous donc perdu, dites-moi, la parole,\
Et faut-il qu\'en ceci je fasse votre r&ocirc;le&nbsp;?\
Souffrir qu\'on vous propose un projet insens&eacute;,\
Sans que du moindre mot vous l\'ayez repouss&eacute;&nbsp;!

#### MARIANE. {#mariane.-12 .mariane}

Contre un p&egrave;re absolu que veux-tu que je fasse&nbsp;?

#### DORINE. {#dorine.-29 .dorine}

Ce qu\'il faut pour parer une telle menace.

#### MARIANE. {#mariane.-13 .mariane}

Quoi&nbsp;?

#### DORINE. {#dorine.-30 .dorine}

Lui dire qu\'un coeur n\'aime point par autrui,\
Que vous vous mariez pour vous, non pas pour lui,\
Qu\'&eacute;tant celle pour qui se fait toute l\'affaire,\
C\'est &agrave; vous, non &agrave; lui, que le mari doit plaire,\
Et que si son Tartuffe est pour lui si charmant,\
Il le peut &eacute;pouser sans nul emp&ecirc;chement.

#### MARIANE. {#mariane.-14 .mariane}

Un p&egrave;re, je l\'avoue, a sur nous tant d\'empire,\
Que je n\'ai jamais eu la force de rien dire.

#### DORINE. {#dorine.-31 .dorine}

Mais raisonnons. Val&egrave;re a fait pour vous des pas[^57]&nbsp;:\
L\'aimez-vous, je vous prie, ou ne l\'aimez-vous pas&nbsp;?

#### MARIANE. {#mariane.-15 .mariane}

Ah&nbsp;! qu\'envers mon amour ton injustice est grande,\
Dorine&nbsp;! me dois-tu faire cette demande&nbsp;?\
T\'ai-je pas l&agrave;-dessus ouvert cent fois mon coeur,\
Et sais-tu pas pour lui jusqu\'o&ugrave; va mon ardeur&nbsp;?

#### DORINE. {#dorine.-32 .dorine}

Que sais-je si le coeur a parl&eacute; par la bouche,\
Et si c\'est tout de bon que cet amant vous touche&nbsp;?

#### MARIANE. {#mariane.-16 .mariane}

Tu me fais un grand tort, Dorine, d\'en douter,\
Et mes vrais sentiments ont su trop &eacute;clater.

#### DORINE. {#dorine.-33 .dorine}

Enfin, vous l\'aimez donc&nbsp;?

#### MARIANE. {#mariane.-17 .mariane}

Oui, d\'une ardeur extr&ecirc;me.

#### DORINE. {#dorine.-34 .dorine}

Et selon l\'apparence il vous aime de m&ecirc;me&nbsp;?

#### MARIANE. {#mariane.-18 .mariane}

Je le crois.

#### DORINE. {#dorine.-35 .dorine}

Et tous deux br&ucirc;lez &eacute;galement\
De vous voir mari&eacute;s ensemble&nbsp;?

#### MARIANE. {#mariane.-19 .mariane}

Assur&eacute;ment.

#### DORINE. {#dorine.-36 .dorine}

Sur cette autre union quelle est donc votre attente &nbsp;?

#### MARIANE. {#mariane.-20 .mariane}

De me donner la mort si l\'on me violente.

#### DORINE. {#dorine.-37 .dorine}

Fort bien&nbsp;: c\'est un recours o&ugrave; je ne songeois pas&nbsp;;\
Vous n\'avez qu\'&agrave; mourir pour sortir d\'embarras&nbsp;;\
Le rem&egrave;de sans doute est merveilleux. J\'enrage\
Lorsque j\'entends tenir ces sortes de langage.

#### MARIANE. {#mariane.-21 .mariane}

Mon Dieu&nbsp;! de quelle humeur, Dorine, tu te rends &nbsp;!\
Tu ne compatis point aux d&eacute;plaisirs des gens.

#### DORINE. {#dorine.-38 .dorine}

Je ne compatis point &agrave; qui dit des sornettes\
Et dans l\'occasion[^58] mollit comme vous faites.

#### MARIANE. {#mariane.-22 .mariane}

Mais que veux-tu&nbsp;? si j\'ai de la timidit&eacute;.

#### DORINE. {#dorine.-39 .dorine}

Mais l\'amour dans un coeur veut de la fermet&eacute;.

#### MARIANE. {#mariane.-23 .mariane}

Mais n\'en gard&eacute;-je pas pour les feux de Val&egrave;re&nbsp;?\
Et n\'est-ce pas &agrave; lui de m\'obtenir d\'un p&egrave;re&nbsp;?

#### DORINE. {#dorine.-40 .dorine}

Mais quoi&nbsp;? si votre p&egrave;re est un bourru[^59] fieff&eacute;,\
Qui s\'est de son Tartuffe enti&egrave;rement coiff&eacute;[^60],\
Et manque &agrave; l\'union qu\'il avoit arr&ecirc;t&eacute;e,\
La faute &agrave; votre amant doit-elle &ecirc;tre imput&eacute;e&nbsp;?

#### MARIANE. {#mariane.-24 .mariane}

Mais par un haut refus et d\'&eacute;clatants m&eacute;pris\
Ferai-je dans mon choix voir un coeur trop &eacute;pris &nbsp;?\
Sortirai-je pour lui, quelque &eacute;clat dont il brille,\
De la pudeur du sexe et du devoir de fille &nbsp;?\
Et veux-tu que mes feux par le monde &eacute;tal&eacute;s\...&nbsp;?

#### DORINE. {#dorine.-41 .dorine}

Non, non, je ne veux rien. Je vois que vous voulez\
&Ecirc;tre &agrave; Monsieur Tartuffe, et j\'aurois, quand j\'y pense,\
Tort de vous d&eacute;tourner d\'une telle alliance.\
Quelle raison aurois-je &agrave; combattre vos voeux ?\
Le parti de soi-m&ecirc;me est fort avantageux.\
Monsieur Tartuffe&nbsp;! oh&nbsp;! oh&nbsp;! n\'est-ce rien qu\'on
propose&nbsp;?\
Certes Monsieur Tartuffe, &agrave; bien prendre la chose,\
N\'est pas un homme, non, qui se mouche du pi&eacute;[^61],\
Et ce n\'est pas peu d\'heur[^62] que d\'&ecirc;tre sa moiti&eacute;.\
Tout le monde d&eacute;j&agrave; de gloire le couronne&nbsp;;\
Il est noble chez lui[^63], bien fait de sa personne&nbsp;;\
Il a l\'oreille rouge et le teint bien fleuri&nbsp;&nbsp;:\
Vous vivrez trop contente avec un tel mari[^64].

#### MARIANE. {#mariane.-25 .mariane}

Mon Dieu&nbsp;!\...

#### DORINE. {#dorine.-42 .dorine}

Quelle all&eacute;gresse aurez-vous dans votre &acirc;me,\
Quand d\'un &eacute;poux si beau vous vous verrez la femme&nbsp;!

#### MARIANE. {#mariane.-26 .mariane}

Ha&nbsp;! cesse, je te prie, un semblable discours,\
Et contre cet hymen ouvre-moi du secours.\
C\'en est fait, je me rends, et suis pr&ecirc;te &agrave; tout faire.

#### DORINE. {#dorine.-43 .dorine}

Non, il faut qu\'une fille ob&eacute;isse &agrave; son p&egrave;re,\
Voul&ucirc;t-il lui donner un singe pour &eacute;poux.\
Votre sort est fort beau&nbsp;: de quoi vous plaignez-vous&nbsp;?\
Vous irez par le coche en sa petite ville,\
Qu\'en oncles et cousins vous trouverez fertile,\
Et vous vous plairez fort &agrave; les entretenir.\
D\'abord chez le beau monde on vous fera venir&nbsp;&nbsp;;\
Vous irez visiter, pour votre bienvenue,\
Madame la baillive, et Madame l\'&eacute;lue[^65],\
Qui d\'un si&egrave;ge pliant[^66] vous feront honorer.\
L&agrave;, dans le carnaval, vous pourrez esp&eacute;rer\
Le bal et la grand\'bande[^67], &agrave; savoir, deux musettes,\
Et parfois, Fagotin[^68], et les marionnettes,\
Si pourtant votre &eacute;poux\...

#### MARIANE. {#mariane.-27 .mariane}

Ah&nbsp;! tu me fais mourir. De tes conseils plut&ocirc;t songe &agrave;
me secourir.

#### DORINE. {#dorine.-44 .dorine}

Je suis votre servante.

#### MARIANE. {#mariane.-28 .mariane}

Eh&nbsp;! Dorine, de gr&acirc;ce\...

#### DORINE. {#dorine.-45 .dorine}

Il faut, pour vous punir, que cette affaire passe.

#### MARIANE. {#mariane.-29 .mariane}

Ma pauvre fille&nbsp;!

#### DORINE. {#dorine.-46 .dorine}

Non.

#### MARIANE. {#mariane.-30 .mariane}

Si mes voeux d&eacute;clar&eacute;s[^69]\...

#### DORINE. {#dorine.-47 .dorine}

Point&nbsp;: Tartuffe est votre homme, et vous en t&acirc;terez.

#### MARIANE. {#mariane.-31 .mariane}

Tu sais qu\'&agrave; toi toujours je me suis confi&eacute;e&nbsp;:
Fais-moi\...

#### DORINE. {#dorine.-48 .dorine}

Non, vous serez, ma foi&nbsp;! tartuffi&eacute;e.

#### MARIANE. {#mariane.-32 .mariane}

H&eacute; bien&nbsp;! puisque mon sort ne sauroit t\'&eacute;mouvoir,\
Laisse-moi d&eacute;sormais toute &agrave; mon d&eacute;sespoir&nbsp;:\
C\'est de lui que mon coeur empruntera de l\'aide,\
Et je sais de mes maux l\'infaillible rem&egrave;de.\
*(Elle veut s\'en aller.)*

#### DORINE. {#dorine.-49 .dorine}

H&eacute;&nbsp;! l&agrave;, l&agrave;, revenez. Je quitte mon courroux.\
Il faut, nonobstant tout, avoir piti&eacute; de vous.

#### MARIANE. {#mariane.-33 .mariane}

Vois-tu, si l\'on m\'expose &agrave; ce cruel martyre,\
Je te le dis, Dorine, il faudra que j\'expire.

#### DORINE. {#dorine.-50 .dorine}

Ne vous tourmentez point. On peut adroitement\
Emp&ecirc;cher\... Mais voici Val&egrave;re, votre amant.

-   -   -   -   -   -   -   -   -   -   -   -   -   

ACTE II.
--------

### SC&Egrave;NE IV

\- Val&egrave;re, Mariane, Dorine.

#### VAL&Egrave;RE. {#valère. .valere}

On vient de d&eacute;biter, Madame, une nouvelle\
Que je ne savois pas, et qui sans doute[^70] est belle.

#### MARIANE. {#mariane.-34 .mariane}

Quoi&nbsp;?

#### VAL&Egrave;RE. {#valère.-1 .valere}

Que vous &eacute;pousez Tartuffe.

#### MARIANE. {#mariane.-35 .mariane}

Il est certain\
Que mon p&egrave;re s\'est mis en t&ecirc;te ce dessein.

#### VAL&Egrave;RE. {#valère.-2 .valere}

Votre p&egrave;re, Madame\...

#### MARIANE. {#mariane.-36 .mariane}

A chang&eacute; de vis&eacute;e&nbsp;:\
La chose vient par lui de m\'&ecirc;tre propos&eacute;e.

#### VAL&Egrave;RE. {#valère.-3 .valere}

Quoi&nbsp;? s&eacute;rieusement&nbsp;?

#### MARIANE. {#mariane.-37 .mariane}

Oui, s&eacute;rieusement.\
Il s\'est pour cet hymen d&eacute;clar&eacute; hautement.

#### VAL&Egrave;RE. {#valère.-4 .valere}

Et quel est le dessein o&ugrave; votre &acirc;me s\'arr&ecirc;te,\
Madame&nbsp;?

#### MARIANE. {#mariane.-38 .mariane}

Je ne sais.

#### VAL&Egrave;RE. {#valère.-5 .valere}

La r&eacute;ponse est honn&ecirc;te. Vous ne savez&nbsp;?

#### MARIANE. {#mariane.-39 .mariane}

Non.

#### VAL&Egrave;RE. {#valère.-6 .valere}

Non&nbsp;?

#### MARIANE. {#mariane.-40 .mariane}

Que me conseillez-vous&nbsp;?

#### VAL&Egrave;RE. {#valère.-7 .valere}

Je vous conseille, moi, de prendre cet &eacute;poux.

#### MARIANE. {#mariane.-41 .mariane}

Vous me le conseillez&nbsp;?

#### VAL&Egrave;RE. {#valère.-8 .valere}

Oui.

#### MARIANE. {#mariane.-42 .mariane}

Tout de bon&nbsp;?

#### VAL&Egrave;RE. {#valère.-9 .valere}

Sans doute.\
&nbsp; Le choix est glorieux, et vaut bien qu\'on l\'&eacute;coute.

#### MARIANE. {#mariane.-43 .mariane}

H&eacute; bien&nbsp;! c\'est un conseil, Monsieur, que je re&ccedil;ois.

#### VAL&Egrave;RE. {#valère.-10 .valere}

Vous n\'aurez pas grand\'peine &agrave; le suivre, je crois.

#### MARIANE. {#mariane.-44 .mariane}

Pas plus qu\'&agrave; le donner en a souffert votre &acirc;me.

#### VAL&Egrave;RE. {#valère.-11 .valere}

Moi, je vous l\'ai donn&eacute; pour vous plaire, Madame.

#### MARIANE. {#mariane.-45 .mariane}

Et moi, je le suivrai pour vous faire plaisir.

#### DORINE. {#dorine.-51 .dorine}

Voyons ce qui pourra de ceci r&eacute;ussir[^71].

#### VAL&Egrave;RE. {#valère.-12 .valere}

C\'est donc ainsi qu\'on aime&nbsp;? Et c\'&eacute;toit tromperie\
Quand vous\...

#### MARIANE. {#mariane.-46 .mariane}

Ne parlons point de cela, je vous prie.\
Vous m\'avez dit tout franc que je dois accepter\
Celui que pour &eacute;poux on me veut pr&eacute;senter&nbsp;:\
Et je d&eacute;clare, moi, que je pr&eacute;tends le faire,\
Puisque vous m\'en donnez le conseil salutaire.

#### VAL&Egrave;RE. {#valère.-13 .valere}

Ne vous excusez point sur mes intentions.\
Vous aviez pris d&eacute;j&agrave; vos r&eacute;solutions&nbsp;;\
Et vous vous saisissez d\'un pr&eacute;texte frivole\
Pour vous autoriser &agrave; manquer de parole.

#### MARIANE. {#mariane.-47 .mariane}

Il est vrai, c\'est bien dit.

#### VAL&Egrave;RE. {#valère.-14 .valere}

Sans doute, et votre coeur\
N\'a jamais eu pour moi de v&eacute;ritable ardeur.

#### MARIANE. {#mariane.-48 .mariane}

H&eacute;las&nbsp;! permis &agrave; vous d\'avoir cette pens&eacute;e.

#### VAL&Egrave;RE. {#valère.-15 .valere}

Oui, oui, permis &agrave; moi&nbsp;; mais mon &acirc;me offens&eacute;e\
Vous pr&eacute;viendra peut-&ecirc;tre en un pareil dessein&nbsp;;\
Et je sais o&ugrave; porter et mes voeux et ma main.

#### MARIANE. {#mariane.-49 .mariane}

Ah&nbsp;! je n\'en doute point&nbsp;; et les ardeurs qu\'excite\
Le m&eacute;rite\...

#### VAL&Egrave;RE. {#valère.-16 .valere}

Mon Dieu, laissons l&agrave; le m&eacute;rite&nbsp;:\
J\'en ai fort peu, sans doute[^72], et vous en faites foi.\
Mais j\'esp&egrave;re aux bont&eacute;s qu\'une autre aura pour moi,\
Et j\'en sais de qui l\'&acirc;me, &agrave; ma retraite ouverte,\
Consentira sans honte &agrave; r&eacute;parer ma perte.

#### MARIANE. {#mariane.-50 .mariane}

La perte n\'est pas grande &nbsp;; et de ce changement\
Vous vous consolerez assez facilement.

#### VAL&Egrave;RE. {#valère.-17 .valere}

J\'y ferai mon possible, et vous le pouvez croire.\
Un coeur qui nous oublie engage notre gloire[^73]&nbsp;;\
Il faut &agrave; l\'oublier mettre aussi tous nos soins&nbsp;:\
Si l\'on n\'en vient &agrave; bout, on le doit feindre au
moins&nbsp;&nbsp;;\
Et cette l&acirc;chet&eacute; jamais ne se pardonne,\
De montrer de l\'amour pour qui nous abandonne.

#### MARIANE. {#mariane.-51 .mariane}

Ce sentiment, sans doute[^74], est noble et relev&eacute;.

#### VAL&Egrave;RE. {#valère.-18 .valere}

Fort bien&nbsp;; et d\'un chacun il doit &ecirc;tre approuv&eacute;.\
H&eacute; quoi&nbsp;? vous voudriez qu\'&agrave; jamais dans mon
&acirc;me\
Je gardasse pour vous les ardeurs de ma flamme,\
Et vous visse, &agrave; mes yeux, passer en d\'autres bras,\
Sans mettre ailleurs un coeur dont vous ne voulez pas&nbsp;?

#### MARIANE. {#mariane.-52 .mariane}

Au contraire&nbsp;: pour moi, c\'est ce que je souhaite&nbsp;;\
Et je voudrois d&eacute;j&agrave; que la chose f&ucirc;t faite.

#### VAL&Egrave;RE. {#valère.-19 .valere}

Vous le voudriez&nbsp;?

#### MARIANE. {#mariane.-53 .mariane}

Oui.

#### VAL&Egrave;RE. {#valère.-20 .valere}

C\'est assez m\'insulter,\
Madame, et de ce pas je vais vous contenter.\
*(Il fait un pas pour s\'en aller et revient toujours.)*

#### MARIANE. {#mariane.-54 .mariane}

Fort bien.

#### VAL&Egrave;RE. {#valère.-21 .valere}

Souvenez-vous au moins que c\'est vous-m&ecirc;me\
Qui contraignez mon coeur &agrave; cet effort extr&ecirc;me.

#### MARIANE. {#mariane.-55 .mariane}

Oui.

#### VAL&Egrave;RE. {#valère.-22 .valere}

Et que le dessein que mon &acirc;me con&ccedil;oit\
N\'est rien qu\'&agrave; votre exemple.

#### MARIANE. {#mariane.-56 .mariane}

&Agrave; mon exemple, soit.

#### VAL&Egrave;RE. {#valère.-23 .valere}

Suffit&nbsp;: vous allez &ecirc;tre &agrave; point nomm&eacute; servie.

#### MARIANE. {#mariane.-57 .mariane}

Tant mieux.

#### VAL&Egrave;RE. {#valère.-24 .valere}

Vous me voyez, c\'est pour toute ma vie.

#### MARIANE. {#mariane.-58 .mariane}

&Agrave; la bonne heure.

#### VAL&Egrave;RE. {#valère.-25 .valere}

Euh&nbsp;?\
*(Il s\'en va&nbsp;; et lorsqu\'il est vers la porte, il se\
retourne.)*

#### MARIANE. {#mariane.-59 .mariane}

Quoi&nbsp;?

#### VAL&Egrave;RE. {#valère.-26 .valere}

Ne m\'appelez-vous pas&nbsp;?

#### MARIANE. {#mariane.-60 .mariane}

Moi&nbsp;? Vous r&ecirc;vez.

#### VAL&Egrave;RE. {#valère.-27 .valere}

H&eacute; bien&nbsp;! je poursuis donc mes pas.\
Adieu, Madame.

#### MARIANE. {#mariane.-61 .mariane}

Adieu, Monsieur.

#### DORINE. {#dorine.-52 .dorine}

Pour moi, je pense\
Que vous perdez l\'esprit par cette extravagance&nbsp;;\
Et je vous ai laiss&eacute; tout du long quereller,\
Pour voir o&ugrave; tout cela pourroit enfin aller.\
Hol&agrave;&nbsp;! seigneur Val&egrave;re.\
*(Elle va l\'arr&ecirc;ter par le bras, et lui, fait mine\
de grande r&eacute;sistance.)*

#### VAL&Egrave;RE. {#valère.-28 .valere}

H&eacute;&nbsp;! que veux-tu, Dorine&nbsp;?

#### DORINE. {#dorine.-53 .dorine}

Venez ici.

#### VAL&Egrave;RE. {#valère.-29 .valere}

Non, non, le d&eacute;pit me domine.\
Ne me d&eacute;tourne point de ce qu\'elle a voulu.

#### DORINE. {#dorine.-54 .dorine}

Arr&ecirc;tez.

#### VAL&Egrave;RE. {#valère.-30 .valere}

Non, vois-tu&nbsp;? c\'est un point r&eacute;solu.

#### DORINE. {#dorine.-55 .dorine}

Ah&nbsp;!

#### MARIANE. {#mariane.-62 .mariane}

Il souffre &agrave; me voir, ma pr&eacute;sence le chasse,\
Et je ferai bien mieux de lui quitter la place.

#### DORINE. *Elle quitte Val&egrave;re et court &agrave; Mariane.* {#dorine.-elle-quitte-valère-et-court-à-mariane. .dorine}

&Agrave; l\'autre. O&ugrave; courez-vous&nbsp;?

#### MARIANE. {#mariane.-63 .mariane}

Laisse.

#### DORINE. {#dorine.-56 .dorine}

Il faut revenir.

#### MARIANE. {#mariane.-64 .mariane}

Non, non, Dorine&nbsp;; en vain tu veux me retenir.

#### VAL&Egrave;RE. {#valère.-31 .valere}

Je vois bien que ma vue est pour elle un supplice,\
Et sans doute il vaut mieux que je l\'en affranchisse.

#### DORINE. *Elle quitte Mariane et court &agrave; Val&egrave;re.* {#dorine.-elle-quitte-mariane-et-court-à-valère. .dorine}

Encor&nbsp;! Diantre soit fait de vous si je le veux[^75]&nbsp;!\
Cessez ce badinage, et venez &ccedil;&agrave; tous deux.\
*(Elle les tire l\'un et l\'autre.)*

#### VAL&Egrave;RE. {#valère.-32 .valere}

Mais quel est ton dessein&nbsp;?

#### MARIANE. {#mariane.-65 .mariane}

Qu\'est-ce que tu veux faire&nbsp;?

#### DORINE. {#dorine.-57 .dorine}

Vous bien remettre ensemble, et vous tirer d\'affaire.\
&Ecirc;tes-vous fou d\'avoir un pareil d&eacute;m&ecirc;l&eacute;&nbsp;?

#### VAL&Egrave;RE. {#valère.-33 .valere}

N\'as-tu pas entendu comme elle m\'a parl&eacute;&nbsp;?

#### DORINE. {#dorine.-58 .dorine}

&Ecirc;tes-vous folle, vous, de vous &ecirc;tre emport&eacute;e&nbsp;?

#### MARIANE. {#mariane.-66 .mariane}

N\'as-tu pas vu la chose, et comme il m\'a trait&eacute;e&nbsp;?

#### DORINE. {#dorine.-59 .dorine}

Sottise des deux parts. Elle n\'a d\'autre soin\
Que de se conserver &agrave; vous, j\'en suis t&eacute;moin.\
Il n\'aime que vous seule, et n\'a point d\'autre envie\
Que d\'&ecirc;tre votre &eacute;poux&nbsp;; j\'en r&eacute;ponds sur ma
vie.

#### MARIANE. {#mariane.-67 .mariane}

Pourquoi donc me donner un semblable conseil&nbsp;?

#### VAL&Egrave;RE. {#valère.-34 .valere}

Pourquoi m\'en demander sur un sujet pareil&nbsp;?

#### DORINE. {#dorine.-60 .dorine}

Vous &ecirc;tes fous tous deux. &Ccedil;&agrave;, la main l\'un et
l\'autre.\
Allons, vous.

#### VAL&Egrave;RE, *en donnant sa main &agrave; Dorine.* {#valère-en-donnant-sa-main-à-dorine. .valere}

&Agrave; quoi bon ma main&nbsp;?

#### DORINE. {#dorine.-61 .dorine}

Ah&nbsp;! &Ccedil;&agrave; la v&ocirc;tre.

#### MARIANE, *en donnant aussi sa main.* {#mariane-en-donnant-aussi-sa-main. .mariane}

De quoi sert tout cela&nbsp;?

#### DORINE. {#dorine.-62 .dorine}

Mon Dieu&nbsp;! vite, avancez.\
Vous vous aimez tous deux plus que vous ne pensez.

#### VAL&Egrave;RE. {#valère.-35 .valere}

Mais ne faites donc point les choses avec g&ecirc;ne,\
Et regardez un peu les gens sans nulle haine.\
*(Mariane tourne l\'oeil vers Val&egrave;re et fait un petit sourire.)*

#### DORINE. {#dorine.-63 .dorine}

&Agrave; vous dire le vrai, les amants sont bien fous&nbsp;!

#### VAL&Egrave;RE. {#valère.-36 .valere}

Ho &ccedil;&agrave; n\'ai-je pas lieu de me plaindre de vous&nbsp;?\
Et pour n\'en point mentir, n\'&ecirc;tes-vous pas m&eacute;chante\
De vous plaire &agrave; me dire une chose affligeante&nbsp;?

#### MARIANE. {#mariane.-68 .mariane}

Mais vous, n\'&ecirc;tes-vous pas l\'homme le plus ingrat\...&nbsp;?

#### DORINE. {#dorine.-64 .dorine}

Pour une autre saison laissons tout ce d&eacute;bat,\
Et songeons &agrave; parer ce f&acirc;cheux mariage.

#### MARIANE. {#mariane.-69 .mariane}

Dis-nous donc quels ressorts il faut mettre en usage.

#### DORINE. {#dorine.-65 .dorine}

Nous en ferons agir de toutes les fa&ccedil;ons.\
Votre p&egrave;re se moque, et ce sont des chansons&nbsp;;\
Mais pour vous, il vaut mieux qu\'&agrave; son extravagance\
D\'un doux consentement vous pr&ecirc;tiez l\'apparence,\
Afin qu\'en cas d\'alarme il vous soit plus ais&eacute;\
De tirer en longueur cet hymen propos&eacute;.\
En attrapant du temps, &agrave; tout on rem&eacute;die.\
Tant&ocirc;t vous payerez de quelque maladie[^76],\
Qui viendra tout &agrave; coup et voudra des d&eacute;lais&nbsp;;\
Tant&ocirc;t vous payerez de pr&eacute;sages mauvais&nbsp;:\
Vous aurez fait d\'un mort la rencontre f&acirc;cheuse,\
Cass&eacute; quelque miroir, ou song&eacute; d\'eau bourbeuse.\
Enfin le bon de tout, c\'est qu\'&agrave; d\'autres qu\'&agrave; lui\
On ne vous peut lier, que vous ne disiez
&laquo;&nbsp;oui&nbsp;&raquo;[^77].\
Mais pour mieux r&eacute;ussir, il est bon, ce me semble,\
Qu\'on ne vous trouve point tous deux parlant ensemble.\
*(&Agrave; Val&egrave;re.)*\
Sortez, et sans tarder employez vos amis,\
Pour vous faire tenir ce qu\'on vous a promis.\
Nous allons r&eacute;veiller les efforts de son fr&egrave;re,\
Et dans votre parti jeter la belle-m&egrave;re[^78].\
Adieu.

#### VAL&Egrave;RE, *&agrave; Mariane.* {#valère-à-mariane. .valere}

Quelques efforts que nous pr&eacute;parions tous,\
Ma plus grande esp&eacute;rance, &agrave; vrai dire, est en vous.

#### MARIANE, *&agrave; Val&egrave;re.* {#mariane-à-valère. .mariane}

Je ne vous r&eacute;pons pas des volont&eacute;s d\'un
p&egrave;re&nbsp;;\
Mais je ne serai point &agrave; d\'autre qu\'&agrave; Val&egrave;re.

#### VAL&Egrave;RE. {#valère.-37 .valere}

Que vous me comblez d\'aise&nbsp;! Et quoi que puisse oser\...

#### DORINE. {#dorine.-66 .dorine}

Ah&nbsp;! jamais les amants ne sont las de jaser.\
Sortez, vous dis-je.

#### VAL&Egrave;RE. *Il fait un pas et revient.* {#valère.-il-fait-un-pas-et-revient. .valere}

Enfin\...

#### DORINE. {#dorine.-67 .dorine}

Quel caquet est le v&ocirc;tre&nbsp;!\
Tirez[^79] de cette part&nbsp;; et vous, tirez de l\'autre.\
*(Les poussant chacun par l\'&eacute;paule.)*

-   -   -   -   -   -   -   -   -   -   

ACTE III.
---------

### SC&Egrave;NE I

\- Damis, Dorine.

#### DAMIS. {#damis. .damis}

Que la foudre sur l\'heure ach&egrave;ve mes destins,\
Qu\'on me traite partout du plus grand des faquins,\
S\'il est aucun respect ni pouvoir qui m\'arr&ecirc;te,\
Et si je ne fais pas quelque coup de ma t&ecirc;te&nbsp;!

#### DORINE. {#dorine. .dorine}

De gr&acirc;ce, mod&eacute;rez un tel emportement&nbsp;;\
Votre p&egrave;re n\'a fait qu\'en parler simplement.\
On n\'ex&eacute;cute pas tout ce qui se propose,\
Et le chemin est long du projet &agrave; la chose.

#### DAMIS. {#damis.-1 .damis}

Il faut que de ce fat j\'arr&ecirc;te les complots,\
Et qu\'&agrave; l\'oreille un peu je lui dise deux mots.

#### DORINE. {#dorine.-1 .dorine}

Ha&nbsp;! tout doux&nbsp;! Envers lui, comme envers votre p&egrave;re,\
Laissez agit les soins de votre belle-m&egrave;re.\
Sur l\'esprit de Tartuffe elle a quelque cr&eacute;dit ;\
Il se rend complaisant &agrave; tout ce qu\'elle dit,\
Et pourroit bien avoir douceur de coeur pour elle.\
Pl&ucirc;t &agrave; Dieu qu\'il f&ucirc;t vrai&nbsp;! la chose seroit
belle.\
Enfin votre int&eacute;r&ecirc;t l\'oblige &agrave; le mander&nbsp;:\
Sur l\'hymen qui vous touche elle veut le sonder,\
Savoir ses sentiments, et lui faire conna&icirc;tre\
Quels f&acirc;cheux d&eacute;m&ecirc;l&eacute;s il pourra faire
na&icirc;tre,\
S\'il faut qu\'&agrave; ce dessein il pr&ecirc;te quelque espoir[^80].\
Son valet dit qu\'il prie, et je n\'ai pu le voir&nbsp;;\
Mais ce valet m\'a dit qu\'il s\'en alloit descendre.\
Sortez donc, je vous prie, et me laissez l\'attendre.

#### DAMIS. {#damis.-2 .dorine}

Je puis &ecirc;tre pr&eacute;sent &agrave; tout cet entretien.

#### DORINE. {#dorine.-2 .dorine}

Point. Il faut qu\'ils soient seuls.

#### DAMIS. {#damis.-3 .damis}

Je ne lui dirai rien.

#### DORINE. {#dorine.-3 .dorine}

Vous vous moquez&nbsp;: on sait vos transports ordinaires,\
Et c\'est le vrai moyen de g&acirc;ter les affaires.\
Sortez.

#### DAMIS. {#damis.-4 .damis}

Non&nbsp;: je veux voir sans me mettre en courroux.

#### DORINE. {#dorine.-4 .dorine}

Que vous &ecirc;tes f&acirc;cheux&nbsp;! Il vient. Retirez-vous.

-   

ACTE III.
---------

### SC&Egrave;NE II

\- Tartuffe, Laurent, Dorine.

#### TARTUFFE, *apercevant Dorine.* {#tartuffe-apercevant-dorine. .tartuffe}

Laurent, serrez ma haire avec ma discipline,\
Et priez que toujours le Ciel vous illumine.\
Si l\'on vient pour me voir, je vais aux prisonniers\
Des aum&ocirc;nes que j\'ai partager les deniers.

#### DORINE. {#dorine.-5 .dorine}

Que d\'affectation et de forfanterie&nbsp;!

#### TARTUFFE. {#tartuffe. .tartuffe}

Que voulez-vous&nbsp;?

#### DORINE. {#dorine.-6 .dorine}

Vous dire\...

#### TARTUFFE. *Il tire un mouchoir de sa poche.* {#tartuffe.-il-tire-un-mouchoir-de-sa-poche. .tartuffe}

Ah&nbsp;! mon Dieu, je vous prie,\
Avant que de parler prenez-moi ce mouchoir.

#### DORINE. {#dorine.-7 .dorine}

Comment&nbsp;?

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Couvrez ce sein que je ne saurois voir&nbsp;:\
Par de pareils objets les &acirc;mes sont bless&eacute;es,\
Et cela fait venir de coupables pens&eacute;es.

#### DORINE. {#dorine.-8 .dorine}

Vous &ecirc;tes donc bien tendre &agrave; la tentation,\
Et la chair sur vos sens fait grande impression&nbsp;?\
Certes je ne sais pas quelle chaleur vous monte&nbsp;:\
Mais &agrave; convoiter, moi, je ne suis point si prompte[^81],\
Et je vous verrois nu du haut jusques en bas,\
Que toute votre peau ne me tenteroit pas.

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Mettez dans vos discours un peu de modestie,\
Ou je vais sur-le-champ vous quitter la partie.

#### DORINE. {#dorine.-9 .dorine}

Non, non, c\'est moi qui vais vous laisser en repos,\
Et je n\'ai seulement qu\'&agrave; vous dire deux mots.\
Madame va venir dans cette salle basse,\
Et d\'un mot d\'entretien vous demande la gr&acirc;ce.

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

H&eacute;las[^82]&nbsp;! tr&egrave;s volontiers.

#### DORINE, *en soi-m&ecirc;me.* {#dorine-en-soi-même. .dorine}

Comme il se radoucit&nbsp;!\
Ma foi, je suis toujours pour ce que j\'en ai dit.

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

Viendra-t-elle bient&ocirc;t&nbsp;?

#### DORINE. {#dorine.-10 .dorine}

Je l\'entends, ce me semble.\
Oui, c\'est elle en personne, et je vous laisse ensemble.

-   -   

ACTE III.
---------

### SC&Egrave;NE III

\- Elmire, Tartuffe.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Que le Ciel &agrave; jamais par sa toute bont&eacute;\
Et de l\'&acirc;me et du corps vous donne la sant&eacute;,\
Et b&eacute;nisse vos jours autant que le d&eacute;sire\
Le plus humble de ceux que son amour inspire.

#### ELMIRE. {#elmire. .elmire}

Je suis fort oblig&eacute;e &agrave; ce souhait pieux.\
Mais prenons une chaise, afin d\'&ecirc;tre un peu mieux.

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

Comment de votre mal vous sentez-vous remise&nbsp;?

#### ELMIRE. {#elmire.-1 .elmire}

Fort bien ; et cette fi&egrave;vre a bient&ocirc;t quitt&eacute; prise.

#### TARTUFFE. {#tartuffe.-7 .tartuffe}

Mes pri&egrave;res n\'ont pas le m&eacute;rite qu\'il faut\
Pour avoir attir&eacute; cette gr&acirc;ce d\'en haut&nbsp;;\
Mais je n\'ai fait au Ciel nulle d&eacute;vote instance\
Qui n\'ait eu pour objet votre convalescence.

#### ELMIRE. {#elmire.-2 .elmire}

Votre z&egrave;le pour moi s\'est trop inqui&eacute;t&eacute;.

#### TARTUFFE. {#tartuffe.-8 .tartuffe}

On ne peut trop ch&eacute;rir votre ch&egrave;re sant&eacute;,\
Et pour la r&eacute;tablir j\'aurois donn&eacute; la mienne.

#### ELMIRE. {#elmire.-3 .elmire}

C\'est pousser bien avant la charit&eacute; chr&eacute;tienne,\
Et je vous dois beaucoup pour toutes ces bont&eacute;s.

#### TARTUFFE. {#tartuffe.-9 .tartuffe}

Je fais bien moins pour vous que vous ne m&eacute;ritez.

#### ELMIRE. {#elmire.-4 .elmire}

J\'ai voulu vous parler en secret d\'une affaire,\
Et suis bien aise ici qu\'aucun ne nous &eacute;claire[^83].

#### TARTUFFE. {#tartuffe.-10 .tartuffe}

J\'en suis ravi de m&ecirc;me, et sans doute[^84] il m\'est doux,\
Madame, de me voir seul &agrave; seul avec vous&nbsp;:\
C\'est une occasion qu\'au Ciel j\'ai demand&eacute;e,\
Sans que jusqu\'&agrave; cette heure il me l\'ait accord&eacute;e.

#### ELMIRE. {#elmire.-5 .elmire}

Pour moi, ce que je veux, c\'est un mot d\'entretien,\
O&ugrave; tout votre coeur s\'ouvre, et ne me cache rien.

#### TARTUFFE. {#tartuffe.-11 .tartuffe}

Et je ne veux aussi pour gr&acirc;ce singuli&egrave;re\
Que montrer &agrave; vos yeux mon &acirc;me toute enti&egrave;re,\
Et vous faire serment que les bruits que j\'ai faits[^85]\
Des visites qu\'ici re&ccedil;oivent vos attraits\
Ne sont pas envers vous l\'effet d\'aucune haine,\
Mais plut&ocirc;t d\'un transport de z&egrave;le qui m\'entra&icirc;ne,\
Et d\'un pur mouvement\...

#### ELMIRE. {#elmire.-6 .elmire}

Je le prends bien aussi,\
Et crois que mon salut vous donne ce souci.

#### TARTUFFE. *Il lui serre le bout des doigts.* {#tartuffe.-il-lui-serre-le-bout-des-doigts. .tartuffe}

Oui, Madame, sans doute, et ma ferveur est telle\...

#### ELMIRE. {#elmire.-7 .elmire}

Ouf&nbsp;! vous me serrez trop.

#### TARTUFFE. {#tartuffe.-12 .tartuffe}

C\'est par exc&egrave;s de z&egrave;le.\
De vous faire autre mal je n\'eus jamais dessein[^86],\
Et j\'aurois bien plut&ocirc;t\...\
*(Il lui met la main sur le genou.)*

#### ELMIRE. {#elmire.-8 .elmire}

Que fait l&agrave; votre main&nbsp;?

#### TARTUFFE. {#tartuffe.-13 .tartuffe}

Je t&acirc;te votre habit&nbsp;: l\'&eacute;toffe en est moelleuse.

#### ELMIRE. {#elmire.-9 .elmire}

Ah&nbsp;! de gr&acirc;ce, laissez, je suis fort chatouilleuse.\
*(Elle recule sa chaise, et Tartuffe rapproche la sienne.)*

#### TARTUFFE. {#tartuffe.-14 .tartuffe}

Mon Dieu&nbsp;! que de ce point l\'ouvrage est merveilleux&nbsp;!\
On travaille aujourd\'hui d\'un air miraculeux&nbsp;;\
Jamais, en toute chose, on n\'a vu si bien faire.

#### ELMIRE. {#elmire.-10 .elmire}

Il est vrai. Mais parlons un peu de notre affaire.\
On tient que mon mari veut d&eacute;gager sa foi,\
Et vous donner sa fille. Est-il vrai, dites-moi&nbsp;?

#### TARTUFFE. {#tartuffe.-15 .tartuffe}

Il m\'en a dit deux mots&nbsp;; mais, Madame, &agrave; vrai dire,\
Ce n\'est pas le bonheur apr&egrave;s quoi je soupire&nbsp;;\
Et je vois autre part les merveilleux attraits\
De la f&eacute;licit&eacute; qui fait tous mes souhaits.

#### ELMIRE. {#elmire.-11 .elmire}

C\'est que vous n\'aimez rien des choses de la terre.

#### TARTUFFE. {#tartuffe.-16 .tartuffe}

Mon sein n\'enferme pas un coeur qui soit de pierre.

#### ELMIRE. {#elmire.-12 .elmire}

Pour moi, je crois qu\'au Ciel tendent tous vos soupirs,\
Et que rien ici-bas n\'arr&ecirc;te vos d&eacute;sirs.

#### TARTUFFE. {#tartuffe.-17 .tartuffe}

L\'amour qui nous attache aux beaut&eacute;s &eacute;ternelles\
N\'&eacute;touffe pas en nous l\'amour des temporelles&nbsp;;\
Nos sens facilement peuvent &ecirc;tre charm&eacute;s\
Des ouvrages parfaits que le Ciel a form&eacute;s.\
Ses attraits r&eacute;fl&eacute;chis[^87] brillent dans vos
pareilles&nbsp;;\
Mais il &eacute;tale en vous ses plus rares merveilles&nbsp;;\
Il a sur votre face &eacute;panch&eacute; des beaut&eacute;s\
Dont les yeux sont surpris, et les coeurs transport&eacute;s,\
Et je n\'ai pu vous voir, parfaite cr&eacute;ature,\
Sans admirer en vous l\'auteur de la nature,\
Et d\'une ardente amour[^88] sentir mon coeur atteint,\
Au plus beau des portraits[^89] o&ugrave; lui-m&ecirc;me il s\'est
peint.\
D\'abord j\'appr&eacute;hendai que cette ardeur secr&egrave;te\
Ne f&ucirc;t du noir esprit[^90] une surprise adroite[^91]&nbsp;;\
Et m&ecirc;me &agrave; fuir vos yeux mon coeur se r&eacute;solut,\
Vous croyant un obstacle &agrave; faire mon salut.\
Mais enfin je connus, &ocirc; beaut&eacute; toute aimable,\
Que cette passion peut n\'&ecirc;tre point coupable,\
Que je puis l\'ajuster avecque la pudeur,\
Et c\'est ce qui m\'y fait abandonner mon coeur.\
Ce m\'est, je le confesse, une audace bien grande\
Que d\'oser de ce coeur vous adresser l\'offrande&nbsp;;\
Mais j\'attends en mes voeux tout de votre bont&eacute;,\
Et rien des vains efforts de mon infirmit&eacute;&nbsp;;\
En vous est mon espoir, mon bien, ma qui&eacute;tude,\
De vous d&eacute;pend ma peine ou ma b&eacute;atitude,\
Et je vais &ecirc;tre enfin, par votre seul arr&ecirc;t,\
Heureux si vous voulez, malheureux s\'il vous pla&icirc;t.

#### ELMIRE. {#elmire.-13 .elmire}

La d&eacute;claration est tout &agrave; fait galante,\
Mais elle est, &agrave; vrai dire, un peu bien surprenante.\
Vous deviez[^92], ce me semble, armer mieux votre sein,\
Et raisonner un peu sur un pareil dessein.\
Un d&eacute;vot comme vous, et que partout on nomme\...

#### TARTUFFE. {#tartuffe.-18 .tartuffe}

Ah&nbsp;! pour &ecirc;tre d&eacute;vot, je n\'en suis pas moins
homme[^93]&nbsp;;\
Et lorsqu\'on vient &agrave; voir vos c&eacute;lestes appas,\
Un coeur se laisse prendre, et ne raisonne pas.\
Je sais qu\'un tel discours de moi paro&icirc;t &eacute;trange&nbsp;;\
Mais, Madame, apr&egrave;s tout, je ne suis pas un ange&nbsp;;\
Et si vous condamnez l\'aveu que je vous fais,\
Vous devez vous en prendre &agrave; vos charmants attraits.\
D&egrave;s qu j\'en vis briller la splendeur plus qu\'humaine,\
De mon int&eacute;rieur[^94] vous f&ucirc;tes souveraine&nbsp;;\
De vos regards divins l\'ineffable douceur\
For&ccedil;a la r&eacute;sistance o&ugrave; s\'obstinoit mon
coeur&nbsp;&nbsp;;\
Elle surmonta tout, je&ucirc;nes, pri&egrave;res, larmes,\
Et tourna tous mes voeux du c&ocirc;t&eacute; de vos charmes.\
Mes yeux et mes soupirs vous l\'ont dit mille fois,\
Et pour mieux m\'expliquer j\'emploie ici la voix.\
Que si vous contemplez d\'une &acirc;me un peu b&eacute;nigne\
Les tribulations de votre esclave indigne,\
S\'il faut que vos bont&eacute;s veuillent me consoler\
Et jusqu\'&agrave; mon n&eacute;ant daignent se ravaler,\
J\'aurai toujours pour vous, &ocirc; suave merveille,\
Une d&eacute;votion &agrave; nulle autre pareille.\
Votre honneur avec moi ne court point de hasard,\
Et n\'a nulle disgr&acirc;ce &agrave; craindre de ma part.\
Tous ces galants de cour, dont les femmes sont folles,\
Sont bruyants dans leurs faits et vains dans leurs paroles,\
De leurs progr&egrave;s sans cesse on les voit se targuer&nbsp;;\
Ils n\'ont point de faveurs qu\'ils n\'aillent divulguer,\
Et leur langue indiscr&egrave;te, en qui l\'on se confie,\
D&eacute;shonore l\'autel o&ugrave; leur coeur sacrifie.\
Mais les gens comme nous br&ucirc;lent d\'un feu discret,\
Avec qui pour toujours on est s&ucirc;r du secret&nbsp;:\
Le soin que nous prenons de notre renomm&eacute;e\
R&eacute;pond de toute chose &agrave; la personne aim&eacute;e,\
Et c\'est en nous qu\'on trouve, acceptant notre coeur,\
De l\'amour sans scandale et du plaisir sans peur[^95].

#### ELMIRE. {#elmire.-14 .elmire}

Je vous &eacute;coute dire, et votre rh&eacute;torique\
En termes assez forts &agrave; mon &acirc;me s\'explique.\
N\'appr&eacute;hendez-vous point que je ne sois d\'humeur\
&Agrave; dire &agrave; mon mari cette galante ardeur,\
Et que le prompt avis d\'un amour de la sorte\
Ne p&ucirc;t bien alt&eacute;rer l\'amiti&eacute; qu\'il vous porte
&nbsp;?

#### TARTUFFE. {#tartuffe.-19 .tartuffe}

Je sais que vous avez trop de b&eacute;nignit&eacute;,\
Et que vous ferez gr&acirc;ce &agrave; ma t&eacute;m&eacute;rit&eacute;\
Que vous m\'excuserez sur l\'humaine foiblesse\
Des violents transports d\'un amour qui vous blesse,\
Et consid&eacute;rerez, en regardant votre air,\
Que l\'on est pas aveugle, et qu\'un homme est de chair.

#### ELMIRE. {#elmire.-15 .elmire}

D\'autres prendroient cela d\'autre fa&ccedil;on peut-&ecirc;tre&nbsp;;\
Mais ma discr&eacute;tion se veut faire paro&icirc;tre.\
Je ne redirai point l\'affaire &agrave; mon &eacute;poux &nbsp;;\
Mais je veux en revanche une chose de vous&nbsp;:\
C\'est de presser tout franc et sans nulle chicane\
L\'union de Val&egrave;re avecque Mariane,\
De renoncer vous-m&ecirc;me &agrave; l\'injuste pouvoir\
Qui veut du bien d\'un autre enrichir votre espoir,\
Et\...

-   -   -   -   -   -   -   -   -   -   -   -   -   

ACTE III.
---------

### SC&Egrave;NE IV

\- Damis, Elmire, Tartuffe.

#### DAMIS, *sortant du petit cabinet o&ugrave; il s\'&eacute;toit retir&eacute;.* {#damis-sortant-du-petit-cabinet-où-il-sétoit-retiré. .damis}

Non, Madame, non&nbsp;: ceci doit se r&eacute;pandre.\
J\'&eacute;tois en cet endroit, d\'o&ugrave; j\'ai pu tout
entendre&nbsp;;\
Et la bont&eacute; du Ciel m\'y semble avoir conduit\
Pour confondre l\'orgueil d\'un tra&icirc;tre qui me nuit,\
Pour m\'ouvrir une voie &agrave; prendre la vengeance\
De son hypocrisie et de son insolence,\
&Agrave; d&eacute;tromper mon p&egrave;re, et lui mettre en plein jour\
L\'&acirc;me d\'un sc&eacute;l&eacute;rat qui vous parle d\'amour.

#### ELMIRE. {#elmire.-16 .elmire}

Non, Damis&nbsp;: il suffit qu\'il se rende plus sage,\
Et t&acirc;che &agrave; m&eacute;riter la gr&acirc;ce o&ugrave; je
m\'engage.\
Puisque je l\'ai promis, ne m\'en d&eacute;dites pas.\
Ce n\'est point mon humeur de faire des &eacute;clats&nbsp;:\
Une femme se rit de sottises pareilles,\
Et jamais d\'un mari n\'en trouble les oreilles.

#### DAMIS. {#damis.-5 .damis}

Vous avez vos raisons pour en user ainsi,\
Et pour faire autrement j\'ai les miennes aussi.\
Le vouloir &eacute;pargner est une raillerie[^96]&nbsp;;\
Et l\'insolent orgueil de sa cagoterie\
N\'a triomph&eacute; que trop de mon juste courroux,\
Et que trop excit&eacute; de d&eacute;sordre chez nous.\
Le fourbe trop longtemps a gouvern&eacute; mon p&egrave;re,\
Et desservi mes feux avec ceux de Val&egrave;re.\
Il faut que du perfide il soit d&eacute;sabus&eacute;,\
Et le Ciel pour cela m\'offre un moyen ais&eacute;.\
De cette occasion je lui suis redevable,\
Et pour la n&eacute;gliger, elle est trop favorable&nbsp;:\
Ce seroit m&eacute;riter qu\'il me la v&icirc;nt ravir\
Que de l\'avoir en main et ne m\'en pas servir.

#### ELMIRE. {#elmire.-17 .elmire}

Damis\...

#### DAMIS. {#damis.-6 .damis}

Non, s\'il vous pla&icirc;t, il faut que je me croie[^97].\
Mon &acirc;me est maintenant au comble de sa joie&nbsp;;\
Et vos discours en vain pr&eacute;tendent m\'obliger\
&Agrave; quitter le plaisir de me pouvoir venger.\
Sans aller plus avant, je vais vuider d\'affaire[^98]&nbsp;;\
Et voici justement de quoi me satisfaire.

-   -   -   

ACTE III.
---------

### SC&Egrave;NE V

\- Orgon, Damis, Tartuffe, Elmire.

#### DAMIS. {#damis.-7 .damis}

Nous allons r&eacute;galer, mon p&egrave;re, votre abord\
D\'un incident tout frais qui vous surprendra fort.\
Vous &ecirc;tes bien pay&eacute; de toutes vos caresses,\
Et Monsieur d\'un beau prix reconno&icirc;t vos tendresses.\
Son grand z&egrave;le pour vous vient de se d&eacute;clarer&nbsp;:\
Il ne va pas &agrave; moins qu\'&agrave; vous d&eacute;shonorer&nbsp;;\
Et je l\'ai surpris l&agrave; qui faisoit &agrave; Madame\
L\'injurieux aveu d\'une coupable flamme.\
Elle est d\'une humeur douce, et son coeur trop discret\
Vouloit &agrave; toute force en garder le secret&nbsp;;\
Mais je ne puis flatter une telle impudence,\
Et crois que vous la taire est vous faire une offense.

#### ELMIRE. {#elmire.-18 .elmire}

Oui, je tiens que jamais de tous ces vains propos\
On ne doit d\'un mari traverser le repos,\
Que ce n\'est point de l&agrave; que l\'honneur peut d&eacute;pendre,\
Et qu\'il suffit pour nous de savoir nous d&eacute;fendre&nbsp;:\
Ce sont mes sentiments&nbsp;; et vous n\'auriez rien dit,\
Damis, si j\'avois eu sur vous quelque cr&eacute;dit.

ACTE III.
---------

### SC&Egrave;NE VI

\- Orgon, Damis, Tartuffe.

#### ORGON. {#orgon. .orgon}

Ce que je viens d\'entendre, &ocirc; Ciel&nbsp;! est-il croyable&nbsp;?

#### TARTUFFE. {#tartuffe.-20 .tartuffe}

Oui, mon fr&egrave;re, je suis un m&eacute;chant, un coupable,\
Un malheureux p&eacute;cheur, tout plein d\'iniquit&eacute;,\
Le plus grand sc&eacute;l&eacute;rat qui jamais ait
&eacute;t&eacute;&nbsp;;\
Chaque instant de ma vie est charg&eacute; de souillures&nbsp;;\
Elle n\'est qu\'un amas de crimes et d\'ordures&nbsp;;\
Et je vois que le Ciel, pour ma punition,\
Me veut mortifier en cette occasion.\
De quelque grand forfait qu\'on me puisse reprendre,\
Je n\'ai garde d\'avoir l\'orgueil de m\'en d&eacute;fendre.\
Croyez ce qu\'on vous dit, armez votre courroux,\
Et comme un criminel chassez-moi de chez vous&nbsp;:\
Je ne saurois avoir tant de honte en partage,\
Que je n\'en aie encor m&eacute;rit&eacute; davantage.

#### ORGON, *&agrave; son fils.* {#orgon-à-son-fils. .orgon}

Ah&nbsp;! tra&icirc;tre, oses-tu bien par cette fausset&eacute;\
Vouloir de sa vertu ternir la puret&eacute;&nbsp;?

#### DAMIS. {#damis.-8 .damis}

Quoi&nbsp;? la feinte douceur de cette &acirc;me hypocrite\
Vous fera d&eacute;mentir\...&nbsp;?

#### ORGON. {#orgon.-1 .orgon}

Tais-toi, peste maudite.

#### TARTUFFE. {#tartuffe.-21 .tartuffe}

Ah&nbsp;! laissez-le parler&nbsp;: vous l\'accusez &agrave; tort,\
Et vous feriez bien mieux de croire &agrave; son rapport.\
Pourquoi sur un tel fait m\'&ecirc;tre si favorable&nbsp;?\
Savez-vous, apr&egrave;s tout, de quoi je suis capable&nbsp;?\
Vous fiez-vous, mon fr&egrave;re, &agrave; mon ext&eacute;rieur&nbsp;?\
Et, pour tout ce qu\'on voit, me croyez-vous meilleur&nbsp;?\
Non, non&nbsp;: vous vous laissez tromper &agrave; l\'apparence,\
Et je ne suis rien moins, h&eacute;las&nbsp;! que ce qu\'on
pense&nbsp;;\
Tout le monde me prend pour un homme de bien&nbsp;;\
Mais la v&eacute;rit&eacute; pure est que je ne vaux rien.\
*(S\'adressant &agrave; Damis.)*\
Oui, mon cher fils, parlez&nbsp;: traitez-moi de perfide,\
D\'inf&acirc;me, de perdu, de voleur, d\'homicide&nbsp;;\
Accablez-moi de noms encor plus d&eacute;test&eacute;s&nbsp;:\
Je n\'y contredis point, je les ai m&eacute;rit&eacute;s&nbsp;;\
Et j\'en veux &agrave; genoux souffrir l\'ignominie,\
Comme une honte due aux crimes de ma vie.

#### ORGON. {#orgon.-2 .orgon}

*(&Agrave; Tartuffe.)*\
Mon fr&egrave;re, c\'en est trop.\
*(&Agrave; son fils.)*\
Ton coeur ne se rend point,\
Tra&icirc;tre&nbsp;?

#### DAMIS. {#damis.-9 .damis}

Quoi&nbsp;! ses discours vous s&eacute;duiront au point\...

#### ORGON. {#orgon.-3 .orgon}

Tais-toi, pendard.\
*(&Agrave; Tartuffe.)*\
Mon fr&egrave;re, eh&nbsp;! levez-vous, de gr&acirc;ce&nbsp;!\
*(&Agrave; son fils.)*\
Inf&acirc;me&nbsp;!

#### DAMIS. {#damis.-10 .damis}

Il peut\...

#### ORGON. {#orgon.-4 .orgon}

Tais-toi.

#### DAMIS. {#damis.-11 .damis}

J\'enrage&nbsp;! Quoi ? je passe\...

#### ORGON. {#orgon.-5 .orgon}

Si tu dis un seul mot, je te romprai les bras.

#### TARTUFFE. {#tartuffe.-22 .tartuffe}

Mon fr&egrave;re, au nom de Dieu, ne vous emportez pas.\
J\'aimerois mieux souffrir la peine la plus dure,\
Qu\'il e&ucirc;t re&ccedil;u pour moi la moindre &eacute;gratignure.

#### ORGON. {#orgon.-6 .orgon}

*(&Agrave; son fils.)*\
Ingrat&nbsp;!

#### TARTUFFE. {#tartuffe.-23 .tartuffe}

Laissez-le en paix[^99]. S\'il faut, &agrave; deux genoux,\
Vous demander sa gr&acirc;ce\...

#### ORGON, *&agrave; Tartuffe.* {#orgon-à-tartuffe. .orgon}

H&eacute;las&nbsp;! vous moquez-vous&nbsp;?\
*(&Agrave; son fils.)*\
Coquin&nbsp;! vois sa bont&eacute;.

#### DAMIS. {#damis.-12 .damis}

Donc\...

#### ORGON. {#orgon.-7 .orgon}

Paix.

#### DAMIS. {#damis.-13 .damis}

Quoi&nbsp;? je\...

#### ORGON. {#orgon.-8 .orgon}

Paix, dis-je\
Je sais bien quel motif &agrave; l\'attaquer t\'oblige&nbsp;:\
Vous le ha&iuml;ssez tous&nbsp;; et je vois aujourd\'hui\
Femme, enfants et valets d&eacute;cha&icirc;n&eacute;s contre
lui&nbsp;;\
On met impudemment toute chose en usage,\
Pour &ocirc;ter de chez moi ce d&eacute;vot personnage.\
Mais plus on fait d\'effort afin de le bannir,\
Plus j\'en veux employer &agrave; l\'y mieux retenir&nbsp;;\
Et je vais me h&acirc;ter de lui donner ma fille,\
Pour confondre l\'orgueil de toute ma famille.

#### DAMIS. {#damis.-14 .damis}

A recevoir sa main on pense l\'obliger&nbsp;?

#### ORGON. {#orgon.-9 .orgon}

Oui, tra&icirc;tre, et d&egrave;s ce soir, pour vous faire enrager.\
Ah&nbsp;! je vous brave tous, et vous ferai conna&icirc;tre\
Qu\'il faut qu\'on m\'ob&eacute;isse et que je suis le ma&icirc;tre.\
Allons, qu\'on se r&eacute;tracte, et qu\'&agrave; l\'instant, fripon,\
On se jette &agrave; ses pieds pour demander pardon.

#### DAMIS. {#damis.-15 .damis}

Qui, moi&nbsp;? de ce coquin, qui, par ses impostures\...

#### ORGON. {#orgon.-10 .orgon}

Ah&nbsp;! tu r&eacute;sistes, gueux, et lui dis des injures&nbsp;?\
*(&Agrave; Tartuffe.)*\
Un b&acirc;ton&nbsp;! un b&acirc;ton&nbsp;! Ne me retenez pas.\
*(&Agrave; son fils.)*\
Sus, que de ma maison on sorte de ce pas,\
Et que d\'y revenir on n\'ait jamais l\'audace.

#### DAMIS. {#damis.-16 .damis}

Oui, je sortirai&nbsp;; mais\...

#### ORGON. {#orgon.-11 .orgon}

Vite quittons la place.\
Je te prive, pendard, de ma succession,\
Et te donne de plus ma mal&eacute;diction.

-   

ACTE III.
---------

### SC&Egrave;NE VII

\- Orgon, Tartuffe.

#### ORGON. {#orgon.-12 .orgon}

Offenser de la sorte une sainte personne&nbsp;!

#### TARTUFFE. {#tartuffe.-24 .tartuffe}

&Ocirc; Ciel, pardonne-lui la douleur qu\'il me donne[^100]&nbsp;!\
*(&Agrave; Orgon.)*\
Si vous pouviez savoir avec quel d&eacute;plaisir\
Je vois qu\'envers mon fr&egrave;re on t&acirc;che &agrave; me
noircir\...

#### ORGON. {#orgon.-13 .orgon}

H&eacute;las&nbsp;!

#### TARTUFFE. {#tartuffe.-25 .tartuffe}

Le seul penser de cette ingratitude\
Fait souffrir &agrave; mon &acirc;me un supplice si rude\...\
L\'horreur que j\'en con&ccedil;ois\... J\'ai le coeur si serr&eacute;,\
Que je ne puis parler, et crois que j\'en mourrai.

#### ORGON. {#orgon.-14 .orgon}

*(Il court tout en larmes &agrave; la porte par o&ugrave; il a
chass&eacute; son fils.)*\
Coquin&nbsp;! je me repens que ma main t\'ai fait gr&acirc;ce,\
Et ne t\'ait pas d\'abord assomm&eacute; sur la place.\
Remettez-vous, mon fr&egrave;re, et ne vous f&acirc;chez pas.

#### TARTUFFE. {#tartuffe.-26 .tartuffe}

Rompons, rompons le cours de ces f&acirc;cheux d&eacute;bats.\
Je regarde c&eacute;ans quels grands troubles j\'apporte,\
Et crois qu\'il est besoin, mon fr&egrave;re, que j\'en sorte.

#### ORGON. {#orgon.-15 .orgon}

Comment&nbsp;? vous moquez-vous&nbsp;?

#### TARTUFFE. {#tartuffe.-27 .tartuffe}

On m\'y hait, et je voi\
Qu\'on cherche &agrave; vous donner des soup&ccedil;ons de ma foi[^101].

#### ORGON. {#orgon.-16 .orgon}

Qu\'importe&nbsp;? Voyez-vous que mon coeur les &eacute;coute&nbsp;?

#### TARTUFFE. {#tartuffe.-28 .tartuffe}

On ne manquera pas de poursuivre, sans doute[^102]&nbsp;;\
Et ces m&ecirc;mes rapports qu\'ici vous rejetez\
Peut-&ecirc;tre une autre fois seront-ils &eacute;cout&eacute;s.

#### ORGON. {#orgon.-17 .orgon}

Non, mon fr&egrave;re, jamais.

#### TARTUFFE. {#tartuffe.-29 .tartuffe}

Ah&nbsp;! mon fr&egrave;re, une femme\
Ais&eacute;ment d\'un mari peut bien surprendre l\'&acirc;me.

#### ORGON. {#orgon.-18 .orgon}

Non, non.

#### TARTUFFE. {#tartuffe.-30 .tartuffe}

Laissez-moi vite, en m\'&eacute;loignant d\'ici,\
Leur &ocirc;ter tout sujet de m\'attaquer ainsi.

#### ORGON. {#orgon.-19 .orgon}

Non, vous demeurerez&nbsp;: il y va de ma vie.

#### TARTUFFE. {#tartuffe.-31 .tartuffe}

H&eacute; bien&nbsp;! il faudra donc que je me mortifie.\
Pourtant, si vous vouliez\...

#### ORGON. {#orgon.-20 .orgon}

Ah&nbsp;!

#### TARTUFFE. {#tartuffe.-32 .tartuffe}

Soit&nbsp;: n\'en parlons plus.\
Mais je sais comme il faut en user l&agrave;-dessus.\
L\'honneur est d&eacute;licat, et l\'amiti&eacute; m\'engage\
&Agrave; pr&eacute;venir les bruits et les sujets d\'ombrage.\
Je fuirai votre &eacute;pouse, et vous ne me verrez\...

#### ORGON. {#orgon.-21 .orgon}

Non, en d&eacute;pit de tous vous la fr&eacute;quenterez.\
Faire enrager le monde est ma plus grande joie,\
Et je veux qu\'avec elle &agrave; toute heure on vous voie.\
Ce n\'est pas tout encor &nbsp;: pour les mieux braver tous,\
Je ne veux point avoir d\'autre h&eacute;ritier que vous,\
Et je vais de ce pas, en fort bonne mani&egrave;re,\
Vous faire de mon bien donation enti&egrave;re.\
Un bon et franc ami, que pour gendre je prends,\
M\'est bien plus cher que fils, que femme, et que parents.\
N\'accepterez-vous pas ce que je vous propose&nbsp;?

#### TARTUFFE. {#tartuffe.-33 .tartuffe}

La volont&eacute; du Ciel soit faite en toute chose.

#### ORGON. {#orgon.-22 .orgon}

Le pauvre homme&nbsp;! Allons vite en dresser un &eacute;crit,\
Et que puisse l\'envie en crever de d&eacute;pit &nbsp;!

-   -   -   

ACTE IV.
--------

### SC&Egrave;NE I

\- Cl&eacute;ante, Tartuffe.

#### CL&Eacute;ANTE. {#cléante. .cleante}

Oui, tout le monde en parle, et vous m\'en pouvez croire,\
L\'&eacute;clat que fait ce bruit n\'est point &agrave; votre
gloire&nbsp;;\
Et je vous ai trouv&eacute;, Monsieur, fort &agrave; propos,\
Pour vous en dire net ma pens&eacute;e en deux mots.\
Je n\'examine point &agrave; fond ce qu\'on expose&nbsp;;\
Je passe l&agrave;-dessus, et prends au pis la chose.\
Supposons que Damis n\'en ait pas bien us&eacute;,\
Et que ce soit &agrave; tort qu\'on vous ait accus&eacute;&nbsp;;\
N\'est-il pas d\'un chr&eacute;tien de pardonner l\'offense,\
Et d\'&eacute;teindre en son coeur tout d&eacute;sir de
vengeance&nbsp;?\
Et devez-vous souffrir, pour votre d&eacute;m&ecirc;l&eacute;,\
Que du logis d\'un p&egrave;re un fils soit exil&eacute;&nbsp;?\
Je vous le dis encore, et parle avec franchise,\
Il n\'est petit ni grand qui ne s\'en scandalise&nbsp;;\
Et si vous m\'en croyez, vous pacifierez tout,\
Et ne pousserez point les affaires &agrave; bout.\
Sacrifiez &agrave; Dieu toute votre col&egrave;re,\
Et remettez le fils en gr&acirc;ce avec le p&egrave;re.

#### TARTUFFE. {#tartuffe. .tartuffe}

H&eacute;las&nbsp;! je le voudrois, quant &agrave; moi, de bon
coeur&nbsp;:\
Je ne garde pour lui, Monsieur, aucune aigreur&nbsp;;\
Je lui pardonne tout, de rien je ne le bl&acirc;me,\
Et voudrois le servir du meilleur de mon &acirc;me&nbsp;;\
Mais l\'int&eacute;r&ecirc;t du Ciel n\'y sauroit consentir,\
Et s\'il rentre c&eacute;ans, c\'est &agrave; moi d\'en sortir.\
Apr&egrave;s son action, qui n\'eut jamais d\'&eacute;gale,\
Le commerce entre nous porteroit du scandale&nbsp;:\
Dieu sait ce que d\'abord tout le monde en croiroit&nbsp;!\
&Agrave; pure politique on me l\'imputeroit&nbsp;;\
Et l\'on diroit partout que, me sentant coupable,\
Je feins pour qui m\'accuse un z&egrave;le charitable,\
Que mon coeur l\'appr&eacute;hende et veut le m&eacute;nager,\
Pour le pouvoir sous main au silence engager.

#### CL&Eacute;ANTE. {#cléante.-1 .cleante}

Vous nous payez ici d\'excuses color&eacute;es[^103],\
Et toutes vos raisons, Monsieur, sont trop tir&eacute;es[^104].\
Des int&eacute;r&ecirc;ts du Ciel pourquoi vous chargez-vous&nbsp;?\
Pour punir le coupable a-t-il besoin de vous&nbsp;?\
Laissez-lui, laissez-lui le soin de ses vengeances&nbsp;;\
Ne songez qu\'au pardon qu\'il prescrit des offenses&nbsp;;\
Et ne regardez point aux jugements humains,\
Quand vous suivez du Ciel les ordres souverains.\
Quoi&nbsp;? le foible int&eacute;r&ecirc;t de ce qu\'on pourra croire\
D\'une bonne action emp&ecirc;chera la gloire&nbsp;?\
Non, non&nbsp;: faisons toujours ce que le Ciel prescrit,\
Et d\'aucun autre soin ne nous brouillons l\'esprit.

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Je vous ai d&eacute;j&agrave; dit que mon coeur lui pardonne,\
Et c\'est faire, Monsieur, ce que le Ciel ordonne&nbsp;;\
Mais apr&egrave;s le scandale et l\'affront d\'aujourd\'hui,\
Le Ciel n\'ordonne pas que je vive avec lui.

#### CL&Eacute;ANTE. {#cléante.-2 .cleante}

Et vous ordonne-t-il, Monsieur, d\'ouvrir l\'oreille\
&Agrave; ce qu\'un pur caprice &agrave; son p&egrave;re conseille,\
Et d\'accepter le don qui vous est fait d\'un bien\
O&ugrave; le droit vous oblige &agrave; ne pr&eacute;tendre rien&nbsp;?

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Ceux qui me conno&icirc;tront n\'auront pas la pens&eacute;e\
Que ce soit un effet d\'une &acirc;me int&eacute;ress&eacute;e.\
Tous les biens de ce monde ont pour moi peu d\'appas,\
De leur &eacute;clat trompeur je ne m\'&eacute;blouis pas&nbsp;;\
Et si je me r&eacute;sous &agrave; recevoir du p&egrave;re\
Cette donation qu\'il a voulu me faire,\
Ce n\'est, &agrave; dire vrai, que parce que je crains\
Que tout ce bien ne tombe en de m&eacute;chantes mains,\
Qu\'il ne trouve des gens qui, l\'ayant en partage,\
En fassent dans le monde un criminel usage,\
Et ne s\'en servent pas, ainsi que j\'ai dessein,\
Pour la gloire du Ciel et le bien du prochain.

#### CL&Eacute;ANTE. {#cléante.-3 .cleante}

H&eacute;, Monsieur, n\'ayez point ces d&eacute;licates craintes,\
Qui d\'un juste h&eacute;ritier peuvent causer les plaintes&nbsp;;\
Souffrez, sans vous vouloir embarrasser de rien,\
Qu\'il soit &agrave; ses p&eacute;rils possesseur de son bien&nbsp;;\
Et songez qu\'il vaut mieux encor qu\'il en m&eacute;suse,\
Que si de l\'en frustrer il faut qu\'on vous accuse.\
J\'admire seulement que sans confusion\
Vous en ayez souffert la proposition&nbsp;;\
Car enfin le vrai z&egrave;le a-t-il quelque maxime\
Qui montre &agrave;[^105] d&eacute;pouiller l\'h&eacute;ritier
l&eacute;gitime&nbsp;?\
Et s\'il faut que le Ciel dans votre coeur ait mis\
Un invincible obstacle &agrave; vivre avec Damis,\
Ne vaudroit-il pas mieux qu\'en personne discr&egrave;te\
Vous fissiez de c&eacute;ans une honn&ecirc;te retraite,\
Que de souffrir ainsi, contre toute raison,\
Qu\'on en chasse pour vous le fils de la maison&nbsp;?\
Croyez-moi, c\'est donner de votre prud\'homie,\
Monsieur\...

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

Il est, Monsieur, trois heures et demie &nbsp;:\
Certain devoir pieux me demande l&agrave;-haut,\
Et vous m\'excuserez de vous quitter sit&ocirc;t.

#### CL&Eacute;ANTE. {#cléante.-4 .cleante}

Ah&nbsp;!

-   -   -   

ACTE IV.
--------

### SC&Egrave;NE II

\- Elmire, Mariane, Dorine, Cl&eacute;ante.

#### DORINE. {#dorine. .dorine}

De gr&acirc;ce, avec nous employez-vous pour elle,\
Monsieur&nbsp;: son &acirc;me souffre une douleur mortelle &nbsp;;\
Et l\'accord que son p&egrave;re a conclu pour ce soir\[\^106\]\
La fait, &agrave; tout moment, entrer en d&eacute;sespoir.\
Il va venir. Joignons nos efforts, je vous prie,\
Et t&acirc;chons d\'&eacute;branler, de force ou d\'industrie,\
Ce malheureux dessein qui nous a tous troubl&eacute;s.

-   \[\^106\]*Et l\'accord que son p&egrave;re a conclu pour ce
    soir*&nbsp;: accord est ici synonyme, non de contrat de mariage, car
    le contrat est d&eacute;j&agrave; tout r&eacute;dig&eacute; et Orgon
    le rapporte de chez son notaire, mais de mariage m&ecirc;me.

ACTE IV.
--------

### SC&Egrave;NE III

\- Orgon, Elmire, Mariane, Cl&eacute;ante, Dorine.

#### ORGON. {#orgon. .orgon}

Ha&nbsp;! je me r&eacute;jouis de vous voir assembl&eacute;s&nbsp;:\
*(&Agrave; Mariane.)*\
Je porte en ce contrat de quoi vous faire rire,\
Et vous savez d&eacute;j&agrave; ce que cela veut dire.

#### MARIANE, *&agrave; genoux.* {#mariane-à-genoux. .mariane}

Mon p&egrave;re, au nom du Ciel, qui conno&icirc;t ma douleur,\
Et par tout ce qui peut &eacute;mouvoir votre coeur,\
Rel&acirc;chez-vous un peu des droits de la naissance[^106],\
Et dispensez mes voeux de cette ob&eacute;issance[^107]&nbsp;;\
Ne me r&eacute;duisez point par cette dure loi\
Jusqu\'&agrave; me plaindre au Ciel de ce que je vous doi,\
Et cette vie, h&eacute;las &nbsp;! que vous m\'avez donn&eacute;e,\
Ne me la rendez pas, mon p&egrave;re, infortun&eacute;e.\
Si, contre un doux espoir que j\'avois pu former,\
Vous me d&eacute;fendez d\'&ecirc;tre &agrave; ce que j\'ose aimer,\
Au moins, par vos bont&eacute;s, qu\'&agrave; vos genoux j\'implore,\
Sauvez-moi du tourment d\'&ecirc;tre &agrave; ce que j\'abhorre,\
Et ne me portez point a quelque d&eacute;sespoir,\
En vous servant sur moi de tout votre pouvoir.

#### ORGON, *se sentant attendrir.* {#orgon-se-sentant-attendrir. .orgon}

Allons, ferme, mon coeur, point de foiblesse humaine.

#### MARIANE. {#mariane. .mariane}

Vos tendresses pour lui ne me font point de peine&nbsp;;\
Faites-les &eacute;clater, donnez-lui votre bien,\
Et, si ce n\'est assez, joignez-y tout le mien[^108]&nbsp;:\
J\'y consens de bon coeur, et je vous l\'abandonne&nbsp;;\
Mais au moins n\'allez pas jusques &agrave; ma personne,\
Et souffrez qu\'un couvent dans les aust&eacute;rit&eacute;s\
Use les tristes jours que le Ciel m\'a compt&eacute;s.

#### ORGON. {#orgon.-1 .orgon}

Ah&nbsp;! voil&agrave; justement de mes religieuses,\
Lorsqu\'un p&egrave;re combat leurs flammes amoureuses &nbsp;!\
Debout&nbsp;! Plus votre coeur r&eacute;pugne &agrave; l\'accepter,\
Plus ce sera pour vous mati&egrave;re &agrave; m&eacute;riter&nbsp;:\
Mortifiez vos sens avec ce mariage,\
Et ne me rompez pas la t&ecirc;te davantage.

#### DORINE. {#dorine.-1 .dorine}

Mais quoi\...&nbsp;?

#### ORGON. {#orgon.-2 .orgon}

Taisez-vous, vous&nbsp;; parlez &agrave; votre &eacute;cot[^109]&nbsp;:\
Je vous d&eacute;fends tout net d\'oser dire un seul mot.

#### CL&Eacute;ANTE. {#cléante.-5 .cleante}

Si par quelque conseil vous souffrez qu\'on r&eacute;ponde\...

#### ORGON. {#orgon.-3 .orgon}

Mon fr&egrave;re, vos conseils sont les meilleurs du monde,\
Ils sont bien raisonn&eacute;s, et j\'en fais un grand cas&nbsp;;\
Mais vous trouverez bon que je n\'en use pas.

#### ELMIRE, *&agrave; son mari.* {#elmire-à-son-mari. .elmire}

&Agrave; voir ce que je vois, je ne sais plus que dire,\
Et votre aveuglement fait que je vous admire[^110]&nbsp;:\
C\'est &ecirc;tre bien coiff&eacute;[^111], bien pr&eacute;venu de lui,\
Que de nous d&eacute;mentir sur le fait d\'aujourd\'hui.

#### ORGON. {#orgon.-4 .orgon}

Je suis votre valet, et crois les apparences&nbsp;:\
Pour mon fripon de fils je sais vos complaisances,\
Et vous avez eu peur de le d&eacute;savouer\
Du trait qu\'&agrave; ce pauvre homme il a voulu jouer&nbsp;;\
Vous &eacute;tiez trop tranquille enfin pour &ecirc;tre crue,\
Et vous auriez parue d\'autre mani&egrave;re &eacute;mue.

#### ELMIRE. {#elmire. .elmire}

Est-ce qu\'au simple aveu d\'un amoureux transport\
Il faut que notre honneur se gendarme si fort&nbsp;?\
Et ne peut-on r&eacute;pondre &agrave; tout ce qui le touche\
Que le feu dans les yeux et l\'injure &agrave; la bouche&nbsp;?\
Pour moi, de tels propos je me ris simplement,\
Et l\'&eacute;clat l&agrave;-dessus ne me pla&icirc;t nullement&nbsp;;\
J\'aime qu\'avec douceur nous nous montrions sages,\
Et ne suis point du tout pour ces prudes sauvages\
Dont l\'honneur est arm&eacute; de griffes et de dents,\
Et veut au moindre mot d&eacute;visager[^112] les gens&nbsp;:\
Me pr&eacute;serve le Ciel d\'une telle sagesse&nbsp;!\
Je veux une vertu qui ne soit point diablesse,\
Et crois que d\'un refus la discr&egrave;te froideur\
N\'en est pas moins puissante &agrave; rebuter un coeur.

#### ORGON. {#orgon.-5 .orgon}

Enfin je sais l\'affaire et ne prends point le change.

#### ELMIRE. {#elmire.-1 .elmire}

J\'admire, encore un coup, cette foiblesse &eacute;trange.\
Mais que me r&eacute;pondroit votre incr&eacute;dulit&eacute;\
Si l\'on vous faisoit voir qu\'on vous dit v&eacute;rit&eacute;&nbsp;?

#### ORGON. {#orgon.-6 .orgon}

Voir&nbsp;?

#### ELMIRE. {#elmire.-2 .elmire}

Oui.

#### ORGON. {#orgon.-7 .orgon}

Chansons.

#### ELMIRE. {#elmire.-3 .elmire}

Mais quoi&nbsp;? si je trouvois mani&egrave;re\
De vous le faire voir avec pleine lumi&egrave;re&nbsp;?

#### ORGON. {#orgon.-8 .orgon}

Contes en l\'air.

#### ELMIRE. {#elmire.-4 .elmire}

Quel homme&nbsp;! Au moins r&eacute;pondez-moi.\
Je ne vous parle pas de nous ajouter foi&nbsp;;\
Mais supposons ici que, d\'un lieu qu\'on peut prendre,\
On vous f&icirc;t clairement tout voir et tout entendre.\
Que diriez-vous alors de votre homme de bien&nbsp;?

#### ORGON. {#orgon.-9 .orgon}

En ce cas, je dirois que\... Je ne dirois rien,\
Car cela ne se peut.

#### ELMIRE. {#elmire.-5 .elmire}

L\'erreur trop longtemps dure,\
Et c\'est trop condamner ma bouche d\'imposture.\
Il faut que par plaisir, et sans aller plus loin[^113],\
De tout ce qu\'on vous dit je vous fasse t&eacute;moin.

#### ORGON. {#orgon.-10 .orgon}

Soit&nbsp;: je vous prends au mot. Nous verrons votre adresse,\
Et comment vous pourrez remplir cette promesse.

#### ELMIRE. {#elmire.-6 .elmire}

Faites-le moi venir.

#### DORINE. {#dorine.-2 .dorine}

Son esprit est rus&eacute;,\
Et peut-&ecirc;tre &agrave; surprendre il sera malais&eacute;.

#### ELMIRE. {#elmire.-7 .elmire}

Non&nbsp;: on est ais&eacute;ment dup&eacute; par ce qu\'on aime,\
Et l\'amour-propre engage &agrave; se tromper soi-m&ecirc;me.\
*(Parlant &agrave; Cl&eacute;ante et &agrave; Mariane.)*\
Faites-le moi descendre. Et vous, retirez-vous.

-   -   -   -   -   -   -   -   

ACTE IV.
--------

### SC&Egrave;NE IV

\- Elmire, Orgon.

#### ELMIRE. {#elmire.-8 .elmire}

Approchons cette table, et vous mettez dessous.

#### ORGON. {#orgon.-11 .orgon}

Comment&nbsp;?

#### ELMIRE. {#elmire.-9 .elmire}

Vous bien cacher est un point n&eacute;cessaire.

#### ORGON. {#orgon.-12 .orgon}

Pourquoi sous cette table&nbsp;?

#### ELMIRE. {#elmire.-10 .elmire}

Ah, mon Dieu&nbsp;! laissez faire&nbsp;:\
J\'ai mon dessein en t&ecirc;te, et vous en jugerez.\
Mettez-vous l&agrave;, vous dis-je&nbsp;; et quand vous y serez,\
Gardez qu\'on ne vous voie et qu\'on ne vous entende.

#### ORGON. {#orgon.-13 .orgon}

Je confesse qu\'ici ma complaisance est grande&nbsp;;\
Mais de votre entreprise il faut vous voir sortir.

#### ELMIRE. {#elmire.-11 .elmire}

Vous n\'aurez, que je crois, rien &agrave; me repartir.\
*(&Agrave; son mari qui est sous la table.)*\
Au moins, je vais toucher une &eacute;trange mati&egrave;re&nbsp;:\
Ne vous scandalisez en aucune mani&egrave;re.\
Quoi que je puisse dire, il[^114] doit m\'&ecirc;tre permis,\
Et c\'est pour vous convaincre, ainsi que j\'ai promis.\
Je vais par des douceurs, puisque j\'y suis r&eacute;duite,\
Faire poser le masque &agrave; cette &acirc;me hypocrite,\
Flatter de son amour les d&eacute;sirs effront&eacute;s,\
Et donner un champ libre &agrave; ses t&eacute;m&eacute;rit&eacute;s.\
Comme c\'est pour vous seul,et pour mieux le confondre,\
Que mon &acirc;me &agrave; ses voeux va feindre de r&eacute;pondre,\
J\'aurai lieu de cesser d&egrave;s que vous vous rendrez,\
Et les choses n\'iront que jusqu\'o&ugrave; vous voudrez.\
C\'est &agrave; vous d\'arr&ecirc;ter son ardeur insens&eacute;e,\
Quand vous croirez l\'affaire assez avant pouss&eacute;e,\
D\'&eacute;pargner votre femme, et de ne m\'exposer\
Qu\'&agrave; ce qu\'il vous faudra pour vous d&eacute;sabuser&nbsp;:\
Ce sont vos int&eacute;r&ecirc;ts&nbsp;; vous en serez le ma&icirc;tre,\
Et\... L\'on vient. Tenez-vous, et gardez de para&icirc;tre.

-   

ACTE IV.
--------

### SC&Egrave;NE V

\- Tartuffe, Elmire, Orgon.

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

On m\'a dit qu\'en ce lieu vous me vouliez parler.

#### ELMIRE. {#elmire.-12 .elmire}

Oui. L\'on a des secrets &agrave; vous y r&eacute;v&eacute;ler.\
Mais tirez cette porte avant qu\'on vous les dise,\
Et regardez partout de crainte de surprise.\
Une affaire pareille &agrave; celle de tant&ocirc;t\
N\'est pas assur&eacute;ment ici ce qu\'il nous faut.\
Jamais il ne s\'est vu de surprise de m&ecirc;me&nbsp;;\
Damis m\'a fait pour vous une frayeur extr&ecirc;me,\
Et vous avez bien vu que j\'ai fait mes efforts\
Pour rompre ses desseins et calmer ses transports.\
Mon trouble, il est bien vrai, m\'a si fort
poss&eacute;d&eacute;e[^115],\
Que de le d&eacute;mentir je n\'ai point eu l\'id&eacute;e&nbsp;;\
Mais par l&agrave;, gr&acirc;ce au Ciel, tout a bien mieux
&eacute;t&eacute;,\
Et les choses en sont dans plus de s&ucirc;ret&eacute;[^116].\
L\'estime o&ugrave; l\'on vous tient a dissip&eacute; l\'orage,\
Et mon mari de vous ne peut prendre d\'ombrage.\
Pour mieux braver l\'&eacute;clat des mauvais jugements,\
Il veut que nous soyons ensemble &agrave; tous moments&nbsp;;\
Et c\'est par o&ugrave; je puis, sans peur d\'&ecirc;tre
bl&acirc;m&eacute;e,\
Me trouver ici seule avec vous enferm&eacute;e,\
Et ce qui m\'autorise &agrave; vous ouvrir un coeur\
Un peu trop prompt peut-&ecirc;tre &agrave; souffrir votre ardeur.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Ce langage &agrave; comprendre est assez difficile,\
Madame, et vous parliez tant&ocirc;t d\'un autre style.

#### ELMIRE. {#elmire.-13 .elmire}

Ah&nbsp;! si d\'un tel refus vous &ecirc;tes en courroux,\
Que le coeur d\'une femme est mal connu de vous&nbsp;!\
Et que vous savez peu ce qu\'il veut faire entendre\
Lorsque si foiblement on le voit se d&eacute;fendre&nbsp;!\
Toujours notre pudeur combat dans ces moments\
Ce qu\'on peut nous donner de tendres sentiments.\
Quelque raison qu\'on trouve &agrave; l\'amour qui nous dompte,\
On trouve &agrave; l\'avouer toujours un peu de honte&nbsp;;\
On s\'en d&eacute;fend d\'abord&nbsp;; mais de l\'air qu\'on s\'y
prend,\
On fait conno&icirc;tre assez que notre coeur se rend,\
Qu\'&agrave; nos voeux par honneur notre bouche s\'oppose,\
Et que de tels refus promettent toute chose.\
C\'est vous faire sans doute un assez libre aveu,\
Et sur notre pudeur me m&eacute;nager bien peu&nbsp;;\
Mais puisque la parole enfin en est l&acirc;ch&eacute;e,\
&Agrave; retenir Damis me serois-je attach&eacute;e,\
Aurois-je, je vous prie, avec tant de douceur\
&Eacute;cout&eacute; tout au long l\'offre de votre coeur,\
Aurois-je pris la chose ainsi qu\'on m\'a vu faire,\
Si l\'offre de ce coeur n\'e&ucirc;t eu de quoi me plaire &nbsp;?\
Et lorsque j\'ai voulu moi-m&ecirc;me vous forcer\
&Agrave; refuser l\'hymen qu\'on venoit d\'annoncer,\
Qu\'est-ce que cette instance a d&ucirc; vous faire entendre,\
Que l\'int&eacute;r&ecirc;t[^117] qu\'en vous on s\'avise de prendre,\
Et l\'ennui qu\'on auroit que ce noeud qu\'on r&eacute;sout\
V&icirc;nt partager du moins un coeur que l\'on veut tout&nbsp;?

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

C\'est sans doute[^118], Madame, une douceur extr&ecirc;me\
Que d\'entendre ces mots d\'une bouche qu\'on aime&nbsp;:\
Leur miel dans tous mes sens fait couler &agrave; longs traits\
Une suavit&eacute; qu\'on ne go&ucirc;ta jamais.\
Le bonheur de vous plaire est ma supr&ecirc;me &eacute;tude,\
Et mon coeur de vos voeux fait sa b&eacute;atitude&nbsp;;\
Mais ce coeur vous demande ici la libert&eacute;\
D\'oser douter un peu de sa f&eacute;licit&eacute;.\
Je puis croire ces mots un artifice honn&ecirc;te\
Pour m\'obliger &agrave; rompre un hymen qui s\'appr&ecirc;te&nbsp;;\
Et s\'il faut librement m\'expliquer avec vous,\
Je ne me fierai point &agrave; des propos si doux,\
Qu\'un peu de vos faveurs, apr&egrave;s quoi je soupire,\
Ne vienne m\'assurer tout ce qu\'ils m\'ont pu dire,\
Et planter dans mon &acirc;me une constante foi\
Des charmantes bont&eacute;s que vous avez pour moi.

#### ELMIRE. *Elle tousse pour avertir son mari.* {#elmire.-elle-tousse-pour-avertir-son-mari. .elmire}

Quoi&nbsp;? vous voulez aller avec cette vitesse,\
Et d\'un coeur tout d\'abord &eacute;puiser la tendresse&nbsp;?\
On se tue &agrave; vous faire un aveu des plus doux&nbsp;;\
Cependant ce n\'est pas encore assez pour vous,\
Et l\'on ne peut aller jusqu\'&agrave; vous satisfaire,\
Qu\'aux derni&egrave;res faveurs on ne pousse l\'affaire[^119]&nbsp;?

#### TARTUFFE. {#tartuffe.-7 .tartuffe}

Moins on m&eacute;rite un bien, moins on l\'ose esp&eacute;rer.\
Nos voeux sur des discours ont peine &agrave; s\'assurer.\
On soup&ccedil;onne ais&eacute;ment un sort[^120] tout plein de gloire,\
Et l\'on veut en jouir avant que de le croire.\
Pour moi, qui crois si peu m&eacute;riter vos bont&eacute;s,\
Je doute du bonheur de mes t&eacute;m&eacute;rit&eacute;s[^121]&nbsp;;\
Et je ne croirai rien, que vous n\'ayez, Madame,\
Par des r&eacute;alit&eacute;s su convaincre ma flamme.

#### ELMIRE. {#elmire.-14 .elmire}

Mon Dieu, que votre amour en vrai tyran agit,\
Et qu\'en un trouble &eacute;trange il me jette l\'esprit&nbsp;!\
Que sur les coeurs il prend un furieux empire,\
Et qu\'avec violence il veut ce qu\'il d&eacute;sire&nbsp;!\
Quoi&nbsp;? de votre poursuite on ne peut se parer[^122],\
Et vous ne donnez pas le temps de respirer&nbsp;?\
Sied-il bien de tenir une rigueur si grande,\
De vouloir sans quartier les choses qu\'on demande,\
Et d\'abuser ainsi par vos efforts pressants\
Du foible que pour vous vous voyez qu\'ont les gens&nbsp;?

#### TARTUFFE. {#tartuffe.-8 .tartuffe}

Mais si d\'un oeil b&eacute;nin vous voyez mes hommages,\
Pourquoi m\'en refuser d\'assur&eacute;s t&eacute;moignages&nbsp;?

#### ELMIRE. {#elmire.-15 .elmire}

Mais comment consentir &agrave; ce que vous voulez,\
Sans offenser le Ciel, dont toujours vous parlez&nbsp;?

#### TARTUFFE. {#tartuffe.-9 .tartuffe}

Si ce n\'est que le Ciel qu\'&agrave; mes voeux on oppose,\
Lever un tel obstacle est &agrave; moi peu de chose,\
Et cela ne doit pas retenir votre coeur.

#### ELMIRE. {#elmire.-16 .elmire}

Mais des arr&ecirc;ts du Ciel on nous fait tant de peur&nbsp;!

#### TARTUFFE. {#tartuffe.-10 .tartuffe}

Je puis vous dissiper ces craintes ridicules,\
Madame, et je sais l\'art de lever les scrupules.\
Le Ciel d&eacute;fend, de vrai, certains contentements&nbsp;;\
*(C\'est un sc&eacute;l&eacute;rat qui parle.)*\
Mais on trouve avec lui des accommodements&nbsp;;\
Selon divers besoins, il est une science\
D\'&eacute;tendre les liens de notre conscience,\
Et de rectifier le mal de l\'action\
Avec la puret&eacute; de notre intention[^123].\
De ces secrets, Madame, on saura vous instruire&nbsp;;\
Vous n\'avez seulement qu\'&agrave; vous laisser conduire.\
Contentez mon d&eacute;sir, et n\'ayez point d\'effroi &nbsp;:\
Je vous r&eacute;ponds de tout, et prends le mal sur moi.\
Vous toussez fort, Madame.

#### ELMIRE. {#elmire.-17 .elmire}

Oui, je suis au supplice.

#### TARTUFFE. {#tartuffe.-11 .tartuffe}

Vous pla&icirc;t-il un morceau de ce jus de r&eacute;glisse &nbsp;?

#### ELMIRE. {#elmire.-18 .elmire}

C\'est un rhume obstin&eacute;, sans doute, et je vois bien\
Que tous les jus du monde ici ne feront rien.

#### TARTUFFE. {#tartuffe.-12 .tartuffe}

Cela certe est f&acirc;cheux.

#### ELMIRE. {#elmire.-19 .elmire}

Oui, plus qu\'on ne peut dire.

#### TARTUFFE. {#tartuffe.-13 .tartuffe}

Enfin votre scrupule est facile &agrave; d&eacute;truire&nbsp;:\
Vous &ecirc;tes assur&eacute;e ici d\'un plein secret,\
Et le mal n\'est jamais que dans l\'&eacute;clat qu\'on fait &nbsp;;\
Le scandale du monde est ce qui fait l\'offense,\
Et ce n\'est pas p&eacute;cher que p&eacute;cher en silence.

#### ELMIRE, *apr&egrave;s avoir encore touss&eacute;*. {#elmire-après-avoir-encore-toussé. .elmire}

Enfin je vois qu\'il faut se r&eacute;soudre &agrave; c&eacute;der,\
Qu\'il faut que je consente &agrave; vous tout accorder,\
Et qu\'&agrave; moins de cela je ne dois point pr&eacute;tendre\
Qu\'on puisse &ecirc;tre content et qu\'on veuille se rendre.\
Sans doute[^124], il est f&acirc;cheux d\'en venir jusque-l&agrave;,\
Et c\'est bien malgr&eacute; moi que je franchis cela&nbsp;;\
Mais puisque l\'on s\'obstine &agrave; m\'y vouloir r&eacute;duire,\
Puisqu\'on ne veut point croire &agrave; tout ce qu\'on peut dire,\
Et qu\'on veut des t&eacute;moins qui soient plus convaincants,\
Il faut bien s\'y r&eacute;soudre, et contenter les gens.\
Si ce consentement porte en soi quelque offense,\
Tant pis pour qui me force &agrave; cette violence&nbsp;;\
La faute assur&eacute;ment n\'en doit pas &ecirc;tre &agrave; moi.

#### TARTUFFE. {#tartuffe.-14 .tartuffe}

Oui, Madame, on s\'en charge&nbsp;; et la chose de soi\...

#### ELMIRE. {#elmire.-20 .elmire}

Ouvrez un peu la porte, et voyez, je vous prie,\
Si mon mari n\'est point dans cette galerie.

#### TARTUFFE. {#tartuffe.-15 .tartuffe}

Qu\'est-il besoin pour lui du soin que vous prenez&nbsp;?\
C\'est un homme, entre nous, &agrave; mener par le nez &nbsp;;\
De tous nos entretiens il est pour faire gloire,\
Et je l\'ai mis au point de voir tout sans rien croire.

#### ELMIRE. {#elmire.-21 .elmire}

Il n\'importe&nbsp;: sortez, je vous prie, un moment,\
Et partout l&agrave; dehors voyez exactement.

-   -   -   -   -   -   -   -   -   -   

ACTE IV.
--------

### SC&Egrave;NE VI

\- Orgon, Elmire.

#### ORGON, *sortant de dessous la table.* {#orgon-sortant-de-dessous-la-table. .orgon}

Voil&agrave;, je vous l\'avoue, un abominable homme.\
Je n\'en puis revenir, et tout ceci m\'assomme.

#### ELMIRE. {#elmire.-22 .elmire}

Quoi&nbsp;? vous sortez si t&ocirc;t ? vous vous moquez des gens.\
Rentrez sous le tapis, il n\'est pas encor temps&nbsp;;\
Attendez jusqu\'au bout pour voir les choses s&ucirc;res,\
Et ne vous fiez point aux simples conjectures.

#### ORGON. {#orgon.-14 .orgon}

Non, rien de plus m&eacute;chant n\'est sorti de l\'enfer.

#### ELMIRE. {#elmire.-23 .elmire}

Mon Dieu&nbsp;! l\'on ne doit point croire trop de l&eacute;ger.\
Laissez-vous bien convaincre avant que de vous rendre,\
Et ne vous h&acirc;tez point, de peur de vous m&eacute;prendre[^125].\
*(Elle fait mettre son mari derri&egrave;re elle.)*

-   

ACTE IV.
--------

### SC&Egrave;NE VII

\- Tartuffe, Elmire, Orgon.

#### TARTUFFE. {#tartuffe.-16 .tartuffe}

Tout conspire, Madame, &agrave; mon contentement&nbsp;:\
J\'ai visit&eacute; de l\'oeil tout cet appartement&nbsp;;\
Personne ne s\'y trouve ; et mon &acirc;me ravie\...

#### ORGON, *en l\'arr&ecirc;tant.* {#orgon-en-larrêtant. .orgon}

Tout doux&nbsp;! vous suivez trop votre amoureuse envie,\
Et vous ne devez pas vous tant passionner.\
Ah&nbsp;! ah&nbsp;! l\'homme de bien, vous m\'en voulez
donner[^126]&nbsp;!\
Comme aux tentations s\'abandonne votre &acirc;me&nbsp;!\
Vous &eacute;pousiez ma fille, et convoitiez ma femme !\
J\'ai dout&eacute; fort longtemps que ce f&ucirc;t tout de bon,\
Et je croyois toujours qu\'on changeroit de ton &nbsp;;\
Mais c\'est assez avant pousser le t&eacute;moignage &nbsp;:\
Je m\'y tiens, et n\'en veux, pour moi, pas davantage.

#### ELMIRE, *&agrave; Tartuffe.* {#elmire-à-tartuffe. .elmire}

C\'est contre mon humeur que j\'ai fait tout ceci &nbsp;;\
Mais on m\'a mise au point de vous traiter ainsi.

#### TARTUFFE. {#tartuffe.-17 .tartuffe}

Quoi&nbsp;? vous croyez\...&nbsp;?

#### ORGON. {#orgon.-15 .orgon}

Allons, point de bruit, je vous prie.\
D&eacute;nichez de c&eacute;ans, et sans c&eacute;r&eacute;monie.

#### TARTUFFE. {#tartuffe.-18 .tartuffe}

Mon dessein\...

#### ORGON. {#orgon.-16 .orgon}

Ces discours ne sont plus de saison&nbsp;:\
Il faut, tout sur-le-champ, sortir de la maison.

#### TARTUFFE. {#tartuffe.-19 .tartuffe}

C\'est &agrave; vous d\'en sortir, vous qui parlez en
ma&icirc;tre&nbsp;:\
La maison m\'appartient, je le ferai conna&icirc;tre,\
Et vous montrerai bien qu\'en vain on a recours,\
Pour me chercher querelle, &agrave; ces l&acirc;ches d&eacute;tours,\
Qu\'on n\'est pas o&ugrave; l\'on pense en me faisant injure,\
Que j\'ai de quoi confondre et punir l\'imposture,\
Venger le Ciel qu\'on blesse, et faire repentir\
Ceux qui parlent ici de me faire sortir.

-   

ACTE IV.
--------

### SC&Egrave;NE VIII

\- Elmire, Orgon.

#### ELMIRE. {#elmire.-24 .elmire}

Quel est donc ce langage&nbsp;? et qu\'est-ce qu\'il veut dire&nbsp;?

#### ORGON. {#orgon.-17 .orgon}

Ma foi, je suis confus, et n\'ai pas lieu de rire.

#### ELMIRE. {#elmire.-25 .elmire}

Comment&nbsp;?

#### ORGON. {#orgon.-18 .orgon}

Je vois ma faute aux choses qu\'il me dit,\
Et la donation m\'embarrasse l\'esprit.

#### ELMIRE. {#elmire.-26 .elmire}

La donation\...

#### ORGON. {#orgon.-19 .orgon}

Oui, c\'est une affaire faite.\
Mais j\'ai quelque autre chose encor qui m\'inqui&egrave;te.

#### ELMIRE. {#elmire.-27 .elmire}

Et quoi&nbsp;?

#### ORGON. {#orgon.-20 .orgon}

Vous saurez tout. Mais voyons au plus t&ocirc;t\
Si certaine cassette est encore l&agrave;-haut.

ACTE V.
-------

### SC&Egrave;NE I

\- Orgon, Cl&eacute;ante.

#### CL&Eacute;ANTE. {#cléante. .cleante}

O&ugrave; voulez-vous courir&nbsp;?

#### ORGON. {#orgon. .orgon}

Las&nbsp;! que sais-je&nbsp;?

#### CL&Eacute;ANTE. {#cléante.-1 .cleante}

Il me semble\
Que l\'on doit commencer par consulter ensemble\
Les choses qu\'on peut faire en cet &eacute;v&eacute;nement.

#### ORGON. {#orgon.-1 .orgon}

Cette cassette-l&agrave; me trouble enti&egrave;rement &nbsp;;\
Plus que le reste encore elle me d&eacute;sesp&egrave;re.

#### CL&Eacute;ANTE. {#cléante.-2 .cleante}

Cette cassette est donc un important myst&egrave;re&nbsp;?

#### ORGON. {#orgon.-2 .orgon}

C\'est un d&eacute;p&ocirc;t qu\'Argas, cet ami que je plains,\
Lui-m&ecirc;me, en grand secret, m\'a mis entre les mains&nbsp;:\
Pour cela, dans sa fuite il me voulut &eacute;lire[^127]&nbsp;;\
Et ce sont des papiers, &agrave; ce qu\'il m\'a pu dire,\
O&ugrave; sa vie et ses biens se trouvent attach&eacute;s.

#### CL&Eacute;ANTE. {#cléante.-3 .cleante}

Pourquoi donc les avoir en d\'autres mains l&acirc;ch&eacute;s&nbsp;?

#### ORGON. {#orgon.-3 .orgon}

Ce fut par un motif de cas de conscience&nbsp;:\
J\'allai droit &agrave; mon tra&icirc;tre en faire confidence&nbsp;;\
Et son raisonnement me vint persuader\
De lui donner plut&ocirc;t la cassette &agrave; garder,\
Afin que, pour nier, en cas de quelque enqu&ecirc;te,\
J\'eusse d\'un faux-fuyant la faveur toute pr&ecirc;te,\
Par o&ugrave; ma conscience e&ucirc;t pleine s&ucirc;ret&eacute;\
&Agrave; faire des serments contre la v&eacute;rit&eacute;.

#### CL&Eacute;ANTE. {#cléante.-4 .cleante}

Vous voil&agrave; mal, au moins si j\'en crois l\'apparence&nbsp;;\
Et la donation, et cette confidence[^128],\
Sont, &agrave; vous en parler selon mon sentiment,\
Des d&eacute;marches par vous faites l&eacute;g&egrave;rement.\
On peut vous mener loin avec de pareils gages &nbsp;;\
Et cet homme sur vous ayant ces avantages,\
Le pousser est encor grande imprudence &agrave; vous,\
Et vous deviez chercher quelque biais plus doux.

#### ORGON. {#orgon.-4 .orgon}

Quoi&nbsp;? sous un beau semblant[^129] de ferveur si touchante\
Cacher un coeur si double, une &acirc;me si m&eacute;chante&nbsp;!\
Et moi qui l\'ai re&ccedil;u gueusant et n\'ayant rien\...\
C\'en est fait, je renonce &agrave; tous les gens de bien&nbsp;:\
J\'en aurai d&eacute;sormais une horreur effroyable,\
Et m\'en vais devenir pour eux pire qu\'un diable.

#### CL&Eacute;ANTE. {#cléante.-5 .cleante}

H&eacute; bien&nbsp;! ne voil&agrave; pas de vos emportements&nbsp;!\
Vous ne gardez en rien les doux temp&eacute;raments&nbsp;;\
Dans la droite raison jamais n\'entre la v&ocirc;tre,\
Et toujours d\'un exc&egrave;s vous vous jetez dans l\'autre.\
Vous voyez votre erreur, et vous avez connu\
Que par un z&egrave;le feint vous &eacute;tiez pr&eacute;venu&nbsp;;\
Mais pour vous corriger, quelle raison demande\
Que vous alliez passer dans une erreur plus grande,\
Et qu\'avecque le coeur d\'un perfide vaurien\
Vous confondiez les coeurs de tous les gens de bien&nbsp;?\
Quoi&nbsp;? parce qu\'un fripon vous dupe avec audace\
Sous le pompeux &eacute;clat d\'une aust&egrave;re grimace,\
Vous voulez que partout on soit fait comme lui,\
Et qu\'aucun vrai d&eacute;vot ne se trouve aujourd\'hui&nbsp;?\
Laissez aux libertins ces sottes cons&eacute;quences&nbsp;;\
D&eacute;m&ecirc;lez la vertu d\'avec ses apparences,\
Ne hasardez jamais votre estime trop t&ocirc;t,\
Et soyez pour cela dans le milieu qu\'il faut&nbsp;:\
Gardez-vous, s\'il se peut, d\'honorer l\'imposture,\
Mais au vrai z&egrave;le aussi n\'allez pas faire injure&nbsp;;\
Et s\'il vous faut tomber dans une extr&eacute;mit&eacute;,\
P&eacute;chez plut&ocirc;t encor de cet autre c&ocirc;t&eacute;.

-   -   -   

ACTE V.
-------

### SC&Egrave;NE II

\- Damis, Orgon, Cl&eacute;ante.

#### DAMIS. {#damis. .damis}

Quoi&nbsp;? mon p&egrave;re, est-il vrai qu\'un coquin vous
menace&nbsp;?\
Qu\'il n\'est point de bienfait qu\'en son &acirc;me il n\'efface,\
Et que son l&acirc;che orgueil, trop digne de courroux,\
Se fait de vos bont&eacute;s des armes contre vous&nbsp;?

#### ORGON. {#orgon.-5 .orgon}

Oui, mon fils, et j\'en sens des douleurs non pareilles.

#### DAMIS. {#damis.-1 .damis}

Laissez-moi, je lui veux couper les deux oreilles&nbsp;:\
Contre son insolence on ne doit point gauchir[^130]&nbsp;;\
C\'est &agrave; moi, tout d\'un coup, de vous en affranchir,\
Et pour sortir d\'affaire, il faut que je l\'assomme.

#### CL&Eacute;ANTE. {#cléante.-6 .cleante}

Voil&agrave; tout justement parler en vrai jeune homme.\
Mod&eacute;rez, s\'il vous pla&icirc;t, ces transports
&eacute;clatants&nbsp;:\
Nous vivons sous un r&egrave;gne et sommes dans un temps\
O&ugrave; par la violence on fait mal ses affaires.

-   

ACTE V.
-------

### SC&Egrave;NE III

\- Madame Pernelle, Mariane, Elmire, Dorine, Damis, Orgon,
Cl&eacute;ante.

#### MADAME PERNELLE. {#madame-pernelle. .pernelle}

Qu\'est-ce&nbsp;? J\'apprends ici de terribles myst&egrave;res[^131].

#### ORGON. {#orgon.-6 .orgon}

Ce sont des nouveaut&eacute;s dont mes yeux sont t&eacute;moins,\
Et vous voyez le prix dont sont pay&eacute;s mes soins.\
Je recueille avec z&egrave;le un homme en sa mis&egrave;re,\
Je le loge, et le tiens comme mon propre fr&egrave;re&nbsp;;\
De bienfaits chaque jour il est par moi charg&eacute;&nbsp;;\
Je lui donne ma fille et tout le bien que j\'ai&nbsp;;\
Et, dans le m&ecirc;me temps, le perfide, l\'inf&acirc;me,\
Tente le noir dessein de suborner ma femme,\
Et non content encor de ces l&acirc;ches essais,\
Il m\'ose menacer de mes propres bienfaits,\
Et veut, &agrave; ma ruine, user des avantages\
Dont le viennent d\'armer mes bont&eacute;s trop peu sages,\
Me chasser de mes biens, o&ugrave; je l\'ai transf&eacute;r&eacute;,\
Et me r&eacute;duire au point d\'o&ugrave; je l\'ai retir&eacute;.

#### DORINE. {#dorine. .dorine}

Le pauvre homme&nbsp;!

#### MADAME PERNELLE. {#madame-pernelle.-1 .pernelle}

Mon fils, je ne puis du tout croire\
Qu\'il ait voulu commettre une action si noire.

#### ORGON. {#orgon.-7 .orgon}

Comment&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-2 .pernelle}

Les gens de bien sont envi&eacute;s toujours.

#### ORGON. {#orgon.-8 .orgon}

Que voulez-vous donc dire avec votre discours,\
Ma m&egrave;re&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-3 .pernelle}

Que chez vous on vit d\'&eacute;trange sorte,\
Et qu\'on ne sait que trop la haine qu\'on lui porte.

#### ORGON. {#orgon.-9 .orgon}

Qu\'&agrave; cette haine &agrave; faire avec ce qu\'on vous dit&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-4 .pernelle}

Je vous l\'ai dit cent fois quand vous &eacute;tiez petit&nbsp;:\
La vertu dans le monde est toujours poursuivie&nbsp;;\
Les envieux mourront, mais non jamais l\'envie.

#### ORGON. {#orgon.-10 .orgon}

Mais que fait ce discours aux choses d\'aujourd\'hui&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-5 .pernelle}

On vous aura forg&eacute; cent sots contes de lui.

#### ORGON. {#orgon.-11 .orgon}

Je vous ai d&eacute;j&agrave; dit que j\'ai vu tout moi-m&ecirc;me.

#### MADAME PERNELLE. {#madame-pernelle.-6 .pernelle}

Des esprits m&eacute;disants la malice est extr&ecirc;me.

#### ORGON. {#orgon.-12 .orgon}

Vous me feriez damner, ma m&egrave;re. Je vous di\
Que j\'ai vu de mes yeux un crime si hardi.

#### MADAME PERNELLE. {#madame-pernelle.-7 .pernelle}

Les langues ont toujours du venin &agrave; r&eacute;pandre,\
Et rien n\'est ici-bas qui s\'en puisse d&eacute;fendre.

#### ORGON. {#orgon.-13 .orgon}

C\'est tenir un propos de sens bien d&eacute;pourvu.\
Je l\'ai vu, dis-je, vu, de mes propres yeux vu,\
Ce qui s\'appelle vu &nbsp;: faut-il vous le rebattre\
Aux oreilles cent fois, et crier comme quatre &nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-8 .pernelle}

Mon Dieu, le plus souvent l\'apparence d&eacute;&ccedil;oit &nbsp;:\
Il ne faut pas toujours juger sur ce qu\'on voit.

#### ORGON. {#orgon.-14 .orgon}

J\'enrage.

#### MADAME PERNELLE. {#madame-pernelle.-9 .pernelle}

Aux faux soup&ccedil;ons la nature est sujette,\
Et c\'est souvent &agrave; mal que le bien s\'interpr&egrave;te.

#### ORGON. {#orgon.-15 .orgon}

Je dois interpr&eacute;ter &agrave; charitable soin\
Le d&eacute;sir d\'embrasser ma femme&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-10 .pernelle}

Il est besoin,\
Pour accuser les gens, d\'avoir de justes causes&nbsp;;\
Et vous deviez attendre &agrave; vous voir s&ucirc;r des choses.

#### ORGON. {#orgon.-16 .orgon}

H&eacute;, diantre&nbsp;! le moyen de m\'en assurer mieux&nbsp;?\
Je devois donc, ma m&egrave;re, attendre qu\'&agrave; mes yeux\
Il e&ucirc;t\... Vous me feriez dire quelque sottise.

#### MADAME PERNELLE. {#madame-pernelle.-11 .pernelle}

Enfin d\'un trop pur z&egrave;le on voit son &acirc;me
&eacute;prise&nbsp;;\
Et je ne puis du tout me mettre dans l\'esprit\
Qu\'il ait voulu tenter les choses que l\'on dit.

#### ORGON. {#orgon.-17 .orgon}

Allez, je ne sais pas, si vous n\'&eacute;tiez ma m&egrave;re,\
Ce que je vous dirois, tant je suis en col&egrave;re.

#### DORINE. {#dorine.-1 .dorine}

Juste retour, Monsieur, des choses d\'ici-bas&nbsp;:\
Vous ne vouliez point croire, et l\'on ne vous croit pas.

#### CL&Eacute;ANTE. {#cléante.-7 .cleante}

Nous perdons des moments en bagatelles pures,\
Qu\'il faudroit employer &agrave; prendre des mesures.\
Aux menaces du fourbe on doit ne dormir point[^132].

#### DAMIS. {#damis.-2 .damis}

Quoi&nbsp;? son effronterie iroit jusqu\'&agrave; ce point&nbsp;?

#### ELMIRE. {#elmire. .elmire}

Pour moi, je ne crois pas cette instance[^133] possible,\
Et son ingratitude est ici trop visible.

#### CL&Eacute;ANTE. {#cléante.-8 .cleante}

Ne vous y fiez pas&nbsp;: il aura des ressorts\
Pour donner contre vous raison &agrave; ses efforts&nbsp;;\
Et sur moins que cela, le poids d\'une cabale\
Embarrasse les gens dans un f&acirc;cheux d&eacute;dale.\
Je vous le dis encore&nbsp;: arm&eacute; de ce qu\'il a,\
Vous ne deviez jamais le pousser jusque l&agrave;.

#### ORGON. {#orgon.-18 .orgon}

Il est vrai&nbsp;; mais qu\'y faire&nbsp;? &Agrave; l\'orgueil de ce
tra&icirc;tre[^134],\
De mes ressentiments je n\'ai pas &eacute;t&eacute; ma&icirc;tre.

#### CL&Eacute;ANTE. {#cléante.-9 .cleante}

Je voudrois, de bon coeur, qu\'on p&ucirc;t entre vous deux\
De quelque ombre de paix raccommoder les noeuds.

#### ELMIRE. {#elmire.-1 .elmire}

Si j\'avois su qu\'en main il a de telles armes,\
Je n\'aurois pas donn&eacute; mati&egrave;re &agrave; tant d\'alarmes,\
Et mes\...

#### ORGON. {#orgon.-19 .orgon}

Que veut cet homme&nbsp;? Allez t&ocirc;t le savoir.\
Je suis bien en &eacute;tat que l\'on me vienne voir &nbsp;!

-   -   -   -   

ACTE V.
-------

### SC&Egrave;NE IV

\- Monsieur Loyal, Madame Pernelle, Orgon, Damis, Mariane, Dorine,
Elmire, Cl&eacute;ante.

#### MONSIEUR LOYAL. {#monsieur-loyal. .loyal}

Bonjour, ma ch&egrave;re soeur&nbsp;; faites, je vous supplie,\
Que je parle &agrave; Monsieur.

#### DORINE. {#dorine.-2 .dorine}

Il est en compagnie,\
Et je doute qu\'il puisse &agrave; pr&eacute;sent voir quelqu\'un.

#### MONSIEUR LOYAL. {#monsieur-loyal.-1 .loyal}

Je ne suis pas pour &ecirc;tre en ces lieux importun.\
Mon abord n\'aura rien, je crois, qui lui d&eacute;plaise&nbsp;;\
Et je viens pour un fait dont il sera bien aise.

#### DORINE. {#dorine.-3 .dorine}

Votre nom&nbsp;?

#### MONSIEUR LOYAL. {#monsieur-loyal.-2 .loyal}

Dites-lui seulement que je vien\
De la part de Monsieur Tartuffe, pour son bien.

#### DORINE. {#dorine.-4 .dorine}

C\'est un homme qui vient, avec douce mani&egrave;re,\
De la part de Monsieur Tartuffe, pour affaire\
Dont vous serez, dit-il, bien aise.

#### CL&Eacute;ANTE. {#cléante.-10 .cleante}

Il vous faut voir\
Ce que c\'est que cet homme, et ce qu\'il peut vouloir.

#### ORGON. {#orgon.-20 .orgon}

Pour nous raccommoder il vient ici peut-&ecirc;tre&nbsp;:\
Quels sentiments aurai-je &agrave; lui faire paro&icirc;tre&nbsp;?

#### CL&Eacute;ANTE. {#cléante.-11 .cleante}

Votre ressentiment ne doit point &eacute;clater&nbsp;;\
Et s\'il parle d\'accord, il le faut &eacute;couter.

#### MONSIEUR LOYAL. {#monsieur-loyal.-3 .loyal}

Salut, Monsieur. Le Ciel perde qui vous veut nuire,\
Et vous soit favorable autant que je d&eacute;sire&nbsp;!

#### ORGON. {#orgon.-21 .orgon}

Ce doux d&eacute;but s\'accorde avec mon jugement,\
Et pr&eacute;sage d&eacute;j&agrave; quelque accommodement.

#### MONSIEUR LOYAL. {#monsieur-loyal.-4 .loyal}

Toute votre maison m\'a toujours &eacute;t&eacute; ch&egrave;re,\
Et j\'&eacute;tois serviteur de Monsieur votre p&egrave;re.

#### ORGON. {#orgon.-22 .orgon}

Monsieur, j\'ai grande honte et demande pardon\
D\'&ecirc;tre sans vous conno&icirc;tre ou savoir votre nom.

#### MONSIEUR LOYAL. {#monsieur-loyal.-5 .loyal}

Je m\'appelle Loyal, natif de Normandie,\
Et suis huissier &agrave; verge[^135], en d&eacute;pit de l\'envie.\
J\'ai depuis quarante ans, gr&acirc;ce au Ciel, le bonheur\
D\'en exercer la charge avec beaucoup d\'honneur&nbsp;;\
Et je vous viens, Monsieur, avec votre licence,\
Signifier l\'exploit de certaine ordonnance\...

#### ORGON. {#orgon.-23 .orgon}

Quoi&nbsp;? vous &ecirc;tes ici\...

#### MONSIEUR LOYAL. {#monsieur-loyal.-6 .loyal}

Monsieur, sans passion&nbsp;:\
Ce n\'est rien seulement qu\'une sommation,\
Un ordre de vuider d\'ici, vous et les v&ocirc;tres,\
Mettre vos meubles hors, et faire place &agrave; d\'autres,\
Sans d&eacute;lai ni remise, ainsi que besoin est\...

#### ORGON. {#orgon.-24 .orgon}

Moi, sortir de c&eacute;ans&nbsp;?

#### MONSIEUR LOYAL. {#monsieur-loyal.-7 .loyal}

Oui, Monsieur, s\'il vous pla&icirc;t.\
La maison &agrave; pr&eacute;sent, comme savez de reste,\
Au bon Monsieur Tartuffe appartient sans conteste.\
De vos biens d&eacute;sormais il est ma&icirc;tre et seigneur,\
En vertu d\'un contrat duquel je suis porteur&nbsp;:\
Il est en bonne forme, et l\'on n\'y peut rien dire.

#### DAMIS. {#damis.-3 .damis}

Certes cette impudence est grande, et je l\'admire[^136].

#### MONSIEUR LOYAL. {#monsieur-loyal.-8 .loyal}

Monsieur, je ne dois point avoir affaire &agrave; vous&nbsp;;\
C\'est &agrave; Monsieur&nbsp;: il est et raisonnable et doux,\
Et d\'un homme de bien il sait trop bien l\'office[^137],\
Pour se vouloir du tout opposer &agrave; justice.

#### ORGON. {#orgon.-25 .orgon}

Mais\...

#### MONSIEUR LOYAL. {#monsieur-loyal.-9 .loyal}

Oui, Monsieur, je sais que pour un million\
Vous ne voudriez pas faire r&eacute;bellion,\
Et que vous souffrirez, en honn&ecirc;te personne,\
Que j\'ex&eacute;cute ici les ordres qu\'on me donne.

#### DAMIS. {#damis.-4 .damis}

Vous pourriez bien ici sur votre noir jupon[^138],\
Monsieur l\'huissier &agrave; verge, attirer le b&acirc;ton.

#### MONSIEUR LOYAL. {#monsieur-loyal.-10 .loyal}

Faites que votre fils se taise ou se retire,\
Monsieur. J\'aurois regret d\'&ecirc;tre oblig&eacute;
d\'&eacute;crire,\
Et de vous voir couch&eacute; dans mon proc&egrave;s-verbal.

#### DORINE. {#dorine.-5 .dorine}

Ce Monsieur Loyal porte un air bien d&eacute;loyal&nbsp;!

#### MONSIEUR LOYAL. {#monsieur-loyal.-11 .loyal}

Pour tous les gens de bien j\'ai de grandes tendresses,\
Et ne me suis voulu, Monsieur, charger des pi&egrave;ces\
Que pour vous obliger et vous faire plaisir,\
Que pour &ocirc;ter par l&agrave; le moyen d\'en choisir\
Qui, n\'ayant point pour vous le z&egrave;le qui me pousse,\
Auroient pu proc&eacute;der d\'une fa&ccedil;on moins douce.

#### ORGON. {#orgon.-26 .orgon}

Et que peut-on de pis que d\'ordonner aux gens\
De sortir de chez eux&nbsp;?

#### MONSIEUR LOYAL. {#monsieur-loyal.-12 .loyal}

On vous donne du temps,\
Et jusques &agrave; demain je ferai surs&eacute;ance\
&Agrave; l\'ex&eacute;cution, Monsieur, de l\'ordonnance.\
Je viendrai seulement passer ici la nuit,\
Avec dix de mes gens, sans scandale et sans bruit.\
Pour la forme, il faudra, s\'il vous pla&icirc;t, qu\'on m\'apporte,\
Avant que se coucher, les clefs de votre porte.\
J\'aurai soin de ne pas troubler votre repos,\
Et de ne rien souffrir qui ne soit &agrave; propos.\
Mais demain, du matin, il vous faut &ecirc;tre habile\
&Agrave; vuider de c&eacute;ans jusqu\'au moindre ustensile&nbsp;:\
Mes gens vous aideront, et je les ai pris forts,\
Pour vous faire service &agrave; tout mettre dehors.\
On n\'en peut pas user mieux que je fais, je pense&nbsp;;\
Et comme je vous traite avec grande indulgence,\
Je vous conjure aussi, Monsieur, d\'en user bien,\
Et qu\'au d&ucirc; de ma charge on ne me trouble en rien.

#### ORGON. {#orgon.-27 .orgon}

Du meilleur de mon coeur je donnerois sur l\'heure\
Les cent plus beaux louis de ce qui me demeure,\
Et pouvoir &agrave; plaisir sur ce mufle assener\
Le plus grand coup de poing qui se puisse donner[^139].

#### CL&Eacute;ANTE. {#cléante.-12 .cleante}

Laissez, ne g&acirc;tons rien.

#### DAMIS. {#damis.-5 .damis}

&Agrave; cette audace &eacute;trange,\
J\'ai peine &agrave; me tenir, et la main me d&eacute;mange[^140].

#### DORINE. {#dorine.-6 .dorine}

Avec un si bon dos, ma foi, Monsieur Loyal,\
Quelques coups de b&acirc;ton ne vous si&eacute;roient pas mal.

#### MONSIEUR LOYAL. {#monsieur-loyal.-13 .loyal}

On pourroit bien punir ces paroles inf&acirc;mes,\
Mamie, et l\'on d&eacute;cr&egrave;te aussi contre les femmes.

#### CL&Eacute;ANTE. {#cléante.-13 .cleante}

Finissons tout cela, Monsieur&nbsp;: c\'en est assez&nbsp;;\
Donnez t&ocirc;t ce papier, de gr&acirc;ce, et nous laissez.

#### MONSIEUR LOYAL. {#monsieur-loyal.-14 .loyal}

Jusqu\'au revoir. Le Ciel vous tienne tous en joie&nbsp;!

#### ORGON. {#orgon.-28 .orgon}

Puisse-t-il te confondre, et celui qui t\'envoie&nbsp;!

-   -   -   -   -   -   

ACTE V.
-------

### SC&Egrave;NE V

\- Orgon, Cl&eacute;ante, Mariane, Elmire, Madame Pernelle, Dorine,
Damis.

#### ORGON. {#orgon.-29 .orgon}

H&eacute; bien, vous le voyez, ma m&egrave;re, si j\'ai droit[^141],\
Et vous pouvez juger du reste par l\'exploit&nbsp;:\
Ses trahisons enfin vous sont-elles connues&nbsp;?

#### MADAME PERNELLE. {#madame-pernelle.-12 .pernelle}

Je suis toute &eacute;baubie, et je tombe des nues&nbsp;!

#### DORINE. {#dorine.-7 .dorine}

Vous vous plaignez &agrave; tort, &agrave; tort vous le bl&acirc;mez,\
Et ses pieux desseins par l&agrave; sont confirm&eacute;s&nbsp;:\
Dans l\'amour du prochain sa vertu se consomme&nbsp;;\
Il sait que tr&egrave;s souvent les biens corrompent l\'homme,\
Et, par charit&eacute; pure, il veut vous enlever\
Tout ce qui vous peut faire obstacle &agrave; vous sauver.

#### ORGON. {#orgon.-30 .orgon}

Taisez-vous, c\'est le mot qu\'il vous faut toujours dire.

#### CL&Eacute;ANTE. {#cléante.-14 .cleante}

Allons voir quel conseil on doit vous faire &eacute;lire[^142].

#### ELMIRE. {#elmire.-2 .elmire}

Allez faire &eacute;clater l\'audace de l\'ingrat.\
Ce proc&eacute;d&eacute; d&eacute;truit la vertu du contrat&nbsp;;\
Et sa d&eacute;loyaut&eacute; va paro&icirc;tre trop noire,\
Pour souffrir qu\'il en ait le succ&egrave;s qu\'on veut croire.

-   -   

ACTE V.
-------

### SC&Egrave;NE VI

\- Val&egrave;re, Orgon, Cl&eacute;ante, Elmire, Mariane, etc.

#### VAL&Egrave;RE. {#valère. .valere}

Avec regret, Monsieur, je viens vous affliger&nbsp;;\
Mais je m\'y vois contraint par le pressant danger.\
Un ami, qui m\'est joint d\'une amiti&eacute; fort tendre,\
Et qui sait l\'int&eacute;r&ecirc;t qu\'en vous j\'ai lieu de prendre,\
A viol&eacute; pour moi, par un pas d&eacute;licat,\
Le secret que l\'on doit aux affaires d\'&Eacute;tat,\
Et me vient d\'envoyer un avis dont la suite\
Vous r&eacute;duit[^143] au parti d\'une soudaine fuite.\
Le fourbe qui longtemps a pu vous imposer\
Depuis une heure au Prince a su vous accuser,\
Et remettre en ses mains, dans les traits qu\'il vous jette,\
D\'un criminel d\'&Eacute;tat l\'importante cassette,\
Dont, au m&eacute;pris, dit-il, du devoir d\'un sujet,\
Vous avez conserv&eacute; le coupable secret.\
J\'ignore le d&eacute;tail du crime qu\'on vous donne&nbsp;;\
Mais un ordre est donn&eacute; contre votre personne&nbsp;;\
Et lui-m&ecirc;me est charg&eacute;, pour mieux l\'ex&eacute;cuter,\
D\'accompagner celui qui vous doit arr&ecirc;ter.

#### CL&Eacute;ANTE. {#cléante.-15 .cleante}

Voil&agrave; ses droits arm&eacute;s[^144]&nbsp;; et c\'est par
o&ugrave; le tra&icirc;tre\
De vos biens qu\'il pr&eacute;tend cherche &agrave; se rendre
ma&icirc;tre.

#### ORGON. {#orgon.-31 .orgon}

L\'homme est, je vous l\'avoue, un m&eacute;chant animal&nbsp;!

#### VAL&Egrave;RE. {#valère.-1 .valere}

Le moindre amusement[^145] vous peut &ecirc;tre fatal.\
J\'ai, pour vous emmener, mon carrosse &agrave; la porte,\
Avec mille louis qu\'ici je vous apporte.\
Ne perdons point de temps&nbsp;: le trait est foudroyant,\
Et ce sont de ces coups que l\'on pare en fuyant.\
&Agrave; vous mettre en lieu s&ucirc;r je m\'offre pour conduite,\
Et veux accompagner jusqu\'au bout votre fuite.

#### ORGON. {#orgon.-32 .orgon}

Las&nbsp;! que ne dois-je point &agrave; vos soins obligeants&nbsp;!\
Pour vous en rendre gr&acirc;ce il faut un autre temps&nbsp;;\
Et je demande au Ciel de m\'&ecirc;tre assez propice,\
Pour reconno&icirc;tre un jour ce g&eacute;n&eacute;reux service.\
Adieu&nbsp;: prenez le soin, vous autres\...

#### CL&Eacute;ANTE. {#cléante.-16 .cleante}

Allez t&ocirc;t&nbsp;:\
Nous songerons, mon fr&egrave;re, &agrave; faire ce qu\'il faut.

-   -   -   

ACTE V.
-------

### SC&Egrave;NE VII (DERNI&Egrave;RE)

\- L\'Exempt, Tartuffe, Val&egrave;re, Orgon, Elmire, Mariane, etc.

#### TARTUFFE. {#tartuffe. .tartuffe}

Tout beau, Monsieur, tout beau, ne courez point si vite&nbsp;:\
Vous n\'irez pas fort loin pour trouver votre g&icirc;te,\
Et de la part du Prince on vous fait prisonnier.

#### ORGON. {#orgon.-33 .orgon}

Tra&icirc;tre, tu me gardois ce trait pour le dernier&nbsp;;\
C\'est le coup, sc&eacute;l&eacute;rat, par o&ugrave; tu
m\'exp&eacute;dies,\
Et voil&agrave; couronner toutes tes perfidies.

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Vos injures n\'ont rien &agrave; me pouvoir aigrir,\
Et je suis pour le Ciel appris &agrave; tout souffrir.

#### CL&Eacute;ANTE. {#cléante.-17 .cleante}

La mod&eacute;ration est grande, je l\'avoue.

#### DAMIS. {#damis.-6 .damis}

Comme du Ciel l\'inf&acirc;me impudemment se joue&nbsp;!

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Tous vos emportements ne sauroient m\'&eacute;mouvoir,\
Et je ne songe &agrave; rien qu\'&agrave; faire mon devoir.

#### MARIANE. {#mariane. .mariane}

Vous avez de ceci grande gloire &agrave; pr&eacute;tendre,\
Et cet emploi pour vous est fort honn&ecirc;te &agrave; prendre.

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

Un emploi ne sauroit &ecirc;tre que glorieux,\
Quand il part du pouvoir qui m\'envoie en ces lieux.

#### ORGON. {#orgon.-34 .orgon}

Mais t\'es-tu souvenu que ma main charitable,\
Ingrat, t\'a retir&eacute; d\'un &eacute;tat mis&eacute;rable&nbsp;?

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

Oui, je sais quels secours j\'en ai pu recevoir&nbsp;;\
Mais l\'int&eacute;r&ecirc;t du Prince est mon premier devoir&nbsp;;\
De ce devoir sacr&eacute; la juste violence\
&Eacute;touffe dans mon coeur toute reconnoissance,\
Et je sacrifierois &agrave; de si puissants noeuds\
Ami, femme, parents, et moi-m&ecirc;me avec eux.

#### ELMIRE. {#elmire.-3 .elmire}

L\'imposteur&nbsp;!

#### DORINE. {#dorine.-8 .dorine}

Comme il sait, de tra&icirc;tresse mani&egrave;re,\
Se faire un beau manteau de tout ce qu\'on r&eacute;v&egrave;re !

#### CL&Eacute;ANTE. {#cléante.-18 .cleante}

Mais s\'il est si parfait que vous le d&eacute;clarez,\
Ce z&egrave;le qui vous pousse et dont vous vous parez,\
D\'o&ugrave; vient que pour paro&icirc;tre il s\'avise d\'attendre\
Qu\'&agrave; poursuivre sa femme il ait su vous surprendre,\
Et que vous ne songez &agrave; l\'aller d&eacute;noncer\
Que lorsque son honneur l\'oblige &agrave; vous chasser&nbsp;?\
Je ne vous parle point, pour devoir en distraire,\
Du don de tout son bien[^146] qu\'il venoit de vous faire&nbsp;;\
Mais le voulant traiter en coupable aujourd\'hui,\
Pourquoi consentiez-vous &agrave; rien prendre de lui&nbsp;?

#### TARTUFFE, *&agrave; l\'Exempt.* {#tartuffe-à-lexempt. .tartuffe}

D&eacute;livrez-moi, Monsieur, de la criaillerie,\
Et daignez accomplir votre ordre, je vous prie.

#### L\'EXEMPT. {#lexempt. .exempt}

Oui, c\'est trop demeurer, sans doute[^147], &agrave; l\'accomplir :\
Votre bouche &agrave; propos m\'invite &agrave; le remplir&nbsp;;\
Et pour l\'ex&eacute;cuter, suivez-moi tout &agrave; l\'heure[^148]\
Dans la prison qu\'on doit vous donner pour demeure.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Qui&nbsp;? moi, Monsieur&nbsp;?

#### L\'EXEMPT. {#lexempt.-1 .exempt}

Oui, vous.

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

Pourquoi donc la prison&nbsp; ?

#### L\'EXEMPT. {#lexempt.-2 .exempt}

Ce n\'est pas vous &agrave; qui j\'en veux rendre raison.\
Remettez-vous, Monsieur, d\'une alarme si chaude[^149].\
Nous vivons sous un prince ennemi de la fraude,\
Un prince dont les yeux se font jour dans les coeurs,\
Et que ne peut tromper tout l\'art des imposteurs.\
D\'un fin discernement sa grande &acirc;me pourvue\
Sur les choses toujours jette une droite vue&nbsp;;\
Chez elle jamais rien ne surprend trop d\'acc&egrave;s,\
Et sa ferme raison ne tombe en nul exc&egrave;s.\
Il donne aux gens de bien une gloire immortelle&nbsp;;\
Mais sans aveuglement il fait briller ce z&egrave;le,\
Et l\'amour pour les vrais[^150] ne ferme point son coeur\
&Agrave; tout ce que les faux doivent donner d\'horreur.\
Celui-ci n\'&eacute;toit pas pour le pouvoir surprendre,\
Et de pi&egrave;ges plus fins on le voit se d&eacute;fendre.\
D\'abord il a perc&eacute;, par ses vives clart&eacute;s,\
Des replis de son coeur toutes les l&acirc;chet&eacute;s.\
Venant vous accuser, il s\'est trahi lui-m&ecirc;me,\
Et par un juste trait de l\'&eacute;quit&eacute; supr&ecirc;me[^151],\
S\'est d&eacute;couvert au Prince un fourbe renomm&eacute;,\
Dont sous un autre nom il &eacute;toit inform&eacute;&nbsp;;\
Et c\'est un long d&eacute;tail d\'actions toutes noires\
Dont on pourroit former des volumes d\'histoires.\
Ce monarque, en un mot, a vers vous d&eacute;test&eacute;\
Sa l&acirc;che ingratitude et sa d&eacute;loyaut&eacute;[^152]&nbsp;;\
&Agrave; ses autres horreurs il a joint cette suite[^153],\
Et ne m\'a jusqu\'ici soumis &agrave; sa conduite\
Que pour voir l\'impudence aller jusques au bout,\
Et vous faire par lui faire raison du tout[^154].\
Oui, de tous vos papiers, dont il se dit le ma&icirc;tre,\
Il veut qu\'entre vos mains je d&eacute;pouille le tra&icirc;tre.\
D\'un souverain pouvoir, il brise les liens\
Du contrat qui lui fait un don de tous vos biens,\
Et vous pardonne enfin cette offense secr&egrave;te\
O&ugrave; vous a d\'un ami fait tomber la retraite&nbsp; ;\
Et c\'est le prix qu\'il donne au z&egrave;le qu\'autrefois\
On vous vit t&eacute;moigner en appuyant ses droits[^155],\
Pour montrer que son coeur sait, quand moins on y pense,\
D\'une bonne action verser la r&eacute;compense,\
Que jamais le m&eacute;rite avec lui ne perd rien,\
Et que mieux que du mal il se souvient du bien.

#### DORINE. {#dorine.-9 .dorine}

Que le Ciel soit lou&eacute;&nbsp;!

#### MADAME PERNELLE. {#madame-pernelle.-13 .pernelle}

Maintenant je respire.

#### ELMIRE. {#elmire.-4 .elmire}

Favorable succ&egrave;s&nbsp;!

#### MARIANE. {#mariane.-1 .mariane}

Qui l\'auroit os&eacute; dire&nbsp;?

#### ORGON, *&agrave; Tartuffe.* {#orgon-à-tartuffe. .orgon}

H&eacute; bien&nbsp;! te voil&agrave;, tra&icirc;tre\...

#### CL&Eacute;ANTE. {#cléante.-19 .cleante}

Ah&nbsp;! mon fr&egrave;re, arr&ecirc;tez,\
Et ne descendez point &agrave; des indignit&eacute;s&nbsp;;\
&Agrave; son mauvais destin laissez un mis&eacute;rable,\
Et ne vous joignez point au remords qui l\'accable&nbsp;:\
Souhaitez bien plut&ocirc;t que son coeur en ce jour\
Au sein de la vertu fasse un heureux retour,\
Qu\'il corrige sa vie en d&eacute;testant son vice\
Et puisse du grand Prince adoucir la justice,\
Tandis qu\'&agrave; sa bont&eacute; vous irez &agrave; genoux\
Rendre ce que demande un traitement si doux.

#### ORGON. {#orgon.-35 .orgon}

Oui, c\'est bien dit&nbsp;: allons &agrave; ses pieds avec joie\
Nous louer des bont&eacute;s que son coeur nous d&eacute;ploie.\
Puis, acquitt&eacute;s un peu de ce premier devoir,\
Aux justes soins d\'un autre il nous faudra pourvoir,\
Et par un doux hymen couronner en Val&egrave;re\
La flamme d\'un amant g&eacute;n&eacute;reux et sinc&egrave;re.

-   -   -   -   -   -   -   -   -   -   

[^1]: Le roi P&eacute;taut, auquel personne n\'ob&eacute;issait, est le
    nom du chef que se donnait, par d&eacute;rision, la corporation des
    mendiants parisiens.

[^2]: Un cagot est un faux d&eacute;vot, un hypocrite&nbsp;; un cagot de
    critique est un hypocrite qui se m&ecirc;le de tout critiquer.

[^3]: Merci de ma vie est un &laquo;&nbsp;serment du petit
    peuple&nbsp;&raquo; (Dictionnaire de Fureti&egrave;re, 1690)&nbsp;:
    Que Dieu ait piti&eacute; de ma vie.

[^4]: *Rebuter* quelqu\'un&nbsp;: rejeter ses conseils.

[^5]: *Les brillants*&nbsp;: l\'&eacute;clat, la beaut&eacute;.

[^6]: VAR. Qui ne saurait souffrir qu\'un autre ait les plaisirs.(1682).
    Un autre, au sens g&eacute;n&eacute;ral d\'une autre, est
    fr&eacute;quent au XVIIe&nbsp;si&egrave;cle.

[^7]: *Les contes bleus*&nbsp;: des adaptations populaires des romans de
    chevalerie formaient une &laquo;&nbsp;Biblioth&egrave;que
    bleue&nbsp;&raquo;, ainsi appel&eacute;e &agrave; cause de la
    couleur du papier utilis&eacute;e pour la couverture des volumes.

[^8]: *Madame*&nbsp;: c\'est Dorine qui est ici ironiquement
    d&eacute;sign&eacute;e.

[^9]: *Au besoin*&nbsp;: parce qu\'il en &eacute;tait besoin.

[^10]: :&laquo;&nbsp;On dit qu\'un homme en aura tout le long de l\'aune
    pour dire qu\'on lui fera tout le mal qu\'on pourra&nbsp;&raquo;
    (Dictionnaire de Fureti&egrave;re, 1690).

[^11]: *Pour c&eacute;ans j\'en rabats de moiti&eacute;*&nbsp;:
    l\'estime que j\'avais pour cette maison diminue fortement.

[^12]: *Jour de Dieu*&nbsp;: &laquo;&nbsp;Sorte de serment burlesque, et
    qui ne se fait que par les femmes&nbsp;&raquo; (Dictionnaire de
    Richelet, 1679).

[^13]: Gaupe&nbsp;: &laquo;&nbsp;Terme d\'injure et de m&eacute;pris.
    Femme malpropre et d&eacute;sagr&eacute;able&nbsp;&raquo;
    (Littr&eacute;).

[^14]: *Cette bonne femme*&nbsp;: cette vieille femme (f&eacute;minin de
    bonhomme, qui signifie alors vieil homme).

[^15]: *&Ecirc;tre coiff&eacute; de quelqu\'un*, c\'est &ecirc;tre
    entich&eacute; de quelqu\'un, ne jurer que par lui.

[^16]: *Nos troubles*&nbsp;: les troubles de la Fronde (1648-1652),
    durant lesquels Orgon n\'a suivi ni le parti du Parlement, ni celui
    des Princes, mais est rest&eacute; fid&egrave;le &agrave; la Cour.
    Il a ainsi acquis la r&eacute;putation d\'homme sage aupr&egrave;s
    du Roi, qui le r&eacute;compensera au d&eacute;nouement.

[^17]: *Le directeur*&nbsp;: Tartuffe est bien le directeur de
    conscience d\'Orgon. Certains la&iuml;cs, au
    XVIIe&nbsp;si&egrave;cle, prenaient ainsi la responsabilit&eacute;
    de diriger les &acirc;mes: ainsi, Jean de Berni&egrave;res-Louvigny
    ou le baron Gaston de Renty.

[^18]: *Au plus haut bout*&nbsp;: &agrave; l\'endroit le plus honorable.

[^19]: VAR. Les bons morceaux de tout, il faut qu\'on les lui
    c&egrave;de. (1682).

[^20]: Les vers 191 &agrave; 194 &eacute;taient saut&eacute;s &agrave;
    la repr&eacute;sentation.

[^21]: *Par cent&nbsp;dehors fard&eacute;s a l\'art de
    l\'&eacute;blouir*&nbsp;: a l\'art de le tromper par
    cent&nbsp;apparences fallacieuses.

[^22]: *Cagotisme*&nbsp;: mot forg&eacute; sur cagot, faux d&eacute;vot,
    hypocrite.

[^23]: *Gloser*&nbsp;: critiquer.

[^24]: *Fat*&nbsp;: sot, niais&nbsp;; gar&ccedil;on&nbsp;: valet
    &agrave; tout faire.

[^25]: *Fleur des Saints*&nbsp;: probablement un des deux gros volumes
    de l\'ouvrage du j&eacute;suite espagnol Ribadeneira, *Les Fleurs
    des Saints et les f&ecirc;tes de toute l\'ann&eacute;e*, qui avait
    &eacute;t&eacute; traduit en fran&ccedil;ais.

[^26]: *Pour moins d\'amusement*&nbsp;: pour perdre moins de temps.

[^27]: *L\'hymen*&nbsp;: le mariage.

[^28]: *&Agrave; son effet*&nbsp;: &agrave; sa
    c&eacute;l&eacute;bration.

[^29]: On renvoie souvent, &agrave; propos de cette exclamation
    r&eacute;p&eacute;t&eacute;e, &agrave; Tallement des R&eacute;aux
    (Historiettes, &eacute;d. A. Adam, Pl&eacute;iade, t. I,
    p.&nbsp;295)&nbsp;: le gardien d\'un couvent de capucins, &agrave;
    qui on donnait d\'excellentes nouvelles du P&egrave;re Joseph,
    l\'&eacute;minence grise de Richelieu, ne cessait de dire, avec une
    admiration attendrie&nbsp;: &laquo;&nbsp;Le pauvre
    homme&nbsp;!&nbsp;&raquo;. Moli&egrave;re a-t-il eu connaissance de
    l\'anecdote&nbsp;?

[^30]: *Vos ravissements*&nbsp;: Orgon emploie l&agrave; abusivement un
    mot du vocabulaire mystique.

[^31]: On lit dans l\'Imitation de J&eacute;sus-Christ, I, 3&nbsp;:
    *Vere prudens est qui omnia terrena arbitratur ut stercora*, ce que
    Corneille traduit&nbsp;: &laquo;&nbsp;Vraiment sage est celui qui
    prend pour du fumier les choses de la terre.&nbsp;&raquo; Mais Orgon
    a rench&eacute;ri sur l\'Imitation dans les vers suivants, car ce ne
    sont pas seulement les biens terrestres qu\'il m&eacute;prise, mais
    aussi les affections familiales les plus l&eacute;gitimes.

[^32]: Cette expression &eacute;quivaut &agrave;&nbsp;: &laquo;&nbsp;Et
    que pr&eacute;tendez-vous que toutes ces niaiseries
    prouvent&nbsp;?&nbsp;&raquo;.

[^33]: *Le libertinage*&nbsp;: non pas la libre-pens&eacute;e
    caract&eacute;ris&eacute;e, mais le manque de respect pour tout ce
    qui touche aux choses religieuses.

[^34]: *Entich&eacute;*&nbsp;: &laquo;&nbsp;g&acirc;t&eacute; par
    quelque chose de faux ou de moralement mauvais&nbsp;&raquo;
    (Littr&eacute;).

[^35]: Sans doute: sans aucun doute, assur&eacute;ment.

[^36]: Caton l\'Ancien passait pour l\'auteur de distiques moraux
    souvent r&eacute;imprim&eacute;s aux XVIe&nbsp;et
    XVIIe&nbsp;si&egrave;cles, et qui sont en r&eacute;alit&eacute;
    l\'oeuvre de Dionysius Cato, qui v&eacute;cut au
    Ier&nbsp;si&egrave;cle de notre &egrave;re.

[^37]: *Ces d&eacute;vots de place*&nbsp;: ces d&eacute;vots qui font
    profession d\'&ecirc;tre d&eacute;vots sur la place publique, comme
    les domestiques qui attendaient sur la place publique qu\'on les
    engage, et qu\'on appelait &laquo;&nbsp;valets de
    place&nbsp;&raquo;.

[^38]: *Demandent chaque jour*&nbsp;: ont chaque jour une requ&ecirc;te
    &agrave; pr&eacute;senter en faveur de tel ou tel de leurs
    prot&eacute;g&eacute;s.

[^39]: *Fier*&nbsp;: brutal, f&eacute;roce.

[^40]: *Appui*, au figur&eacute;, signifie &laquo;&nbsp;faveur,
    cr&eacute;dit&nbsp;&raquo; (Dictionnaire de Fureti&egrave;re, 1690).
    L\'apparence du mal a chez eux peu de cr&eacute;dit, et elle ne
    suffit pas &agrave; les persuader.

[^41]: *&Eacute;bloui*&nbsp;: abus&eacute;, tromp&eacute;.

[^42]: *H&eacute;las*&nbsp;: il arrive que cette interjection ne marque
    pas le regret ni la douleur. (Cf. Les Femmes savantes, IV, 5, v.
    1447&nbsp;: &laquo;&nbsp;H&eacute;las&nbsp;! dans cette humeur
    conservez-le toujours&nbsp;!&nbsp;&raquo;).

[^43]: *Votre\'hymen*&nbsp;: votre mariage.

[^44]: Selon l\'&eacute;dition de&nbsp;1734, c\'est juste apr&egrave;s
    le vers&nbsp;440 prononc&eacute; par Mariane, que Dorine est
    entr&eacute;e doucement et s\'est mise derri&egrave;re Orgon sans
    &ecirc;tre vue de lui.

[^45]: *Sans doute*&nbsp;: sans aucun doute.

[^46]: *Il est bien gentilhomme*&nbsp;: &ecirc;tre gentilhomme, c\'est
    jouir de cette condition par la naissance, et non pas par
    l\'exercice d\'une charge ou par la gr&acirc;ce du roi.

[^47]: *Un peu libertin*&nbsp;: il ne s\'agit pas ici de
    libre-pens&eacute;e caract&eacute;ris&eacute;e, mais de manque de
    respect pour tout ce qui touche &agrave; la religion.

[^48]: VAR. Et sera tout confit en douceurs, et plaisirs. (1682).

[^49]: *Un sot*&nbsp;: un mari tromp&eacute;.

[^50]: *Son ascendant*&nbsp;: l\'influence que les astres exercent sur
    lui (cf.&nbsp;L\'Ecole des maris, v. 1099).

[^51]: *C\'est une conscience*&nbsp;: c\'est une affaire de conscience.
    C\'est-&agrave;-dire&nbsp;: &laquo;&nbsp;C\'est un devoir de ne pas
    vous laisser conclure une telle alliance.&nbsp;&raquo;

[^52]: *&Eacute;lire*&nbsp;: choisir.

[^53]: *Quelque sotte*&nbsp;: seule une sotte ferait cela.

[^54]: *Payer d\'ob&eacute;issance*&nbsp;: faire preuve
    d\'ob&eacute;issance.

[^55]: *Je me moquerais fort*&nbsp;: je me garderais bien, comme d\'une
    chose ridicule, de prendre un tel &eacute;poux.

[^56]: *Me rasseoir*&nbsp;: me remettre, me calmer.

[^57]: *A fait pour vous des pas*&nbsp;: vous a demand&eacute; en
    mariage.

[^58]: *Dans l\'occasion*&nbsp;: au combat, dans la bataille. Le sens
    militaire de l\'expression est ici confirm&eacute; par l\'emploi de
    mollir (manquer de courage, faillir, fl&eacute;chir).

[^59]: *Bourru*&nbsp;: &laquo;&nbsp;fantasque, bizarre,
    extragant&nbsp;&raquo; (Dictionnaire de l\'Acad&eacute;mie, 1694).

[^60]: *&Ecirc;tre coiff&eacute;* de quelqu\'un, c\'est &ecirc;tre
    entich&eacute; de quelqu\'un, ne jurer que par lui.

[^61]: &laquo;&nbsp;On dit d\'un homme habile et difficile &agrave;
    surprendre qu\'il ne se mouche pas du pied&nbsp;&raquo;
    (Dictionnaire de Fureti&egrave;re, 1690).

[^62]: *Heur*, pour bonheur, commence &agrave; &ecirc;tre un
    archa&iuml;sme apr&egrave;s 1660.

[^63]: *Chez lui*&nbsp;: c\'est l&agrave; une restriction significative
    (cf. ce que dit Orgon aux vers 493-494).

[^64]: *Avec un tel mari*&nbsp;: Tartuffe est un sanguin, selon la
    th&eacute;orie des humeurs\*, et les sanguins passaient pour
    particuli&egrave;rement dou&eacute;s pour l\'amour.

[^65]: Le *bailli* a des attributions judiciaires dans une petite
    ville&nbsp;; quant &agrave; l\'&eacute;lu, il est charg&eacute; de
    trancher en premi&egrave;re instance les contestations relatives
    &agrave; la r&eacute;partition de certains impots.

[^66]: *D\'un si&egrave;ge pliant*&nbsp;: et non d\'une chaise ou d\'un
    fauteuil&nbsp;; Mariane sera a peine re&ccedil;ue dans cette
    soci&eacute;t&eacute; provinciale.

[^67]: *La grand\'bande*&nbsp;: on appelait ainsi les vingt-quatre
    violons de la chambre du Roi.

[^68]: *Fagotin*&nbsp;: c\'&eacute;tait le nom du singe de
    Brioch&eacute;, c&eacute;l&egrave;bre montreur de marionnettes au
    milieu du XVIIe&nbsp;si&egrave;cle.

[^69]: *Si mes voeux d&eacute;clar&eacute;s&mldr;*&nbsp;: latinisme qui
    signifie: si le fait de d&eacute;clarer l\'amour que je porte
    &agrave; Val&egrave;re pouvait conjurer le sort&mldr;

[^70]: *Sans doute*&nbsp;: sans aucun doute.

[^71]: *R&eacute;ussir*&nbsp;: arriver.

[^72]: *Sans doute*&nbsp;: sans aucun doute.

[^73]: *Engage notre gloire*&nbsp;: met en cause notre fiert&eacute;.

[^74]: *Sans doute*&nbsp;: sans aucun doute. Assur&eacute;ment.

[^75]: *Diantre soit fait de vous si je le veux&nbsp;!*&nbsp;: le diable
    vous emporte si je consens &agrave; vous laisser partir.

[^76]: *Vous payerez de quelque maladie*&nbsp;: vous pr&eacute;texterez
    quelque maladie.

[^77]: *Que vous disiez &laquo;&nbsp;oui&nbsp;&raquo;*&nbsp;: si vous ne
    dites pas &laquo;&nbsp;oui&nbsp;&raquo;.

[^78]: V. 813-814&nbsp;: Nous allons r&eacute;veiller les efforts de son
    fr&egrave;re (nous dirions aujourd\'hui *beau-fr&egrave;re*)
    Cl&eacute;ante, et jeter Elmire, la belle-m&egrave;re de Mariane,
    dans notre parti. Ces deux vers s\'adressent &agrave; Mariane, comme
    le souligne l\'&eacute;dition de&nbsp;1734.

[^79]: *Tirez*&nbsp;: allez, partez.

[^80]: *S\'il faut qu\'&agrave; ce dessein il pr&ecirc;te quelque
    espoir*&nbsp;: si par malheur il pousse &agrave; la
    r&eacute;alisation de ce projet d\'Orgon.

[^81]: VAR. Mais &agrave; convoiter, moi, je ne suis pas si prompte.
    (1682).

[^82]: *H&eacute;las*&nbsp;: cette interjection ne marque pas ici le
    regret ou la douleur, mais l\'attendrissement. (Cf. Les Femmes
    savantes, IV, 5, v. 1447: &laquo;&nbsp;H&eacute;las&nbsp;! dans
    cette humeur conservez-le toujours&nbsp;!&nbsp;&raquo;).

[^83]: *Ne nous &eacute;claire*&nbsp;: ne nous &eacute;pie, ne nous
    observe.

[^84]: *Sans doute*&nbsp;: sans aucun doute, assur&eacute;ment.

[^85]: VAR. Et vous faire serment, que les bruits que je fais. (1682).

[^86]: VAR. De vous faire aucun mal je n\'eus jamais dessein. (1682).

[^87]: *Ses attraits r&eacute;fl&eacute;chis*&nbsp;: un reflet de ses
    attraits, de ses splendeurs.

[^88]: *Amour* est souvent encore f&eacute;minin au
    XVIIe&nbsp;si&egrave;cle.

[^89]: *Au plus beau des portraits*&nbsp;: devant le plus beau des
    portraits.

[^90]: *Le noir esprit*&nbsp;: le diable.

[^91]: *Adroite* se pronon&ccedil;ait *adr&egrave;te* (Vaugelas nous
    indique, dans ses *Remarques*, que droit se pronon&ccedil;ait dret).

[^92]: *Vous deviez*&nbsp;: vous auriez d&ucirc;.

[^93]: Parodie du fameux vers de Corneille&nbsp;: &laquo;&nbsp;Ah&nbsp;!
    pour &ecirc;tre Romain, je n\'en suis pas moins
    homme&nbsp;!&nbsp;&raquo; (Sertorius, IV, 1, v. 1194).

[^94]: *De mon int&eacute;rieur*&nbsp;: l\'int&eacute;rieur est, dans la
    langue de la spiritualit&eacute;, &laquo;&nbsp;la partie intime de
    l\'&acirc;me&nbsp;&raquo; (Littr&eacute;).

[^95]: Cf. Mathurin R&eacute;gnier, *Satire XIII*, v. 121-124, o&ugrave;
    il est dit des moines&nbsp;: &laquo;&nbsp;Outre le saint voeu qui
    sert de couverture,/ Ils sont trop oblig&eacute;s au secret de
    nature/ Et savent, plus discrets, apporter en aimant/ Avecque moins
    d\'&eacute;clat, plus de contentement.&nbsp;&raquo;

[^96]: *Raillerie*&nbsp;: chose d&eacute;raisonnable.

[^97]: *Il faut que je me croie*&nbsp;: il faut que je suive mon
    sentiment, que je fasse ce que j\'ai envie de faire (Cf. Le
    D&eacute;pit amoureux, v. 927).

[^98]: *Vider d\'affaire* (ou d\'affaires)&nbsp;: &laquo;&nbsp;On dit
    vider d\'affaires pour dire travailler &agrave; en sortir
    promptement, &agrave; les terminer&nbsp;&raquo; (Dictionnaire de
    l\'Acad&eacute;mie, 1694).

[^99]: *Laissez-le en paix*&nbsp;: le *e* muet du pronom *le*
    s\'&eacute;lide devant la voyelle du mot suivant.

[^100]: D\'apr&egrave;s un petit livre publi&eacute; en&nbsp;1730
    (*Lettre &agrave; Mylord \*\*\* sur Baron* et la *Demoiselle Le
    Couvreur&mldr;* par George Wink) et les &eacute;diteurs
    de&nbsp;1734, Tartuffe disait primitivement&nbsp;:
    &laquo;&nbsp;&Ocirc; Ciel, pardonne-lui comme je lui
    pardonne&nbsp;!&nbsp;&raquo;, ou, comme l\'indique Voltaire dans son
    Sommaire de Tartuffe&nbsp;: &laquo;&nbsp;&Ocirc; Ciel, pardonne-moi
    comme je lui pardonne&nbsp;!&nbsp;&raquo;

[^101]: *De ma foi*&nbsp;: de ma fid&eacute;lit&eacute; &agrave; votre
    &eacute;gard.

[^102]: *Sans doute*&nbsp;: sans aucun doute, assur&eacute;ment.

[^103]: *Color&eacute;es*&nbsp;: propres &agrave; tromper.

[^104]: *Tir&eacute;es*&nbsp;: forc&eacute;es, artificieuses.

[^105]: *Qui montre &agrave;*&nbsp;: qui enseigne &agrave;&mldr;

[^106]: *Des droits de la naissance*&nbsp;: des droits que ma naissance
    vous a donn&eacute;s sur moi.

[^107]: *Dispensez mes voeux de cette ob&eacute;issance*&nbsp;:
    dispensez-moi de cet acte d\'ob&eacute;issance malgr&eacute; les
    voeux que j\'ai faits de vous ob&eacute;ir.

[^108]: *Tout le mien*&nbsp;: tout le bien dont Mariane a
    h&eacute;rit&eacute; de sa m&egrave;re, la premi&egrave;re femme
    d\'Orgon.

[^109]: *Parlez &agrave; votre &eacute;cot*&nbsp;: &laquo;&nbsp;Se dit
    &agrave; une personne se m&ecirc;lant de parler &agrave; des gens
    qui ne lui adressent pas la parole&nbsp;&raquo; (Littr&eacute;).

[^110]: *Je vous admire*&nbsp;: je vous regarde avec &eacute;tonnement.

[^111]: *&Ecirc;tre coiff&eacute; de quelqu\'un*, c\'est &ecirc;tre
    entich&eacute; de quelqu\'un, ne jurer que par lui.

[^112]: *D&eacute;visager*&nbsp;: d&eacute;chirer le visage.

[^113]: *Sans aller plus loin*&nbsp;: sans plus tarder.

[^114]: *Il* (au neutre)&nbsp;: cela.

[^115]: VAR. De mon trouble, il est vrai, j\'&eacute;tais si
    poss&eacute;d&eacute;e. (1682).

[^116]: VAR. Et les choses en sont en plus de s&ucirc;ret&eacute;.
    (1682).

[^117]: *Que l\'int&eacute;r&ecirc;t*&nbsp;: sinon
    l\'int&eacute;r&ecirc;t.

[^118]: *Sans doute*&nbsp;: sans aucun doute, assur&eacute;ment.

[^119]: 0*Et l\'on ne peut aller&mldr;*&nbsp;: et l\'on ne peut arriver
    &agrave; vous satisfaire si l\'on ne pousse les choses jusqu\'aux
    derni&egrave;res faveurs.

[^120]: *On soup&ccedil;onne ais&eacute;ment un sort*&nbsp;: on se
    d&eacute;fie ais&eacute;ment d\'un sort&mldr;

[^121]: Ces vers 1459-1464 sont repris, &agrave; quelques modifications
    pr&egrave;s, dans *Dom Garcie de Navarre*, v. 654-659.

[^122]: *Se parer*&nbsp;: se garder, se prot&eacute;ger.

[^123]: C\'est la fameuse *direction d\'intention*, que Pascal a
    reproch&eacute;e aux casuistes j&eacute;suites dans sa
    VIIe&nbsp;Provinciale.

[^124]: *Sans doute*&nbsp;: sans aucun doute, assur&eacute;ment.

[^125]: VAR. Et ne vous h&acirc;tez pas, de peur de vous
    m&eacute;prendre. (1682).

[^126]: VAR. Ah&nbsp;! ah&nbsp;! l\'homme de bien, vous m\'en vouliez
    donner&nbsp;! (1682). En donner &agrave; quelqu\'un: le tromper.

[^127]: *&Eacute;lire*&nbsp;: choisir.

[^128]: *Cette confidence*&nbsp;: le fait d\'avoir conserv&eacute; cette
    cassette qui vous avait &eacute;t&eacute; confi&eacute;e.

[^129]: Var. Quoi&nbsp;? sur un beau semblant&mldr; (1682).

[^130]: *On ne doit point gauchir*&nbsp;: on ne doit pas y aller par
    quatre chemins.

[^131]: *Myst&egrave;res*&nbsp;: secrets, r&eacute;v&eacute;lations.

[^132]: VAR. Aux menaces du fourbe, on ne doit dormir point. (1682).

[^133]: *Cette instance*&nbsp;: ce proc&egrave;s, cette poursuite.

[^134]: *&Agrave; l\'orgueil de ce tra&icirc;tre*&nbsp;: devant
    l\'orgueil de ce tra&icirc;tre.

[^135]: La verge servait aux huissiers &mdash; mais M. Loyal est-il
    huissier ou simplement sergent, comme l&rsquo;indique la liste des
    personnages&nbsp;? &mdash; &agrave; toucher celui auquel il venait
    signifier un exploit.

[^136]: *Je l\'admire*&nbsp;: j&rsquo;en suis stup&eacute;fait.

[^137]: *L\'office*&nbsp;: le r&ocirc;le, la conduite.

[^138]: *Votre noir jupon*&nbsp;: le jupon est, selon le dictionnaire de
    Fureti&egrave;re (1690), un &laquo;&nbsp;grand
    pourpoint&nbsp;&raquo; ou &laquo;&nbsp;un petit juste-au-corps
    \[&mldr;\] qui ne serre point le corps et qui est une esp&egrave;ce
    de veste propre pour l\'&eacute;t&eacute;.&nbsp;&raquo;

[^139]: Le long passage qui va du v.1773 au v.1800 &eacute;tait
    saut&eacute; &agrave; la repr&eacute;sentation.

[^140]: VAR. Cette audace est trop forte,/ J\'ai peine &agrave; me
    tenir, il vaut mieux que je sorte (1682).\
    Selon l\'&eacute;dition de&nbsp;1734 (t. Ier, p.&nbsp;V et VI),
    &laquo;&nbsp;les com&eacute;diens avaient fait ce changement parce
    que souvent ils &eacute;taient dans la n&eacute;cessit&eacute; de
    faire jouer deux personnages &agrave; un m&ecirc;me acteur, et
    qu\'en faisant ainsi sortir Damis du th&eacute;&acirc;tre, il
    pouvait, en changeant d\'habits, faire le r&ocirc;le de l\'Exempt,
    qui vient avec Tartuffe &agrave; la fin de l\'acte.&nbsp;&raquo;

[^141]: *Si j\'ai droit*&nbsp;: si j\'ai sujet de me plaindre.

[^142]: *Quel conseil on doit vous faire &eacute;lire*&nbsp;: quel parti
    on doit vous faire choisir. Les vers&nbsp;1815 &agrave;&nbsp;1822
    &eacute;taient saut&eacute;s &agrave; la repr&eacute;sentation.

[^143]: *Dont la suite vous r&eacute;duit*&nbsp;: dont la
    cons&eacute;quence vous contraint&mldr;

[^144]: *Voil&agrave; ses droits arm&eacute;s*&nbsp;: le voil&agrave;
    maintenant muni d\'armes qui lui permettront d\'abuser de ses
    droits.

[^145]: *Amusement*&nbsp;: d&eacute;lai, atermoiement.

[^146]: *Je ne vous parle point&mldr;*&nbsp;: je ne vous parle point du
    don de tout son bien&mldr;, ce qui aurait d&ucirc; vous
    d&eacute;tourner de cette d&eacute;nonciation.

[^147]: *Sans doute*&nbsp;: sans aucun doute, assur&eacute;ment.

[^148]: *Tout &agrave; l\'heure*&nbsp;: imm&eacute;diatement.

[^149]: Bien entendu, L\'Exempt s\'adresse &agrave; Orgon &agrave;
    partir du vers&nbsp;1905.

[^150]: *Pour les vrais*&nbsp;: pour les v&eacute;ritables gens de bien.

[^151]: *Et par un juste trait de l\'&eacute;quit&eacute;
    supr&ecirc;me*&nbsp;: par un effet de la justice divine.

[^152]: *A vers vous d&eacute;test&eacute; sa l&acirc;che ingratitude et
    sa d&eacute;loyaut&eacute;*&nbsp;: a eu en horreur l\'ingratitude et
    la d&eacute;loyaut&eacute; qu\'il a manifest&eacute;es envers vous.

[^153]: *Cette suite*&nbsp;: cette derni&egrave;re horreur.

[^154]: Les vers&nbsp;1909-1916, 1919-1926, et&nbsp;1929-1932
    &eacute;taient saut&eacute;s &agrave; la repr&eacute;sentation.

[^155]: On se souvient qu\'Orgon, durant la Fronde, a soutenu, contre le
    Parlement et les Princes, les droits de la Couronne.
