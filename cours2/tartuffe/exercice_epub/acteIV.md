## ACTE IV.

### SCÈNE I

\- Cléante, Tartuffe.

#### CLÉANTE. {#cléante. .cleante}

Oui, tout le monde en parle, et vous m\'en pouvez croire,\
L\'éclat que fait ce bruit n\'est point à votre gloire ;\
Et je vous ai trouvé, Monsieur, fort à propos,\
Pour vous en dire net ma pensée en deux mots.\
Je n\'examine point à fond ce qu\'on expose ;\
Je passe là-dessus, et prends au pis la chose.\
Supposons que Damis n\'en ait pas bien usé,\
Et que ce soit à tort qu\'on vous ait accusé ;\
N\'est-il pas d\'un chrétien de pardonner l\'offense,\
Et d\'éteindre en son coeur tout désir de vengeance ?\
Et devez-vous souffrir, pour votre démêlé,\
Que du logis d\'un père un fils soit exilé ?\
Je vous le dis encore, et parle avec franchise,\
Il n\'est petit ni grand qui ne s\'en scandalise ;\
Et si vous m\'en croyez, vous pacifierez tout,\
Et ne pousserez point les affaires à bout.\
Sacrifiez à Dieu toute votre colère,\
Et remettez le fils en grâce avec le père.

#### TARTUFFE. {#tartuffe. .tartuffe}

Hélas ! je le voudrois, quant à moi, de bon coeur :\
Je ne garde pour lui, Monsieur, aucune aigreur ;\
Je lui pardonne tout, de rien je ne le blâme,\
Et voudrois le servir du meilleur de mon âme ;\
Mais l\'intérêt du Ciel n\'y sauroit consentir,\
Et s\'il rentre céans, c\'est à moi d\'en sortir.\
Après son action, qui n\'eut jamais d\'égale,\
Le commerce entre nous porteroit du scandale :\
Dieu sait ce que d\'abord tout le monde en croiroit !\
À pure politique on me l\'imputeroit ;\
Et l\'on diroit partout que, me sentant coupable,\
Je feins pour qui m\'accuse un zèle charitable,\
Que mon coeur l\'appréhende et veut le ménager,\
Pour le pouvoir sous main au silence engager.

#### CLÉANTE. {#cléante.-1 .cleante}

Vous nous payez ici d\'excuses colorées[^103],\
Et toutes vos raisons, Monsieur, sont trop tirées[^104].\
Des intérêts du Ciel pourquoi vous chargez-vous ?\
Pour punir le coupable a-t-il besoin de vous ?\
Laissez-lui, laissez-lui le soin de ses vengeances ;\
Ne songez qu\'au pardon qu\'il prescrit des offenses ;\
Et ne regardez point aux jugements humains,\
Quand vous suivez du Ciel les ordres souverains.\
Quoi ? le foible intérêt de ce qu\'on pourra croire\
D\'une bonne action empêchera la gloire ?\
Non, non : faisons toujours ce que le Ciel prescrit,\
Et d\'aucun autre soin ne nous brouillons l\'esprit.

#### TARTUFFE. {#tartuffe.-1 .tartuffe}

Je vous ai déjà dit que mon coeur lui pardonne,\
Et c\'est faire, Monsieur, ce que le Ciel ordonne ;\
Mais après le scandale et l\'affront d\'aujourd\'hui,\
Le Ciel n\'ordonne pas que je vive avec lui.

#### CLÉANTE. {#cléante.-2 .cleante}

Et vous ordonne-t-il, Monsieur, d\'ouvrir l\'oreille\
À ce qu\'un pur caprice à son père conseille,\
Et d\'accepter le don qui vous est fait d\'un bien\
Où le droit vous oblige à ne prétendre rien ?

#### TARTUFFE. {#tartuffe.-2 .tartuffe}

Ceux qui me connoîtront n\'auront pas la pensée\
Que ce soit un effet d\'une âme intéressée.\
Tous les biens de ce monde ont pour moi peu d\'appas,\
De leur éclat trompeur je ne m\'éblouis pas ;\
Et si je me résous à recevoir du père\
Cette donation qu\'il a voulu me faire,\
Ce n\'est, à dire vrai, que parce que je crains\
Que tout ce bien ne tombe en de méchantes mains,\
Qu\'il ne trouve des gens qui, l\'ayant en partage,\
En fassent dans le monde un criminel usage,\
Et ne s\'en servent pas, ainsi que j\'ai dessein,\
Pour la gloire du Ciel et le bien du prochain.

#### CLÉANTE. {#cléante.-3 .cleante}

Hé, Monsieur, n\'ayez point ces délicates craintes,\
Qui d\'un juste héritier peuvent causer les plaintes ;\
Souffrez, sans vous vouloir embarrasser de rien,\
Qu\'il soit à ses périls possesseur de son bien ;\
Et songez qu\'il vaut mieux encor qu\'il en mésuse,\
Que si de l\'en frustrer il faut qu\'on vous accuse.\
J\'admire seulement que sans confusion\
Vous en ayez souffert la proposition ;\
Car enfin le vrai zèle a-t-il quelque maxime\
Qui montre à[^105] dépouiller l\'héritier légitime ?\
Et s\'il faut que le Ciel dans votre coeur ait mis\
Un invincible obstacle à vivre avec Damis,\
Ne vaudroit-il pas mieux qu\'en personne discrète\
Vous fissiez de céans une honnête retraite,\
Que de souffrir ainsi, contre toute raison,\
Qu\'on en chasse pour vous le fils de la maison ?\
Croyez-moi, c\'est donner de votre prud\'homie,\
Monsieur\...

#### TARTUFFE. {#tartuffe.-3 .tartuffe}

Il est, Monsieur, trois heures et demie  :\
Certain devoir pieux me demande là-haut,\
Et vous m\'excuserez de vous quitter sitôt.

#### CLÉANTE. {#cléante.-4 .cleante}

Ah !

-   [^103]:*Colorées* : propres à tromper.
-   [^104]:*Tirées* : forcées, artificieuses.
-   [^105]:*Qui montre à* : qui enseigne à...

## ACTE IV.

### SCÈNE II

\- Elmire, Mariane, Dorine, Cléante.

#### DORINE. {#dorine. .dorine}

De grâce, avec nous employez-vous pour elle,\
Monsieur : son âme souffre une douleur mortelle  ;\
Et l\'accord que son père a conclu pour ce soir[^106]\
La fait, à tout moment, entrer en désespoir.\
Il va venir. Joignons nos efforts, je vous prie,\
Et tâchons d\'ébranler, de force ou d\'industrie,\
Ce malheureux dessein qui nous a tous troublés.

-   [^106]*Et l\'accord que son père a conclu pour ce soir* :
    accord est ici synonyme, non de contrat de mariage, car le contrat
    est déjà tout rédigé et Orgon le rapporte de chez son notaire, mais
    de mariage même.

## ACTE IV.

### SCÈNE III

\- Orgon, Elmire, Mariane, Cléante, Dorine.

#### ORGON. {#orgon. .orgon}

Ha ! je me réjouis de vous voir assemblés :\
*(À Mariane.)*\
Je porte en ce contrat de quoi vous faire rire,\
Et vous savez déjà ce que cela veut dire.

#### MARIANE, *à genoux.* {#mariane-à-genoux. .mariane}

Mon père, au nom du Ciel, qui connoît ma douleur,\
Et par tout ce qui peut émouvoir votre coeur,\
Relâchez-vous un peu des droits de la naissance[^107],\
Et dispensez mes voeux de cette obéissance[^108] ;\
Ne me réduisez point par cette dure loi\
Jusqu\'à me plaindre au Ciel de ce que je vous doi,\
Et cette vie, hélas  ! que vous m\'avez donnée,\
Ne me la rendez pas, mon père, infortunée.\
Si, contre un doux espoir que j\'avois pu former,\
Vous me défendez d\'être à ce que j\'ose aimer,\
Au moins, par vos bontés, qu\'à vos genoux j\'implore,\
Sauvez-moi du tourment d\'être à ce que j\'abhorre,\
Et ne me portez point a quelque désespoir,\
En vous servant sur moi de tout votre pouvoir.

#### ORGON, *se sentant attendrir.* {#orgon-se-sentant-attendrir. .orgon}

Allons, ferme, mon coeur, point de foiblesse humaine.

#### MARIANE. {#mariane. .mariane}

Vos tendresses pour lui ne me font point de peine ;\
Faites-les éclater, donnez-lui votre bien,\
Et, si ce n\'est assez, joignez-y tout le mien[^109] :\
J\'y consens de bon coeur, et je vous l\'abandonne ;\
Mais au moins n\'allez pas jusques à ma personne,\
Et souffrez qu\'un couvent dans les austérités\
Use les tristes jours que le Ciel m\'a comptés.

#### ORGON. {#orgon.-1 .orgon}

Ah ! voilà justement de mes religieuses,\
Lorsqu\'un père combat leurs flammes amoureuses  !\
Debout ! Plus votre coeur répugne à l\'accepter,\
Plus ce sera pour vous matière à mériter :\
Mortifiez vos sens avec ce mariage,\
Et ne me rompez pas la tête davantage.

#### DORINE. {#dorine.-1 .dorine}

Mais quoi\... ?

#### ORGON. {#orgon.-2 .orgon}

Taisez-vous, vous ; parlez à votre écot[^110] :\
Je vous défends tout net d\'oser dire un seul mot.

#### CLÉANTE. {#cléante.-5 .cleante}

Si par quelque conseil vous souffrez qu\'on réponde\...

#### ORGON. {#orgon.-3 .orgon}

Mon frère, vos conseils sont les meilleurs du monde,\
Ils sont bien raisonnés, et j\'en fais un grand cas ;\
Mais vous trouverez bon que je n\'en use pas.

#### ELMIRE, *à son mari.* {#elmire-à-son-mari. .elmire}

À voir ce que je vois, je ne sais plus que dire,\
Et votre aveuglement fait que je vous admire[^111] :\
C\'est être bien coiffé[^112], bien prévenu de lui,\
Que de nous démentir sur le fait d\'aujourd\'hui.

#### ORGON. {#orgon.-4 .orgon}

Je suis votre valet, et crois les apparences :\
Pour mon fripon de fils je sais vos complaisances,\
Et vous avez eu peur de le désavouer\
Du trait qu\'à ce pauvre homme il a voulu jouer ;\
Vous étiez trop tranquille enfin pour être crue,\
Et vous auriez parue d\'autre manière émue.

#### ELMIRE. {#elmire. .elmire}

Est-ce qu\'au simple aveu d\'un amoureux transport\
Il faut que notre honneur se gendarme si fort ?\
Et ne peut-on répondre à tout ce qui le touche\
Que le feu dans les yeux et l\'injure à la bouche ?\
Pour moi, de tels propos je me ris simplement,\
Et l\'éclat là-dessus ne me plaît nullement ;\
J\'aime qu\'avec douceur nous nous montrions sages,\
Et ne suis point du tout pour ces prudes sauvages\
Dont l\'honneur est armé de griffes et de dents,\
Et veut au moindre mot dévisager[^113] les gens :\
Me préserve le Ciel d\'une telle sagesse !\
Je veux une vertu qui ne soit point diablesse,\
Et crois que d\'un refus la discrète froideur\
N\'en est pas moins puissante à rebuter un coeur.

#### ORGON. {#orgon.-5 .orgon}

Enfin je sais l\'affaire et ne prends point le change.

#### ELMIRE. {#elmire.-1 .elmire}

J\'admire, encore un coup, cette foiblesse étrange.\
Mais que me répondroit votre incrédulité\
Si l\'on vous faisoit voir qu\'on vous dit vérité ?

#### ORGON. {#orgon.-6 .orgon}

Voir ?

#### ELMIRE. {#elmire.-2 .elmire}

Oui.

#### ORGON. {#orgon.-7 .orgon}

Chansons.

#### ELMIRE. {#elmire.-3 .elmire}

Mais quoi ? si je trouvois manière\
De vous le faire voir avec pleine lumière ?

#### ORGON. {#orgon.-8 .orgon}

Contes en l\'air.

#### ELMIRE. {#elmire.-4 .elmire}

Quel homme ! Au moins répondez-moi.\
Je ne vous parle pas de nous ajouter foi ;\
Mais supposons ici que, d\'un lieu qu\'on peut prendre,\
On vous fît clairement tout voir et tout entendre.\
Que diriez-vous alors de votre homme de bien ?

#### ORGON. {#orgon.-9 .orgon}

En ce cas, je dirois que\... Je ne dirois rien,\
Car cela ne se peut.

#### ELMIRE. {#elmire.-5 .elmire}

L\'erreur trop longtemps dure,\
Et c\'est trop condamner ma bouche d\'imposture.\
Il faut que par plaisir, et sans aller plus loin[^114],\
De tout ce qu\'on vous dit je vous fasse témoin.

#### ORGON. {#orgon.-10 .orgon}

Soit : je vous prends au mot. Nous verrons votre adresse,\
Et comment vous pourrez remplir cette promesse.

#### ELMIRE. {#elmire.-6 .elmire}

Faites-le moi venir.

#### DORINE. {#dorine.-2 .dorine}

Son esprit est rusé,\
Et peut-être à surprendre il sera malaisé.

#### ELMIRE. {#elmire.-7 .elmire}

Non : on est aisément dupé par ce qu\'on aime,\
Et l\'amour-propre engage à se tromper soi-même.\
*(Parlant à Cléante et à Mariane.)*\
Faites-le moi descendre. Et vous, retirez-vous.

-   [^107]:*Des droits de la naissance* : des droits que ma
    naissance vous a donnés sur moi.
-   [^108]:*Dispensez mes voeux de cette obéissance* :
    dispensez-moi de cet acte d\'obéissance malgré les voeux que j\'ai
    faits de vous obéir.
-   [^109]:*Tout le mien* : tout le bien dont Mariane a hérité de
    sa mère, la première femme d\'Orgon.
-   [^110]:*Parlez à votre écot* : « Se dit à une personne se
    mêlant de parler à des gens qui ne lui adressent pas la parole »
    (Littré).
-   [^111]:*Je vous admire* : je vous regarde avec étonnement.
-   [^112]:*Être coiffé de quelqu\'un*, c\'est être entiché de
    quelqu\'un, ne jurer que par lui.
-   [^113]:*Dévisager* : déchirer le visage.
-   [^114]:*Sans aller plus loin* : sans plus tarder.

## ACTE IV.

### SCÈNE IV

\- Elmire, Orgon.

#### ELMIRE. {#elmire.-8 .elmire}

Approchons cette table, et vous mettez dessous.

#### ORGON. {#orgon.-11 .orgon}

Comment ?

#### ELMIRE. {#elmire.-9 .elmire}

Vous bien cacher est un point nécessaire.

#### ORGON. {#orgon.-12 .orgon}

Pourquoi sous cette table ?

#### ELMIRE. {#elmire.-10 .elmire}

Ah, mon Dieu ! laissez faire :\
J\'ai mon dessein en tête, et vous en jugerez.\
Mettez-vous là, vous dis-je ; et quand vous y serez,\
Gardez qu\'on ne vous voie et qu\'on ne vous entende.

#### ORGON. {#orgon.-13 .orgon}

Je confesse qu\'ici ma complaisance est grande ;\
Mais de votre entreprise il faut vous voir sortir.

#### ELMIRE. {#elmire.-11 .elmire}

Vous n\'aurez, que je crois, rien à me repartir.\
*(À son mari qui est sous la table.)*\
Au moins, je vais toucher une étrange matière :\
Ne vous scandalisez en aucune manière.\
Quoi que je puisse dire, il[^115] doit m\'être permis,\
Et c\'est pour vous convaincre, ainsi que j\'ai promis.\
Je vais par des douceurs, puisque j\'y suis réduite,\
Faire poser le masque à cette âme hypocrite,\
Flatter de son amour les désirs effrontés,\
Et donner un champ libre à ses témérités.\
Comme c\'est pour vous seul,et pour mieux le confondre,\
Que mon âme à ses voeux va feindre de répondre,\
J\'aurai lieu de cesser dès que vous vous rendrez,\
Et les choses n\'iront que jusqu\'où vous voudrez.\
C\'est à vous d\'arrêter son ardeur insensée,\
Quand vous croirez l\'affaire assez avant poussée,\
D\'épargner votre femme, et de ne m\'exposer\
Qu\'à ce qu\'il vous faudra pour vous désabuser :\
Ce sont vos intérêts ; vous en serez le maître,\
Et\... L\'on vient. Tenez-vous, et gardez de paraître.

-   [^115]:*Il* (au neutre) : cela.

## ACTE IV.

### SCÈNE V

\- Tartuffe, Elmire, Orgon.

#### TARTUFFE. {#tartuffe.-4 .tartuffe}

On m\'a dit qu\'en ce lieu vous me vouliez parler.

#### ELMIRE. {#elmire.-12 .elmire}

Oui. L\'on a des secrets à vous y révéler.\
Mais tirez cette porte avant qu\'on vous les dise,\
Et regardez partout de crainte de surprise.\
Une affaire pareille à celle de tantôt\
N\'est pas assurément ici ce qu\'il nous faut.\
Jamais il ne s\'est vu de surprise de même ;\
Damis m\'a fait pour vous une frayeur extrême,\
Et vous avez bien vu que j\'ai fait mes efforts\
Pour rompre ses desseins et calmer ses transports.\
Mon trouble, il est bien vrai, m\'a si fort
possédée[^116],\
Que de le démentir je n\'ai point eu l\'idée ;\
Mais par là, grâce au Ciel, tout a bien mieux été,\
Et les choses en sont dans plus de sûreté[^117].\
L\'estime où l\'on vous tient a dissipé l\'orage,\
Et mon mari de vous ne peut prendre d\'ombrage.\
Pour mieux braver l\'éclat des mauvais jugements,\
Il veut que nous soyons ensemble à tous moments ;\
Et c\'est par où je puis, sans peur d\'être blâmée,\
Me trouver ici seule avec vous enfermée,\
Et ce qui m\'autorise à vous ouvrir un coeur\
Un peu trop prompt peut-être à souffrir votre ardeur.

#### TARTUFFE. {#tartuffe.-5 .tartuffe}

Ce langage à comprendre est assez difficile,\
Madame, et vous parliez tantôt d\'un autre style.

#### ELMIRE. {#elmire.-13 .elmire}

Ah ! si d\'un tel refus vous êtes en courroux,\
Que le coeur d\'une femme est mal connu de vous !\
Et que vous savez peu ce qu\'il veut faire entendre\
Lorsque si foiblement on le voit se défendre !\
Toujours notre pudeur combat dans ces moments\
Ce qu\'on peut nous donner de tendres sentiments.\
Quelque raison qu\'on trouve à l\'amour qui nous dompte,\
On trouve à l\'avouer toujours un peu de honte ;\
On s\'en défend d\'abord ; mais de l\'air qu\'on s\'y prend,\
On fait connoître assez que notre coeur se rend,\
Qu\'à nos voeux par honneur notre bouche s\'oppose,\
Et que de tels refus promettent toute chose.\
C\'est vous faire sans doute un assez libre aveu,\
Et sur notre pudeur me ménager bien peu ;\
Mais puisque la parole enfin en est lâchée,\
À retenir Damis me serois-je attachée,\
Aurois-je, je vous prie, avec tant de douceur\
Écouté tout au long l\'offre de votre coeur,\
Aurois-je pris la chose ainsi qu\'on m\'a vu faire,\
Si l\'offre de ce coeur n\'eût eu de quoi me plaire  ?\
Et lorsque j\'ai voulu moi-même vous forcer\
À refuser l\'hymen qu\'on venoit d\'annoncer,\
Qu\'est-ce que cette instance a dû vous faire entendre,\
Que l\'intérêt[^118] qu\'en vous on s\'avise de prendre,\
Et l\'ennui qu\'on auroit que ce noeud qu\'on résout\
Vînt partager du moins un coeur que l\'on veut tout ?

#### TARTUFFE. {#tartuffe.-6 .tartuffe}

C\'est sans doute[^119], Madame, une douceur extrême\
Que d\'entendre ces mots d\'une bouche qu\'on aime :\
Leur miel dans tous mes sens fait couler à longs traits\
Une suavité qu\'on ne goûta jamais.\
Le bonheur de vous plaire est ma suprême étude,\
Et mon coeur de vos voeux fait sa béatitude ;\
Mais ce coeur vous demande ici la liberté\
D\'oser douter un peu de sa félicité.\
Je puis croire ces mots un artifice honnête\
Pour m\'obliger à rompre un hymen qui s\'apprête ;\
Et s\'il faut librement m\'expliquer avec vous,\
Je ne me fierai point à des propos si doux,\
Qu\'un peu de vos faveurs, après quoi je soupire,\
Ne vienne m\'assurer tout ce qu\'ils m\'ont pu dire,\
Et planter dans mon âme une constante foi\
Des charmantes bontés que vous avez pour moi.

#### ELMIRE. *Elle tousse pour avertir son mari.* {#elmire.-elle-tousse-pour-avertir-son-mari. .elmire}

Quoi ? vous voulez aller avec cette vitesse,\
Et d\'un coeur tout d\'abord épuiser la tendresse ?\
On se tue à vous faire un aveu des plus doux ;\
Cependant ce n\'est pas encore assez pour vous,\
Et l\'on ne peut aller jusqu\'à vous satisfaire,\
Qu\'aux dernières faveurs on ne pousse l\'affaire[^120] ?

#### TARTUFFE. {#tartuffe.-7 .tartuffe}

Moins on mérite un bien, moins on l\'ose espérer.\
Nos voeux sur des discours ont peine à s\'assurer.\
On soupçonne aisément un sort[^121] tout plein de
gloire,\
Et l\'on veut en jouir avant que de le croire.\
Pour moi, qui crois si peu mériter vos bontés,\
Je doute du bonheur de mes témérités[^122] ;\
Et je ne croirai rien, que vous n\'ayez, Madame,\
Par des réalités su convaincre ma flamme.

#### ELMIRE. {#elmire.-14 .elmire}

Mon Dieu, que votre amour en vrai tyran agit,\
Et qu\'en un trouble étrange il me jette l\'esprit !\
Que sur les coeurs il prend un furieux empire,\
Et qu\'avec violence il veut ce qu\'il désire !\
Quoi ? de votre poursuite on ne peut se parer[^123],\
Et vous ne donnez pas le temps de respirer ?\
Sied-il bien de tenir une rigueur si grande,\
De vouloir sans quartier les choses qu\'on demande,\
Et d\'abuser ainsi par vos efforts pressants\
Du foible que pour vous vous voyez qu\'ont les gens ?

#### TARTUFFE. {#tartuffe.-8 .tartuffe}

Mais si d\'un oeil bénin vous voyez mes hommages,\
Pourquoi m\'en refuser d\'assurés témoignages ?

#### ELMIRE. {#elmire.-15 .elmire}

Mais comment consentir à ce que vous voulez,\
Sans offenser le Ciel, dont toujours vous parlez ?

#### TARTUFFE. {#tartuffe.-9 .tartuffe}

Si ce n\'est que le Ciel qu\'à mes voeux on oppose,\
Lever un tel obstacle est à moi peu de chose,\
Et cela ne doit pas retenir votre coeur.

#### ELMIRE. {#elmire.-16 .elmire}

Mais des arrêts du Ciel on nous fait tant de peur !

#### TARTUFFE. {#tartuffe.-10 .tartuffe}

Je puis vous dissiper ces craintes ridicules,\
Madame, et je sais l\'art de lever les scrupules.\
Le Ciel défend, de vrai, certains contentements ;\
*(C\'est un scélérat qui parle.)*\
Mais on trouve avec lui des accommodements ;\
Selon divers besoins, il est une science\
D\'étendre les liens de notre conscience,\
Et de rectifier le mal de l\'action\
Avec la pureté de notre intention[^124].\
De ces secrets, Madame, on saura vous instruire ;\
Vous n\'avez seulement qu\'à vous laisser conduire.\
Contentez mon désir, et n\'ayez point d\'effroi  :\
Je vous réponds de tout, et prends le mal sur moi.\
Vous toussez fort, Madame.

#### ELMIRE. {#elmire.-17 .elmire}

Oui, je suis au supplice.

#### TARTUFFE. {#tartuffe.-11 .tartuffe}

Vous plaît-il un morceau de ce jus de réglisse  ?

#### ELMIRE. {#elmire.-18 .elmire}

C\'est un rhume obstiné, sans doute, et je vois bien\
Que tous les jus du monde ici ne feront rien.

#### TARTUFFE. {#tartuffe.-12 .tartuffe}

Cela certe est fâcheux.

#### ELMIRE. {#elmire.-19 .elmire}

Oui, plus qu\'on ne peut dire.

#### TARTUFFE. {#tartuffe.-13 .tartuffe}

Enfin votre scrupule est facile à détruire :\
Vous êtes assurée ici d\'un plein secret,\
Et le mal n\'est jamais que dans l\'éclat qu\'on fait  ;\
Le scandale du monde est ce qui fait l\'offense,\
Et ce n\'est pas pécher que pécher en silence.

#### ELMIRE, *après avoir encore toussé*. {#elmire-après-avoir-encore-toussé. .elmire}

Enfin je vois qu\'il faut se résoudre à céder,\
Qu\'il faut que je consente à vous tout accorder,\
Et qu\'à moins de cela je ne dois point prétendre\
Qu\'on puisse être content et qu\'on veuille se rendre.\
Sans doute[^125], il est fâcheux d\'en venir jusque-là,\
Et c\'est bien malgré moi que je franchis cela ;\
Mais puisque l\'on s\'obstine à m\'y vouloir réduire,\
Puisqu\'on ne veut point croire à tout ce qu\'on peut dire,\
Et qu\'on veut des témoins qui soient plus convaincants,\
Il faut bien s\'y résoudre, et contenter les gens.\
Si ce consentement porte en soi quelque offense,\
Tant pis pour qui me force à cette violence ;\
La faute assurément n\'en doit pas être à moi.

#### TARTUFFE. {#tartuffe.-14 .tartuffe}

Oui, Madame, on s\'en charge ; et la chose de soi\...

#### ELMIRE. {#elmire.-20 .elmire}

Ouvrez un peu la porte, et voyez, je vous prie,\
Si mon mari n\'est point dans cette galerie.

#### TARTUFFE. {#tartuffe.-15 .tartuffe}

Qu\'est-il besoin pour lui du soin que vous prenez ?\
C\'est un homme, entre nous, à mener par le nez  ;\
De tous nos entretiens il est pour faire gloire,\
Et je l\'ai mis au point de voir tout sans rien croire.

#### ELMIRE. {#elmire.-21 .elmire}

Il n\'importe : sortez, je vous prie, un moment,\
Et partout là dehors voyez exactement.

-   [^116]:VAR. De mon trouble, il est vrai, j\'étais si possédée.
    (1682).
-   [^117]:VAR. Et les choses en sont en plus de sûreté. (1682).
-   [^118]:*Que l\'intérêt* : sinon l\'intérêt.
-   [^119]:*Sans doute* : sans aucun doute, assurément.
-   [^120]:0*Et l\'on ne peut aller...* : et l\'on ne peut arriver
    à vous satisfaire si l\'on ne pousse les choses jusqu\'aux dernières
    faveurs.
-   [^121]:*On soupçonne aisément un sort* : on se défie aisément
    d\'un sort...
-   [^122]:Ces vers 1459-1464 sont repris, à quelques
    modifications près, dans *Dom Garcie de Navarre*, v. 654-659.
-   [^123]:*Se parer* : se garder, se protéger.
-   [^124]:C\'est la fameuse *direction d\'intention*, que Pascal
    a reprochée aux casuistes jésuites dans sa VIIe Provinciale.
-   [^125]:*Sans doute* : sans aucun doute, assurément.

## ACTE IV.

### SCÈNE VI

\- Orgon, Elmire.

#### ORGON, *sortant de dessous la table.* {#orgon-sortant-de-dessous-la-table. .orgon}

Voilà, je vous l\'avoue, un abominable homme.\
Je n\'en puis revenir, et tout ceci m\'assomme.

#### ELMIRE. {#elmire.-22 .elmire}

Quoi ? vous sortez si tôt ? vous vous moquez des gens.\
Rentrez sous le tapis, il n\'est pas encor temps ;\
Attendez jusqu\'au bout pour voir les choses sûres,\
Et ne vous fiez point aux simples conjectures.

#### ORGON. {#orgon.-14 .orgon}

Non, rien de plus méchant n\'est sorti de l\'enfer.

#### ELMIRE. {#elmire.-23 .elmire}

Mon Dieu ! l\'on ne doit point croire trop de léger.\
Laissez-vous bien convaincre avant que de vous rendre,\
Et ne vous hâtez point, de peur de vous méprendre[^126].\
*(Elle fait mettre son mari derrière elle.)*

-   [^126]:VAR. Et ne vous hâtez pas, de peur de vous méprendre.
    (1682).

## ACTE IV.

### SCÈNE VII

\- Tartuffe, Elmire, Orgon.

#### TARTUFFE. {#tartuffe.-16 .tartuffe}

Tout conspire, Madame, à mon contentement :\
J\'ai visité de l\'oeil tout cet appartement ;\
Personne ne s\'y trouve ; et mon âme ravie\...

#### ORGON, *en l\'arrêtant.* {#orgon-en-larrêtant. .orgon}

Tout doux ! vous suivez trop votre amoureuse envie,\
Et vous ne devez pas vous tant passionner.\
Ah ! ah ! l\'homme de bien, vous m\'en voulez
donner[^127] !\
Comme aux tentations s\'abandonne votre âme !\
Vous épousiez ma fille, et convoitiez ma femme !\
J\'ai douté fort longtemps que ce fût tout de bon,\
Et je croyois toujours qu\'on changeroit de ton  ;\
Mais c\'est assez avant pousser le témoignage  :\
Je m\'y tiens, et n\'en veux, pour moi, pas davantage.

#### ELMIRE, *à Tartuffe.* {#elmire-à-tartuffe. .elmire}

C\'est contre mon humeur que j\'ai fait tout ceci  ;\
Mais on m\'a mise au point de vous traiter ainsi.

#### TARTUFFE. {#tartuffe.-17 .tartuffe}

Quoi ? vous croyez\... ?

#### ORGON. {#orgon.-15 .orgon}

Allons, point de bruit, je vous prie.\
Dénichez de céans, et sans cérémonie.

#### TARTUFFE. {#tartuffe.-18 .tartuffe}

Mon dessein\...

#### ORGON. {#orgon.-16 .orgon}

Ces discours ne sont plus de saison :\
Il faut, tout sur-le-champ, sortir de la maison.

#### TARTUFFE. {#tartuffe.-19 .tartuffe}

C\'est à vous d\'en sortir, vous qui parlez en maître :\
La maison m\'appartient, je le ferai connaître,\
Et vous montrerai bien qu\'en vain on a recours,\
Pour me chercher querelle, à ces lâches détours,\
Qu\'on n\'est pas où l\'on pense en me faisant injure,\
Que j\'ai de quoi confondre et punir l\'imposture,\
Venger le Ciel qu\'on blesse, et faire repentir\
Ceux qui parlent ici de me faire sortir.

-   [^127]:VAR. Ah ! ah ! l\'homme de bien, vous m\'en vouliez
    donner ! (1682). En donner à quelqu\'un: le tromper.

## ACTE IV.

### SCÈNE VIII

\- Elmire, Orgon.

#### ELMIRE. {#elmire.-24 .elmire}

Quel est donc ce langage ? et qu\'est-ce qu\'il veut dire ?

#### ORGON. {#orgon.-17 .orgon}

Ma foi, je suis confus, et n\'ai pas lieu de rire.

#### ELMIRE. {#elmire.-25 .elmire}

Comment ?

#### ORGON. {#orgon.-18 .orgon}

Je vois ma faute aux choses qu\'il me dit,\
Et la donation m\'embarrasse l\'esprit.

#### ELMIRE. {#elmire.-26 .elmire}

La donation\...

#### ORGON. {#orgon.-19 .orgon}

Oui, c\'est une affaire faite.\
Mais j\'ai quelque autre chose encor qui m\'inquiète.

#### ELMIRE. {#elmire.-27 .elmire}

Et quoi ?

#### ORGON. {#orgon.-20 .orgon}

Vous saurez tout. Mais voyons au plus tôt\
Si certaine cassette est encore là-haut.
