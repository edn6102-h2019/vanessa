## ACTE I.

### SCÈNE PREMIÈRE

\- Madame Pernelle et Flipote *sa servante*, Elmire, Mariane, Dorine,
Damis, Cléante.

#### MADAME PERNELLE. {#madame-pernelle. .pernelle}

Allons, Flipote, allons, que d\'eux je me délivre.

#### ELMIRE. {#elmire. .elmire}

Vous marchez d\'un tel pas qu\'on a peine à vous suivre.

#### MADAME PERNELLE. {#madame-pernelle.-1 .pernelle}

Laissez, ma bru, laissez, ne venez pas plus loin :\
Ce sont toutes façons dont je n\'ai pas besoin.

#### ELMIRE. {#elmire.-1 .elmire}

De ce que l\'on vous doit envers vous on s\'acquitte,\
Mais ma mère, d\'où vient que vous sortez si vite ?

#### MADAME PERNELLE. {#madame-pernelle.-2 .pernelle}

C\'est que je ne puis voir tout ce ménage-ci,\
Et que de me complaire on ne prend nul souci.\
Oui, je sors de chez vous fort mal édifiée :\
Dans toutes mes leçons j\'y suis contrariée,\
On n\'y respecte rien, chacun y parle haut,\
Et c\'est tout justement la cour du roi Pétaut.[^1]

#### DORINE. {#dorine. .dorine}

Si\...

#### MADAME PERNELLE. {#madame-pernelle.-3 .pernelle}

Vous êtes, mamie, une fille suivante\
Un peu trop forte en gueule, et fort impertinente :\
Vous vous mêlez sur tout de dire votre avis.

#### DAMIS. {#damis. .damis}

Mais\...

#### MADAME PERNELLE. {#madame-pernelle.-4 .pernelle}

Vous êtes un sot en trois lettres, mon fils.\
C\'est moi qui vous le dis, qui suis votre grand\'mère ;\
Et j\'ai prédit cent fois à mon fils, votre père,\
Que vous preniez tout l\'air d\'un méchant garnement,\
Et ne lui donneriez jamais que du tourment.

#### MARIANE. {#mariane. .mariane}

Je crois\...

#### MADAME PERNELLE. {#madame-pernelle.-5 .pernelle}

Mon Dieu, sa soeur, vous faites la discrette,\
Et vous n\'y touchez pas, tant vous semblez doucette ;\
Mais il n\'est, comme on dit, pire eau que l\'eau qui dort,\
Et vous menez sous chape un train que je hais fort.

#### ELMIRE. {#elmire.-2 .elmire}

Mais, ma mère,\...

#### MADAME PERNELLE. {#madame-pernelle.-6 .pernelle}

Ma bru, qu\'il ne vous en déplaise,\
Votre conduite en tout est tout à fait mauvaise ;\
Vous devriez leur mettre un bon exemple aux yeux,\
Et leur défunte mère en usoit beaucoup mieux.\
Vous êtes dépensière ; et cet état me blesse,\
Que vous alliez vêtue ainsi qu\'une princesse.\
Quiconque à son mari veut plaire seulement,\
Ma bru, n\'a pas besoin de tant d\'ajustement.

#### CLÉANTE. {#cléante. .cleante}

Mais, Madame, après tout\...

#### MADAME PERNELLE. {#madame-pernelle.-7 .pernelle}

Pour vous, Monsieur son frère,\
Je vous estime fort, vous aime, et vous révère ;\
Mais enfin, si j\'étois de mon fils, son époux,\
Je vous prierois bien fort de n\'entrer point chez nous.\
Sans cesse vous prêchez des maximes de vivre\
Qui par d\'honnêtes gens ne se doivent point suivre.\
Je vous parle un peu franc ; mais c\'est là mon humeur,\
Et je ne mâche point ce que j\'ai sur le coeur.

#### DAMIS. {#damis.-1 .damis}

Votre Monsieur Tartuffe est bien heureux sans doute\...

#### MADAME PERNELLE. {#madame-pernelle.-8 .pernelle}

C\'est un homme de bien, qu\'il faut que l\'on écoute ;\
Et je ne puis souffrir sans me mettre en courroux\
De le voir querellé par un fou comme vous.

#### DAMIS. {#damis.-2 .damis}

Quoi ? je souffrirai, moi, qu\'un cagot[^2] de critique\
Vienne usurper céans un pouvoir tyrannique,\
Et que nous ne puissions à rien nous divertir,\
Si ce beau Monsieur-là n\'y daigne consentir ?

#### DORINE. {#dorine.-1 .dorine}

S\'il le faut écouter et croire à ses maximes,\
On ne peut faire rien qu\'on ne fasse des crimes ;\
Car il contrôle tout, ce critique zélé.

#### MADAME PERNELLE. {#madame-pernelle.-9 .pernelle}

Et tout ce qu\'il contrôle est fort bien contrôlé.\
C\'est au chemin du Ciel qu\'il prétend vous conduire,\
Et mon fils à l\'aimer vous devroit tous induire.

#### DAMIS. {#damis.-3 .damis}

Non, voyez-vous, ma mère, il n\'est père ni rien\
Qui me puisse obliger à lui vouloir du bien :\
Je trahirois mon coeur de parler d\'autre sorte ;\
Sur ses façons de faire à tous coups je m\'emporte ;\
J\'en prévois une suite, et qu\'avec ce pied plat\
Il faudra que j\'en vienne à quelque grand éclat.

#### DORINE. {#dorine.-2 .dorine}

Certes c\'est une chose aussi qui scandalise,\
De voir qu\'un inconnu céans s\'impatronise,\
Qu\'un gueux qui, quand il vint, n\'avoit pas de souliers\
Et dont l\'habit entier valoit bien six deniers,\
En vienne jusque-là que de se méconnaître,\
De contrarier tout, et de faire le maître.

#### MADAME PERNELLE. {#madame-pernelle.-10 .pernelle}

Hé ! merci de ma vie[^3] ! il en iroit bien mieux,\
Si tout se gouvernoit par ses ordres pieux.

#### DORINE. {#dorine.-3 .dorine}

Il passe pour un saint dans votre fantaisie :\
Tout son fait, croyez-moi, n\'est rien qu\'hypocrisie.

#### MADAME PERNELLE. {#madame-pernelle.-11 .pernelle}

Voyez la langue !

#### DORINE. {#dorine.-4 .dorine}

À lui, non plus qu\'à son Laurent,\
Je ne me fierois, moi, que sur un bon garant.

#### MADAME PERNELLE. {#madame-pernelle.-12 .pernelle}

J\'ignore ce qu\'au fond le serviteur peut être ;\
Mais pour homme de bien, je garantis le maître.\
Vous ne lui voulez mal et ne le rebutez[^4]\
Qu\'à cause qu\'il vous dit à tous vos vérités.\
C\'est contre le péché que son coeur se courrouce,\
Et l\'intérêt du Ciel est tout ce qui le pousse.

#### DORINE. {#dorine.-5 .dorine}

Oui ; mais pourquoi, surtout depuis un certain temps,\
Ne sauroit-il souffrir qu\'aucun hante céans ?\
En quoi blesse le Ciel une visite honnête,\
Pour en faire un vacarme à nous rompre la tête ?\
Veut-on que là-dessus je m\'explique entre nous ?\
Je crois que de Madame il est, ma foi, jaloux.

#### MADAME PERNELLE. {#madame-pernelle.-13 .pernelle}

Taisez-vous, et songez aux choses que vous dites.\
Ce n\'est pas lui tout seul qui blâme ces visites.\
Tout ce tracas qui suit les gens que vous hantez,\
Ces carrosses sans cesse à la porte plantés,\
Et de tant de laquais le bruyant assemblage\
Font un éclat fâcheux dans tout le voisinage.\
Je veux croire qu\'au fond il ne se passe rien ;\
Mais enfin on en parle, et cela n\'est pas bien.

#### CLÉANTE. {#cléante.-1 .cleante}

Hé ! voulez-vous, Madame, empêcher qu\'on ne cause ?\
Ce seroit dans la vie une fâcheuse chose,\
Si pour les sots discours où l\'on peut être mis,\
Il falloit renoncer à ses meilleurs amis.\
Et quand même on pourroit se résoudre à le faire,\
Croiriez-vous obliger tout le monde à se taire ?\
Contre la médisance il n\'est point de rempart.\
À tous les sots caquets n\'ayons donc nul égard ;\
Efforçons-nous de vivre avec toute innocence,\
Et laissons aux causeurs une pleine licence.

#### DORINE. {#dorine.-6 .dorine}

Daphné, notre voisine, et son petit époux\
Ne seroient-ils point ceux qui parlent mal de nous ?\
Ceux de qui la conduite offre le plus à rire\
Sont toujours sur autrui les premiers à médire ;\
Ils ne manquent jamais de saisir promptement\
L\'apparente lueur du moindre attachement,\
D\'en semer la nouvelle avec beaucoup de joie,\
Et d\'y donner le tour qu\'ils veulent qu\'on y croie :\
Des actions d\'autrui, teintes de leurs couleurs,\
Ils pensent dans le monde autoriser les leurs,\
Et sous le faux espoir de quelque ressemblance,\
Aux intrigues qu\'ils ont donner de l\'innocence,\
Ou faire ailleurs tomber quelques traits partagés\
De ce blâme public dont ils sont trop chargés.

#### MADAME PERNELLE. {#madame-pernelle.-14 .pernelle}

Tous ces raisonnements ne font rien à l\'affaire.\
On sait qu\'Orante mène une vie exemplaire :\
Tout ses soins vont au Ciel ; et j\'ai su par des gens\
Qu\'elle condamne fort le train qui vient céans.

#### DORINE. {#dorine.-7 .dorine}

L\'exemple est admirable, et cette dame est bonne !\
Il est vrai qu\'elle vit en austère personne ;\
Mais l\'âge dans son âme a mis ce zèle ardent,\
Et l\'on sait qu\'elle est prude à son corps défendant.\
Tant qu\'elle a pu des coeurs attirer les hommages,\
Elle a fort bien joui de tous ses avantages ;\
Mais, voyant de ses yeux tous les brillants[^5] baisser,\
Au monde, qui la quitte, elle veut renoncer,\
Et du voile pompeux d\'une haute sagesse\
De ses attraits usés déguise la foiblesse.\
Ce sont là les retours des coquettes du temps.\
Il leur est dur de voir déserter les galants.\
Dans un tel abandon, leur sombre inquiétude\
Ne voit d\'autre recours que le métier de prude ;\
Et la sévérité de ces femmes de bien\
Censure toute chose, et ne pardonne à rien ;\
Hautement d\'un chacun elles blâment la vie,\
Non point par charité, mais par un trait d\'envie,\
Qui ne sauroit souffrir qu\'une autre ait les plaisirs[^6]\
Dont le penchant de l\'âge a sevré leurs désirs.

#### MADAME PERNELLE. {#madame-pernelle.-15 .pernelle}

Voilà les contes bleus[^7] qu\'il vous faut pour vous
plaire.\
Ma bru, l\'on est chez vous contrainte de se taire,\
Car Madame[^8] à jaser tient le dé tout le jour.\
Mais enfin je prétends discourir à mon tour :\
Je vous dis que mon fils n\'a rien fait de plus sage\
Qu\'en recueillant chez soi ce dévot personnage ;\
Que le Ciel au besoin[^9] l\'a céans envoyé\
Pour redresser à tous votre esprit fourvoyé ;\
Que pour votre salut vous le devez entendre,\
Et qu\'il ne reprend rien qui ne soit à reprendre.\
Ces visites, ces bals, ces conversations\
Sont du malin esprit toutes inventions.\
Là jamais on n\'entend de pieuses paroles :\
Ce sont propos oisifs, chansons et fariboles ;\
Bien souvent le prochain en a sa bonne part,\
Et l\'on y sait médire et du tiers et du quart.\
Enfin les gens sensés ont leurs têtes troublées\
De la confusion de telles assemblées :\
Mille caquets divers s\'y font en moins de rien ;\
Et comme l\'autre jour un docteur dit fort bien,\
C\'est véritablement la tour de Babylone,\
Car chacun y babille, et tout du long de l\'aune[^10] ;\
Et pour conter l\'histoire où ce point l\'engagea\...\
Voilà-t-il pas Monsieur qui ricane déjà !\
Allez chercher vos fous qui vous donnent à rire,\
Et sans\... Adieu, ma bru : je ne veux plus rien dire.\
Sachez que pour céans j\'en rabats de moitié[^11],\
Et qu\'il fera beau temps quand j\'y mettrai le pied.\
*(Donnant un soufflet à Flipote.)*\
Allons, vous, vous rêvez, et bayez aux corneilles.\
Jour de Dieu[^12] ! je saurai vous frotter les oreilles.\
Marchons, gaupe[^13], marchons.

-   [^1]:Le roi Pétaut, auquel personne n\'obéissait, est le nom du
    chef que se donnait, par dérision, la corporation des mendiants
    parisiens.
-   [^2]:Un cagot est un faux dévot, un hypocrite ; un cagot de
    critique est un hypocrite qui se mêle de tout critiquer.
-   [^3]:Merci de ma vie est un « serment du petit peuple »
    (Dictionnaire de Furetière, 1690) : Que Dieu ait pitié de ma vie.
-   [^4]:*Rebuter* quelqu\'un : rejeter ses conseils.
-   [^5]:*Les brillants* : l\'éclat, la beauté.
-   [^6]:VAR. Qui ne saurait souffrir qu\'un autre ait les
    plaisirs.(1682). Un autre, au sens général d\'une autre, est
    fréquent au XVIIe siècle.
-   [^7]:*Les contes bleus* : des adaptations populaires des romans
    de chevalerie formaient une « Bibliothèque bleue », ainsi appelée à
    cause de la couleur du papier utilisée pour la couverture des
    volumes.
-   [^8]:*Madame* : c\'est Dorine qui est ici ironiquement désignée.
-   [^9]:*Au besoin* : parce qu\'il en était besoin.
-   [^10]::« On dit qu\'un homme en aura tout le long de l\'aune
    pour dire qu\'on lui fera tout le mal qu\'on pourra » (Dictionnaire
    de Furetière, 1690).
-   [^11]:*Pour céans j\'en rabats de moitié* : l\'estime que
    j\'avais pour cette maison diminue fortement.
-   [^12]:*Jour de Dieu* : « Sorte de serment burlesque, et qui ne
    se fait que par les femmes » (Dictionnaire de Richelet, 1679).
-   [^13]:Gaupe : « Terme d\'injure et de mépris. Femme malpropre
    et désagréable » (Littré).

## ACTE I.

### SCÈNE II

\- Cléante, Dorine.

#### CLÉANTE. {#cléante.-2 .cleante}

Je n\'y veux point aller,\
De peur qu\'elle ne vînt encor me quereller,\
Que cette bonne femme\...[^14]

#### DORINE. {#dorine.-8 .dorine}

Ah ! certes, c\'est dommage\
Qu\'elle ne vous ouît tenir un tel langage :\
Elle vous diroit bien qu\'elle vous trouve bon,\
Et qu\'elle n\'est point d\'âge à lui donner ce nom.\

#### CLÉANTE. {#cléante.-3 .cleante}

Comme elle s\'est pour rien contre nous échauffée ! Et que de son
Tartuffe elle paroît coiffée[^15] !

#### DORINE. {#dorine.-9 .dorine}

Oh ! vraiment tout cela n\'est rien au prix du fils,\
Et si vous l\'aviez vu, vous diriez : « C\'est bien pis ! »\
Nos troubles l\'avoient mis sur le pied d\'homme
sage[^16],\
Et pour servir son prince il montra du courage ;\
Mais il est devenu comme un homme hébété,\
Depuis que de Tartuffe on le voit entêté ;\
Il l\'appelle son frère, et l\'aime dans son âme\
Cent fois plus qu\'il ne fait mère, fils, fille et femme.\
C\'est de tous ses secrets l\'unique confident,\
Et de ses actions le directeur[^17] prudent ;\
Il le choie, il l\'embrasse, et pour une maîtresse\
On ne sauroit, je pense, avoir plus de tendresse ;\
À table, au plus haut bout[^18] il veut qu\'il soit
assis ;\
Avec joie il l\'y voit manger autant que six ;\
Les bons morceaux de tout, il faut qu\'on les lui
cède[^19] ;\
Et s\'il vient à roter, il lui dit : « Dieu vous
aide[^20] ! »\
*(C\'est une servante qui parle.)*\
Enfin il en est fou ; c\'est son tout, son héros ;\
Il l\'admire à tous coups, le cite à tout propos ;\
Ses moindres actions lui semblent des miracles,\
Et tous les mots qu\'il dit sont pour lui des oracles.\
Lui, qui connoît sa dupe et qui veut en jouir,\
Par cent dehors fardés a l\'art de l\'éblouir[^21] ;\
Son cagotisme[^22] en tire à toute heure des sommes,\
Et prend droit de gloser[^23] sur tous tant que nous
sommes.\
Il n\'est pas jusqu\'au fat qui lui sert de garçon[^24]\
Qui ne se mêle aussi de nous faire leçon ;\
Il vient nous sermonner avec des yeux farouches,\
Et jeter nos rubans, notre rouge et nos mouches.\
Le traître, l\'autre jour, nous rompit de ses mains\
Un mouchoir qu\'il trouva dans une *Fleur des
Saints*[^25],\
Disant que nous mêlions, par un crime effroyable,\
Avec la sainteté les parures du diable.

-   [^14]:*Cette bonne femme* : cette vieille femme (féminin de
    bonhomme, qui signifie alors vieil homme).
-   [^15]:*Être coiffé de quelqu\'un*, c\'est être entiché de
    quelqu\'un, ne jurer que par lui.
-   [^16]:*Nos troubles* : les troubles de la Fronde (1648-1652),
    durant lesquels Orgon n\'a suivi ni le parti du Parlement, ni celui
    des Princes, mais est resté fidèle à la Cour. Il a ainsi acquis la
    réputation d\'homme sage auprès du Roi, qui le récompensera au
    dénouement.
-   [^17]:*Le directeur* : Tartuffe est bien le directeur de
    conscience d\'Orgon. Certains laïcs, au XVIIe siècle, prenaient
    ainsi la responsabilité de diriger les âmes: ainsi, Jean de
    Bernières-Louvigny ou le baron Gaston de Renty.
-   [^18]:*Au plus haut bout* : à l\'endroit le plus honorable.
-   [^19]:VAR. Les bons morceaux de tout, il faut qu\'on les lui
    cède. (1682).
-   [^20]:Les vers 191 à 194 étaient sautés à la représentation.
-   [^21]:*Par cent dehors fardés a l\'art de l\'éblouir* : a
    l\'art de le tromper par cent apparences fallacieuses.
-   [^22]:*Cagotisme* : mot forgé sur cagot, faux dévot, hypocrite.
-   [^23]:*Gloser* : critiquer.
-   [^24]:*Fat* : sot, niais ; garçon : valet à tout faire.
-   [^25]:*Fleur des Saints* : probablement un des deux gros
    volumes de l\'ouvrage du jésuite espagnol Ribadeneira, *Les Fleurs
    des Saints et les fêtes de toute l\'année*, qui avait été traduit en
    français.

## ACTE I.

### SCÈNE III

\- Elmire, Mariane, Damis, Cléante, Dorine.

#### ELMIRE. {#elmire.-3 .elmire}

Vous êtes bien heureux de n\'être point venu\
Au discours qu\'à la porte elle nous a tenu.\
Mais j\'ai vu mon mari : comme il ne m\'a point vue,\
Je veux aller là-haut attendre sa venue.

#### CLÉANTE. {#cléante.-4 .cleante}

Moi, je l\'attends ici pour moins d\'amusement[^26],\
Et je vais lui donner le bonjour seulement.

#### DAMIS. {#damis.-4 .damis}

De l\'hymen[^27] de ma soeur touchez-lui quelque chose.\
J\'ai soupçon que Tartuffe à son effet[^28] s\'oppose,\
Qu\'il oblige mon père à des détours si grands ;\
Et vous n\'ignorez pas quel intérêt j\'y prends.\
Si même ardeur enflamme et ma soeur et Valère,\
La soeur de cet ami, vous le savez, m\'est chère ;\
Et s\'il falloit\...

#### DORINE. {#dorine.-10 .dorine}

Il entre.

-   [^26]:*Pour moins d\'amusement* : pour perdre moins de temps.
-   [^27]:*L\'hymen* : le mariage.
-   [^28]:*À son effet* : à sa célébration.

## ACTE I.

### SCÈNE IV

\- Orgon, Cléante, Dorine.

#### ORGON. {#orgon. .orgon}

Ah ! mon frère, bonjour.

#### CLÉANTE. {#cléante.-5 .cleante}

Je sortois, et j\'ai joie à vous voir de retour.\
La campagne à présent n\'est pas beaucoup fleurie.

#### ORGON. {#orgon.-1 .orgon}

Dorine\...Mon beau-frère, attendez, je vous prie :\
Vous voulez bien souffrir, pour m\'ôter de souci,\
Que je m\'informe un peu des nouvelles d\'ici.\
Tout s\'est-il, ces deux jours, passé de bonne sorte ?\
Qu\'est-ce qu\'on fait céans ? comme est-ce qu\'on s\'y porte ?

#### DORINE. {#dorine.-11 .dorine}

Madame eut avant-hier la fièvre jusqu\'au soir,\
Avec un mal de tête étrange à concevoir.

#### ORGON. {#orgon.-2 .orgon}

Et Tartuffe ?

#### DORINE. {#dorine.-12 .dorine}

Tartuffe ? Il se porte à merveille,\
Gros et gras, le teint frais, et la bouche vermeille.

#### ORGON. {#orgon.-3 .orgon}

Le pauvre homme !

#### DORINE. {#dorine.-13 .dorine}

Le soir, elle eut un grand dégoût,\
Et ne put au souper toucher à rien du tout,\
Tant sa douleur de tête étoit encore cruelle !

#### ORGON. {#orgon.-4 .orgon}

Et Tartuffe ?

#### DORINE. {#dorine.-14 .dorine}

Il soupa, lui tout seul, devant elle,\
Et fort dévotement il mangea deux perdrix,\
Avec une moitié de gigot en hachis.

#### ORGON. {#orgon.-5 .orgon}

Le pauvre homme !

#### DORINE. {#dorine.-15 .dorine}

La nuit se passa toute entière\
Sans qu\'elle pût fermer un moment la paupière ;\
Des chaleurs l\'empêchoient de pouvoir sommeiller,\
Et jusqu\'au jour près d\'elle il nous fallut veiller.

#### ORGON. {#orgon.-6 .orgon}

Et Tartuffe ?

#### DORINE. {#dorine.-16 .dorine}

Pressé d\'un sommeil agréable,\
Il passa dans sa chambre au sortir de la table,\
Et dans son lit bien chaud il se mit tout soudain,\
Où sans trouble il dormit jusques au lendemain.

#### ORGON. {#orgon.-7 .orgon}

Le pauvre homme !

#### DORINE. {#dorine.-17 .dorine}

À la fin, par nos raisons gagnée,\
Elle se résolut à souffrir la saignée,\
Et le soulagement suivit tout aussitôt.

#### ORGON. {#orgon.-8 .orgon}

Et Tartuffe ?

#### DORINE. {#dorine.-18 .dorine}

Il reprit courage comme il faut,\
Et contre tous les maux fortifiant son âme,\
Pour réparer le sang qu\'avoit perdu Madame,\
But à son déjeuner quatre grands coups de vin.

#### ORGON. {#orgon.-9 .orgon}

Le pauvre homme[^29] !

#### DORINE. {#dorine.-19 .dorine}

Tous deux se portent bien enfin ;\
Et je vais à Madame annoncer par avance\
La part que vous prenez à sa convalescence.

-   [^29]:On renvoie souvent, à propos de cette exclamation
    répétée, à Tallement des Réaux (Historiettes, éd. A. Adam,
    Pléiade, t. I, p. 295) : le gardien d\'un couvent de capucins, à qui
    on donnait d\'excellentes nouvelles du Père Joseph, l\'éminence
    grise de Richelieu, ne cessait de dire, avec une admiration
    attendrie : « Le pauvre homme ! ». Molière a-t-il eu connaissance de
    l\'anecdote ?

## ACTE I.

### SCÈNE V

\- Orgon, Cléante.

#### CLÉANTE. {#cléante.-6 .cleante}

À votre nez, mon frère, elle se rit de vous ;\
Et sans avoir dessein de vous mettre en courroux,\
Je vous dirai tout franc que c\'est avec justice.\
A-t-on jamais parlé d\'un semblable caprice ?\
Et se peut-il qu\'un homme ait un charme aujourd\'hui\
À vous faire oublier toutes choses pour lui,\
Qu\'après avoir chez vous réparé sa misère,\
Vous en veniez au point\... ?

#### ORGON. {#orgon.-10 .orgon}

Alte-là, mon beau-frère : Vous ne connoissez pas celui dont vous parlez.

#### CLÉANTE. {#cléante.-7 .cleante}

Je ne le connois pas, puisque vous le voulez ;\
Mais enfin, pour savoir quel homme ce peut être\...

#### ORGON. {#orgon.-11 .orgon}

Mon frère, vous seriez charmé de le connoître,\
Et vos ravissements[^30] ne prendroient point de fin.\
C\'est un homme\... qui\... ha !\... un homme\... un homme enfin.\
Qui suit bien ses leçons goûte une paix profonde,\
Et comme du fumier regarde tout le monde[^31].\
Oui, je deviens tout autre avec son entretien ;\
Il m\'enseigne à n\'avoir affection pour rien,\
De toutes amitiés il détache mon âme ;\
Et je verrois mourir frère, enfants, mère et femme,\
Que je m\'en soucierois autant que de cela.

#### CLÉANTE. {#cléante.-8 .cleante}

Les sentiments humains, mon frère, que voilà !

#### ORGON. {#orgon.-12 .orgon}

Ha ! si vous aviez vu comme j\'en fis rencontre,\
Vous auriez pris pour lui l\'amitié que je montre.\
Chaque jour à l\'église il venoit, d\'un air doux,\
Tout vis-à-vis de moi se mettre à deux genoux.\
Il attiroit les yeux de l\'assemblée entière\
Par l\'ardeur dont au Ciel il poussoit sa prière ;\
Il faisoit des soupirs, de grands élancements,\
Et baisoit humblement la terre à tous moments ;\
Et lorsque je sortois, il me devançoit vite,\
Pour m\'aller à la porte offrir de l\'eau bénite.\
Instruit par son garçon, qui dans tout l\'imitoit,\
Et de son indigence, et de ce qu\'il étoit,\
Je lui faisois des dons ; mais avec modestie\
Il me vouloit toujours en rendre une partie.\
« C\'est trop, me disoit-il, c\'est trop de la moitié ;\
Je ne mérite pas de vous faire pitié ; »\
Et quand je refusois de le vouloir reprendre,\
Aux pauvres, à mes yeux, il alloit le répandre.\
Enfin le Ciel chez moi me le fit retirer,\
Et depuis ce temps-là tout semble y prospérer.\
Je vois qu\'il reprend tout, et qu\'à ma femme même\
Il prend, pour mon honneur, un intérêt extrême ;\
Il m\'avertit des gens qui lui font les yeux doux,\
Et plus que moi six fois il s\'en montre jaloux.\
Mais vous ne croiriez point jusqu\'où monte son zèle :\
Il s\'impute à péché la moindre bagatelle ;\
Un rien presque suffit pour le scandaliser ;\
Jusque-là qu\'il se vint l\'autre jour accuser\
D\'avoir pris une puce en faisant sa prière,\
Et de l\'avoir tuée avec trop de colère.

#### CLÉANTE. {#cléante.-9 .cleante}

Parbleu ! vous êtes fou, mon frère, que je croi.\
Avec de tels discours vous moquez-vous de moi ?\
Et que prétendez-vous que tout ce badinage[^32]\... ?

#### ORGON. {#orgon.-13 .orgon}

Mon frère, ce discours sent le libertinage[^33] :\
Vous en êtes un peu dans votre âme entiché[^34] ;\
Et comme je vous l\'ai plus de dix fois prêché,\
Vous vous attirerez quelque méchante affaire.

#### CLÉANTE. {#cléante.-10 .cleante}

Voilà de vos pareils le discours ordinaire :\
Ils veulent que chacun soit aveugle comme eux.\
C\'est être libertin que d\'avoir de bons yeux,\
Et qui n\'adore pas de vaines simagrées,\
N\'a ni respect ni foi pour les choses sacrées.\
Allez, tous vos discours ne me font point de peur :\
Je sais comme je parle, et le Ciel voit mon coeur.\
De tous vos façonniers on n\'est point les esclaves.\
Il est de faux dévots ainsi que de faux braves ;\
Et comme on ne voit pas qu\'où l\'honneur les conduit\
Les vrais braves soient ceux qui font beaucoup de bruit,\
Les bons et vrais dévots, qu\'on doit suivre à la trace,\
Ne sont pas ceux aussi qui font tant de grimace.\
Hé quoi ? vous ne ferez nulle distinction\
Entre l\'hypocrisie et la dévotion ?\
Vous les voulez traiter d\'un semblable langage,\
Et rendre même honneur au masque qu\'au visage,\
Égaler l\'artifice à la sincérité,\
Confondre l\'apparence avec la vérité,\
Estimer le fantôme autant que la personne,\
Et la fausse monnaie à l\'égal de la bonne ?\
Les hommes la plupart sont étrangement faits !\
Dans la juste nature on ne les voit jamais ;\
La raison a pour eux des bornes trop petites ;\
En chaque caractère ils passent ses limites ;\
Et la plus noble chose, ils la gâtent souvent\
Pour la vouloir outrer et pousser trop avant.\
Que cela vous soit dit en passant, mon beau-frère.

#### ORGON. {#orgon.-14 .orgon}

Oui, vous êtes, sans doute[^35] un docteur qu\'on
révère ;\
Tout le savoir du monde est chez vous retiré ;\
Vous êtes le seul sage et le seul éclairé,\
Un oracle, un Caton[^36] dans le siècle où nous sommes ;\
Et près de vous ce sont des sots que tous les hommes.

#### CLÉANTE. {#cléante.-11 .cleante}

Je ne suis point, mon frère, un docteur révéré,\
Et le savoir chez moi n\'est pas tout retiré.\
Mais, en un mot, je sais, pour toute ma science,\
Du faux avec le vrai faire la différence.\
Et comme je ne vois nul genre de héros\
Qui soient plus à priser que les parfaits dévots,\
Aucune chose au monde et plus noble et plus belle\
Que la sainte ferveur d\'un véritable zèle,\
Aussi ne vois-je rien qui soit plus odieux\
Que le dehors plâtré d\'un zèle spécieux,\
Que ces francs charlatans, que ces dévots de place[^37],\
De qui la sacrilège et trompeuse grimace\
Abuse impunément et se joue à leur gré\
De ce qu\'ont les mortels de plus saint et sacré,\
Ces gens qui, par une âme à l\'intérêt soumise,\
Font de dévotion métier et marchandise,\
Et veulent acheter crédit et dignités\
À prix de faux clins d\'yeux et d\'élans affectés,\
Ces gens, dis-je, qu\'on voit d\'une ardeur non commune\
Par le chemin du Ciel courir à leur fortune,\
Qui, brûlants et priants, demandent chaque jour[^38],\
Et prêchent la retraite au milieu de la cour,\
Qui savent ajuster leur zèle avec leurs vices,\
Sont prompts, vindicatifs, sans foi, pleins d\'artifices,\
Et pour perdre quelqu\'un couvrent insolemment\
De l\'intérêt du Ciel, leur fier[^39] ressentiment,\
D\'autant plus dangereux dans leur âpre colère,\
Qu\'ils prennent contre nous des armes qu\'on révère,\
Et que leur passion, dont on leur sait bon gré,\
Veut nous assassiner avec un fer sacré.\
De ce faux caractère on en voit trop paroître ;\
Mais les dévots de coeur sont aisés à connoître.\
Notre siècle, mon frère, en expose à nos yeux\
Qui peuvent nous servir d\'exemples glorieux :\
Regardez Ariston, regardez Périandre,\
Oronte, Alcidamas, Polydore, Clitandre ;\
Ce titre par aucun ne leur est débattu ;\
Ce ne sont point du tout fanfarons de vertu ;\
On ne voit point en eux ce faste insupportable,\
Et leur dévotion est humaine, est traitable ;\
Ils ne censurent point toutes nos actions :\
Ils trouvent trop d\'orgueil dans ces corrections ;\
Et laissant la fierté des paroles aux autres,\
C\'est par leurs actions qu\'ils reprennent les nôtres.\
L\'apparence du mal a chez eux peu d\'appui[^40],\
Et leur âme est portée à juger bien d\'autrui.\
Point de cabale en eux, point d\'intrigues à suivre ;\
On les voit, pour tous soins, se mêler de bien vivre ;\
Jamais contre un pécheur ils n\'ont d\'acharnement ;\
Ils attachent leur haine au péché seulement,\
Et ne veulent point prendre, avec un zèle extrême,\
Les intérêts du Ciel plus qu\'il ne veut lui-même.\
Voilà mes gens, voilà comme il en faut user,\
Voilà l\'exemple enfin qu\'il se faut proposer.\
Votre homme, à dire vrai, n\'est pas de ce modèle :\
C\'est de fort bonne foi que vous vantez son zèle ;\
Mais par un faux éclat je vous crois ébloui[^41].

#### ORGON. {#orgon.-15 .orgon}

Monsieur mon cher beau-frère, avez-vous tout dit ?

#### CLÉANTE. {#cléante.-12 .cleante}

Oui.

#### ORGON. {#orgon.-16 .orgon}

Je suis votre valet. *(Il veut s\'en aller.)*

#### CLÉANTE. {#cléante.-13 .cleante}

De grâce, un mot, mon frère.\
Laissons là ce discours. Vous savez que Valère\
Pour être votre gendre a parole de vous ?

#### ORGON. {#orgon.-17 .orgon}

Oui.

#### CLÉANTE. {#cléante.-14 .cleante}

Vous aviez pris jour pour un lien si doux.

#### ORGON. {#orgon.-18 .orgon}

Il est vrai.

#### CLÉANTE. {#cléante.-15 .cleante}

Pourquoi donc en différer la fête ?

#### ORGON. {#orgon.-19 .orgon}

Je ne sais.

#### CLÉANTE. {#cléante.-16 .cleante}

Auriez-vous autre pensée en tête ?

#### ORGON. {#orgon.-20 .orgon}

Peut-être.

#### CLÉANTE. {#cléante.-17 .cleante}

Vous voulez manquer à votre foi ?

#### ORGON. {#orgon.-21 .orgon}

Je ne dis pas cela.

#### CLÉANTE. {#cléante.-18 .cleante}

Nul obstacle, je croi,\
Ne peut vous empêcher d\'accomplir vos promesses.

#### ORGON. {#orgon.-22 .orgon}

Selon.

#### CLÉANTE. {#cléante.-19 .cleante}

Pour dire un mot faut-il tant de finesses ?\
Valère sur ce point me fait vous visiter.

#### ORGON. {#orgon.-23 .orgon}

Le Ciel en soit loué !

#### CLÉANTE. {#cléante.-20 .cleante}

Mais que lui reporter ?

#### ORGON. {#orgon.-24 .orgon}

Tout ce qu\'il vous plaira.

#### CLÉANTE. {#cléante.-21 .cleante}

Mais il est nécessaire\
De savoir vos desseins. Quels sont-ils donc ?

#### ORGON. {#orgon.-25 .orgon}

De faire\
Ce que le Ciel voudra.

#### CLÉANTE. {#cléante.-22 .cleante}

Mais parlons tout de bon.\
Valère a votre foi : la tiendrez-vous, ou non ?

#### ORGON. {#orgon.-26 .orgon}

Adieu.

#### CLÉANTE. {#cléante.-23 .cleante}

Pour son amour je crains une disgrâce,\
Et je dois l\'avertir de tout ce qui se passe.

-   [^30]:*Vos ravissements* : Orgon emploie là abusivement un mot
    du vocabulaire mystique.
-   [^31]:On lit dans l\'Imitation de Jésus-Christ, I, 3 : *Vere
    prudens est qui omnia terrena arbitratur ut stercora*, ce que
    Corneille traduit : « Vraiment sage est celui qui prend pour du
    fumier les choses de la terre. » Mais Orgon a renchéri sur
    l\'Imitation dans les vers suivants, car ce ne sont pas seulement
    les biens terrestres qu\'il méprise, mais aussi les affections
    familiales les plus légitimes.
-   [^32]:Cette expression équivaut à : « Et que prétendez-vous que
    toutes ces niaiseries prouvent ? ».
-   [^33]:*Le libertinage* : non pas la libre-pensée caractérisée,
    mais le manque de respect pour tout ce qui touche aux choses
    religieuses.
-   [^34]:*Entiché* : « gâté par quelque chose de faux ou de
    moralement mauvais » (Littré).
-   [^35]:Sans doute: sans aucun doute, assurément.
-   [^36]:Caton l\'Ancien passait pour l\'auteur de distiques
    moraux souvent réimprimés aux XVIe et XVIIe siècles, et qui sont en
    réalité l\'oeuvre de Dionysius Cato, qui vécut au Ier siècle de
    notre ère.
-   [^37]:*Ces dévots de place* : ces dévots qui font profession
    d\'être dévots sur la place publique, comme les domestiques qui
    attendaient sur la place publique qu\'on les engage, et qu\'on
    appelait « valets de place ».
-   [^38]:*Demandent chaque jour* : ont chaque jour une requête à
    présenter en faveur de tel ou tel de leurs protégés.
-   [^39]:*Fier* : brutal, féroce.
-   [^40]:*Appui*, au figuré, signifie « faveur, crédit »
    (Dictionnaire de Furetière, 1690). L\'apparence du mal a chez eux
    peu de crédit, et elle ne suffit pas à les persuader.
-   [^41]:*Ébloui* : abusé, trompé.
